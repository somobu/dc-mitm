use std::io::Result;

fn main() -> Result<()> {
    #[cfg(feature = "recompile")]
    if std::fs::exists("protobuf").unwrap() {
        let mut prost_build = prost_build::Config::new();
        prost_build.protoc_arg("--experimental_allow_proto3_optional");
        prost_build.compile_protos(
            &[
                "protobuf/FrecencyUserSettings.proto",
                "protobuf/PreloadedUserSettings.proto",
            ],
            &["protobuf/"],
        )?;

        std::fs::copy(
            std::env::var("OUT_DIR").unwrap() + "/discord_protos.discord_users.v1.rs",
            std::env::var("CARGO_MANIFEST_DIR").unwrap() + "/src/lib.rs",
        )?;
    }

    Ok(())
}
