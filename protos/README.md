# Protobuf specification

## Build

### Files

Grab somewhere `FrecencyUserSettings.proto` and `PreloadedUserSettings.proto` 
and place it to `protobuf/` folder.

### Tools
Ubuntu:
```bash
sudo apt-get install protobuf-compiler
rustup component add rustfmt
```

### Recompile
```bash
cargo build --features recompile
cargo fmt
```