# DCAS, DC Alternative Server

Small and incomplete DC server -- check [features page](doc/features.md) for
project status.

![](doc/res/readme_screenshot.png)


## Goals

Current objective: implement DC server which can be used by a small community
on a day-to-day basis.


## How to build and run?

To build server: `cargo build` is enough.

To actually use it: grab any unofficial DC client and update its endpoints to
point to your server.

See [setup instructions](doc/setup.md) for details.


## TODO

### Usability concerns

Things discovered during internal testing and which make users suffer. This 
list is considered top priority and normally takes precedence over the actual 
TODO.

- bug / ???: Edit guild -> Apply -> [member list is gone];
    - Investigate guild update sequence;
- bug / api: Edit channel -> Invites -> [infinite loading];
- bug / api: RMB on user -> Ban/Kick -> [nothing happens];


### Actual TODO stages

A step-by-step guide on how to implement fully-featured DC server.

12. Mentions:
    - Generate channel/user mentions for sent messages;
    - bug: channel couldnt be mentioned nor channel mention is updated after 
        edit/delete event;
    - Theres a few `unread_*`/`mention_*` counters here and there;
    - Message Ack proc has mention count too;
13. Roles and Permissions;
14. Friends and DM:
    - Research and implement DM channel type;
    - Do not forget: ack states possibly store channel type in `flags` field;
    - Home screen:
        - Direct messages column;
        - Friends lists (online, all, pending, blocked);
        - Add friend by username;
    - User profile actions:
        - Add friend / Block user;
        - Send message / Invite to server;
        - Mutual servers / Mutual friends;
15. Other channel types:
    - Research and implement Threads;
    - Do not forget: ack states possibly store channel type in `flags` field;
16. Voice connectivity;
17. Message indexing and search;

Other features:
- Pinned messages;
- Custom statuses:
    - do not forget to send appropriate status in `patch_me`;
- Server profiles;
- Invites:
    - dm invites are completely broken;
    - invite create/delete realtime events are not sent;
    - invite properties are not handled at all;


### Backlog

A bunch of non-critical things that should not be forgotten.

- make config's `public_*` fields optional (fall back to bind/port values);
- use SQL cascades to drop all related data on server/channel/message deletion;
- handle `Result`s properly, ditch `unwrap`s!
- reroute `status.discord.com`, `discordstatus.com`, `stripe.com` requests/api;
- feature / server: Token expiration;

Bugs:

- members list disappears after server is edited (e.g.: server icon change);
- picture upload may dupe pic (db unique constraint failed) -- testing needed;
- `settings-protobuf` versioning is not implemented;
- CDN ignores 'range' header (e.g. when serving text attachments);
- CDN doesnt handle videos well (e.g. upload webm -- cdn doesnt detect vid 
    width/height);
- attachments (incl. CDN uploads) is not deleted upon message deletion;
- text files have force-utf8 mime type (encoding should be detected and set 
    properly instead);
- dc<->tg text conversion doesnt handle corner-cases properly (see 
    `tg_conversion` mod);

Security & reliability:

- add salt to pwds;
- disallow unauthenticated CDN uploads (api should generate upload url w/ 
    unique upload key which is to be checked by cdn server during upload
    process);
- zlib compressor buffer are now capped to 16MB (after that client is forced to 
    reconnect). Is it correct?;

Performance:

- `Ready` generation code is not optimal;
- member list updates are far from optimal (is not a concern at current scale);
- is it possible to handle `settings-protobuf` as byte-array and update its 
    sections manually?;
- `read state`-related part of code (http handle + db r/w) does not need to be 
    DB-bound;


### Distant future

- End-to-end encryption;
- Split server's monolith into microservices (use ZMQ for communication?);
- Detect duplicate pictures;
- OCR & index pictures to improve message search;
- Server-side message translation;


## Penging migrations

```bash
cp server/dcas.sqlite server/dcas.sqlite.bak    # Create backup DB
rm server/dcas.sqlite                           # Remove default DB
cargo run                                       # Generate empty DB (ctrl-c server)
cd server                                       # cd server
./../tools/migrate.py                           # Run migration tool
cd .. ; cargo run                               # Check that all works fine
```

### Late-feb

```sql
ALTER TABLE channel ADD COLUMN parent_id INTEGER;
ALTER TABLE channel ADD COLUMN position INTEGER NOT NULL DEFAULT 0;
ALTER TABLE channel ADD COLUMN topic TEXT DEFAULT NULL;
ALTER TABLE channel ADD COLUMN nsfw INTEGER NOT NULL DEFAULT 0;
```


## License

[GNU GPL v3](LICENSE)

