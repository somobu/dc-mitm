# SFU in Rust

Quick review of available options, done at 10 jan 2025.


## RSFU

- last commit 2+ yrs ago
- 314 crates :omg:
- demo works just fine


## webrtc-rs/sfu

- last commit month ago
- 395 crates :omg:
- readme: async perf issues
- demo is broken: fails to connect to the server


## rheomesh

- last commit 2 days ago
- 328 crates :omg:
- client: `436 packages were added to the project (+ 378.33 MiB)`
- unable to build the demo due to some "missing imports" shit
