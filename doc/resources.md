# Known resources

| Author                            | Project           | Summary |
| ---                               | ---               | --- |
| [Dolfies and contributors][6]     | [discord.sex][3]  | Unofficial discord documentation. Contains generous amount of APIs is documented and explained in a form API of reference. |
| [Luna][4], based on prev research | [Discord Docs][5] | Unofficial discord docs: neat notes on guild sync, lazy guilds, statuses, remote auth and much more |
| [SpaceManiac][1]                  | [discord-rs][2]   | Discord model. Has `read_state` implemented. |


[1]: https://github.com/SpaceManiac/
[2]: https://github.com/SpaceManiac/discord-rs
[3]: https://github.com/discord-userdoccers/discord-userdoccers
[4]: https://l4.pm/
[5]: https://luna.gitlab.io/discord-unofficial-docs/
[6]: https://github.com/discord-userdoccers/discord-userdoccers/graphs/contributors
