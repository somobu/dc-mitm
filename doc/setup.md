# Server setup

## How to build and run?

Get Rust and Cargo. Visit [rust-lang.org][install-rust] for  installation
instructions.

Then just do the following:

```bash
git clone https://gitlab.com/somobu/dcas.git && cd dcas
clear ; cargo run
```

[install-rust]: https://www.rust-lang.org/tools/install


## How to connect to running server?

tl;dr: Grab any [third-party client][clients-list], modify its endpoints (if
needed), and login / supply token as it is done normally with that client.

Lets assume you have started the server localy with default config and now it
is running at `localhost:9000`. 

Then, depending on client you have chosen:
- To log in via [JankClient][jank-client] you should set client's `api url` to
  `http://localhost:9000/api/v9` right at the login page and be able to 
  successfully log in.
- [Disrust][disrust] requires extensive patching. Not only you have to replace
  HTTP and WS endpoint domains, but also edit some code as DCAS has slightly
  different API data model.
- [GoofCord][goofcord] appears to be fully working, but you have to somehow
  deploy the official client on your server first.

[clients-list]: https://github.com/Discord-Client-Encyclopedia-Management/Discord3rdparties
[jank-client]: https://jankclient.greysilly7.xyz/
[disrust]: https://github.com/DvorakDwarf/disrust
[goofcord]: https://github.com/Milkshiift/GoofCord


## How to deploy it on my server?

Build server in release mode with:
```
cargo build --release
```

Copy `dcas` executable from `target/release` folder to your server.

Run it once to check if everything all right and to make server generate stub
configuration file at `server/config.json` and database at `server/dcas.sqlite`
for you.

Adjust `server/config.json` if needed and restart.
