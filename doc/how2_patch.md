# How to patch a certain web client

So, you've got a URL of certain DC web client. Now you want to somehow use it
to log into your self-hosted DCAS server.

To get everything up and running you have to fetch DC web client, place it in
some directory (lets call this directory `client`), patch its endpoint urls to
point to DCAS IP/domain and update server's `config.json`, setting 
`main.assets_directory` to your `client` dir. 


## How to fetch data

Server is able to fetch missing resources by itself, so all you have to do is
to specify URL to fetch from.

1. Adjust `assets_fetch_src` parameter in `server/config.json`;
2. Start server: `cargo run`;
3. Open `http://localhost:9000/` in browser;
4. Wait for resources to load (you'll have to wait until the login page is loaded);
5. You should now see standard login page in browser window;
6. And you wont go any further unless you modify client's endpoints.


## Hot to modify data

1. Edit `client/index.html` applying the following:
    - replace each `//discord.com` -> `//R_MAIN_SHORT`,
    - replace `wss://gateway.discord.gg` -> `R_GW_ENDPOINT_FULL`,
    - set `CDN_HOST: 'R_CDN_SHORT',`;
2. Find and replace in all files:
    - update cdn url in all files:
        - `https://cdn.discordapp.com` -> `R_CDN_FULL`;
        - `cdn.discordapp.com` -> `R_CDN_SHORT`
    - set proper scheme in endpoints (useful for `http://localhost` setup):
        - `return"https:"+window.GLOBAL_ENV.API_ENDPOINT` -> `return "R_API_ENDPOINT_FULL"`;
        - (obsolete?) `s="wss:".concat(window.GLOBAL_ENV.REMOTE_AUTH_ENDPOINT,"/?v=2")`, `wss:` -> `ws:`
- Voice:
    - Disable TLS: find `? "wss:" : "ws:"` and replace `wss:` -> `ws:`;


## How to fetch more

In case you want to fetch everything at once.

### Chunks (js and css)

Look for `+ ".js"`.

Collect into `js.list.txt` and transform into lines of full urls.

```bash
cd assets
for i in `cat ../js.list.txt` ; do echo $i ; curl -fLO -x socks5://localhost:9150 $i; done
```

### Media

You wanna grab `.svg` and `.png` assets with this command:

```bash
for file in ./assets/* ; do cat $file | tr '"' '\n' | grep -Po '/assets/[0-9a-z]{20}.png' ; done | sort | uniq > png.list.txt
```
