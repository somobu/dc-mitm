# Features status

Supported client version: API v9, January 2025.


## Services

| Service           | Status                |
| ----------------- | --------------------- |
| Client statics    | works                 |
| HTTP API          | partially implemented |
| WS API (gateway)  | partially implemented |
| CDN               | mostly implemented    |
| Voice             | not implemented       |
| Streaming         | not implemented       |
| Preview proxy     | not implemented       |
| Remote auth       | not implemented       |


## Resources

| Resource          | Access (1) | Props (2) |
| ---               | :---: | :---: |
| Guild             | CRUD | Few  |
| - Invite          | CRUD | Some |
| - Member          |      |      |
| - Template        |      |      |
| Channel           | CRUD | Few  |
| - DM channel      | R    | Few  |
| - Guild channel   | CRUD | Few  |
| - Guild thread    |      |      |
| - Unread icon     | CRU  | Most |
| Message           | CRUD | Some |
| - Inlay data      |      |      |
| - Attachment      | CR   | Most |
| - Embed           |      |      |
| - Reaction        |      |      |
| - Sticker         |      |      |
| Role              |      |      |
| User              | CR   | Few  |
| - User prof.      |  R   | Few  |
| Client settings   | -    | -    |
| - Protobuf        | CRU- | All  |       
| Clan              |      |      |

(1): CRUD stands for Create/Read/Update/Delete:
- C: resource can be created (e.g. you can send new message);
- R: resource can be requested (e.g. you can get list of messages);
- U: resource can be updated (e.g. you can edit message);
- D: resource can be deleted (e.g. you can delete message);

(2): Aproximate completeness:
- Few: a very little of resource's properties is not hard-coded (ex.: message has only ID and text);
- Some: about half of resource's props is handled and stored (ex.: message has ID, text, date, author, but lacks edited flag and system flags);
- Most: a very little (and rarely used) props is not implemented (ex.: user's messages have all the properties they are expected to have, but system messages lacks some data);
- All: all data is stored and handled correctly;
