//! Notes based on [discord.sex](https://docs.discord.sex/topics/voice-connections)
#![allow(dead_code)]

use futures_util::{stream::SplitSink, SinkExt, StreamExt};
use serde::{Deserialize, Serialize};
use tokio::net::{TcpListener, TcpStream, UdpSocket};
use tokio_tungstenite::tungstenite::protocol::{frame::coding::CloseCode, CloseFrame};
use tokio_tungstenite::{tungstenite::Message, WebSocketStream};

use crate::structures::{gateway::Stub, snowflake::Snowflake};
use crate::AppData;

#[derive(Debug, Serialize, Deserialize)]
#[serde(tag = "op", content = "d")]
pub enum VoiceEvent {
    #[serde(rename = 0)]
    Identify(Identify),

    #[serde(rename = 1)]
    SelectProtocol(SelectProtocol),

    #[serde(rename = 2)]
    Ready(Ready),

    #[serde(rename = 3)]
    Heartbeat(u64),

    #[serde(rename = 4)]
    SessionDescription(SessionDescription),

    #[serde(rename = 5)]
    Speaking(Speaking),

    #[serde(rename = 6)]
    HeartbeatAck(HeartbeatAck),

    #[serde(rename = 7)]
    Resume {},

    #[serde(rename = 8)]
    Hello(Hello),

    #[serde(rename = 12)]
    Video(Video),

    #[serde(rename = 16)]
    VoiceBackendVersion {},
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Identify {
    pub server_id: String,
    pub user_id: String,
    pub session_id: String,
    pub token: String,
    // pub max_secure_frames_version: u32,
    // pub video: bool,
    // pub streams: Vec<Stub>
}

#[derive(Debug, Serialize, Deserialize)]
pub struct SelectProtocol {
    // TODO: me
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Ready {
    pub ssrc: u16,
    pub ip: String,
    pub port: u16,
    pub modes: Vec<String>,
    pub experiments: Vec<String>,
    pub streams: Vec<Stub>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct SessionDescription {
    pub audio_codec: String,
    pub video_codec: Option<String>,
    pub media_session_id: String,
    pub mode: Option<String>,
    pub secret_key: Option<[u8; 32]>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Speaking {
    pub speaking: u32,
    pub ssrc: u16,

    /// Only sent by the voice server
    pub user_id: Option<Snowflake>,

    /// Not sent by the voice server
    pub delay: Option<u32>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct HeartbeatAck {
    pub t: u64,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Hello {
    pub v: u32,
    pub heartbeat_interval: u32,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Video {
    pub audio_ssrc: u16,
    pub video_ssrc: u16,

    /// Not sent by the voice server
    pub rtx_ssrc: Option<u16>,

    pub streams: Vec<Stream>,

    /// Only sent by the voice server
    pub user_id: Option<String>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Stream {
    // TODO: me!
}

const UDP_PORT: u16 = 9003;

pub async fn server(data: AppData) -> std::io::Result<()> {
    let s = TcpListener::bind(format!(
        "{}:{}",
        data.config.voice.ws_bind, data.config.voice.ws_port
    ))
    .await
    .unwrap();

    loop {
        let l = s.accept().await.unwrap();

        let stream = tokio_tungstenite::accept_async(l.0).await;

        if stream.is_ok() {
            tokio::spawn(async {
                let _uid = handle_stream(stream.unwrap()).await;

                // Mark client as offline
                // uid.map(|e| update_last_seen(e, Some(0)));
            });
        } else {
            let e = stream.unwrap_err();
            println!("ERROR ACCEPTING SOCKET {e:#?}");
        }
    }
}

async fn handle_stream(stream: WebSocketStream<TcpStream>) {
    let (mut tx, mut rx) = stream.split();
    // let mut bc_rx = BC_PAIR.0.subscribe();

    send(
        &mut tx,
        VoiceEvent::Hello(Hello {
            v: 4,
            heartbeat_interval: 41250,
        }),
    )
    .await;

    let mut _gid: Option<String> = None;
    let mut uid: Option<String> = None;
    let mut _sid: Option<String> = None;
    let mut _token: Option<String> = None;

    loop {
        let msg = rx.next().await.unwrap().unwrap();
        let msg = match msg {
            Message::Text(m) => m,
            Message::Binary(_) => todo!(),
            Message::Ping(_) => todo!(),
            Message::Pong(_) => todo!(),
            Message::Close(_) => return,
            Message::Frame(_) => todo!(),
        };
        let msg: VoiceEvent = serde_json::from_str(&msg).unwrap();

        match msg {
            VoiceEvent::Identify(identify) => {
                _gid = Some(identify.server_id);
                uid = Some(identify.user_id);
                _sid = Some(identify.session_id);
                _token = Some(identify.token);
                send(
                    &mut tx,
                    VoiceEvent::Ready(Ready {
                        ssrc: 1224,
                        ip: "localhost".to_string(),
                        port: UDP_PORT,
                        modes: vec!["xsalsa20_poly1305".to_string()],
                        experiments: vec![],
                        streams: vec![],
                    }),
                )
                .await;
            }
            VoiceEvent::SelectProtocol(_select_protocol) => {
                send(
                    &mut tx,
                    VoiceEvent::SessionDescription(SessionDescription {
                        audio_codec: "opus".to_string(),
                        video_codec: None, //"H264".to_string(),
                        media_session_id: "89f1d62f166b948746f7646713d39dbb".to_string(),
                        mode: Some("xsalsa20_poly1305".to_string()),
                        secret_key: Some([0u8; 32]),
                    }),
                )
                .await;
            }
            VoiceEvent::Ready(_) => todo!(),
            VoiceEvent::Heartbeat(t) => {
                send(&mut tx, VoiceEvent::HeartbeatAck(HeartbeatAck { t })).await;
            }
            VoiceEvent::SessionDescription(_) => todo!(),
            VoiceEvent::Speaking(_speaking) => { /* TODO: me! */ }
            VoiceEvent::HeartbeatAck(_) => todo!(),
            VoiceEvent::Resume {} => {
                tx.send(Message::Close(Some(CloseFrame {
                    code: CloseCode::Bad(4006),
                    reason: "TOKEN_INVALID".into(),
                })))
                .await
                .unwrap();
                return;
            }
            VoiceEvent::Hello(_) => todo!(),

            VoiceEvent::Video(mut video) => {
                video.user_id = Some(uid.clone().unwrap());
                video.rtx_ssrc = None;
                send(&mut tx, VoiceEvent::Video(video)).await;
            }

            VoiceEvent::VoiceBackendVersion {} => { /* TODO: me! */ }
        };
    }
}

async fn send(tx: &mut SplitSink<WebSocketStream<TcpStream>, Message>, evt: VoiceEvent) {
    let m = serde_json::to_string(&evt).unwrap();
    tx.send(Message::Text(m)).await.unwrap();
}

/// Browser voice implementation uses WebRTC :/
pub async fn server_udp(data: AppData) -> std::io::Result<()> {
    let addr = format!("{}:{}", data.config.voice.ws_bind, UDP_PORT);
    println!("VS UDP IS AT {addr}");
    let l = UdpSocket::bind(addr).await.unwrap();

    let mut buf = Vec::new();
    buf.resize(4096, 0_u8);

    loop {
        let (s, a) = l.recv_from(&mut buf).await.unwrap();
        println!("Received {s} bytes: {:?}", &buf[0..s]);
        l.send_to(&buf[0..s], &a).await.unwrap();
    }
}
