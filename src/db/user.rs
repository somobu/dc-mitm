use rand::RngCore;
use rusqlite::Row;

use crate::structures::snowflake::Snowflake;

use super::DB;

#[derive(Debug, Clone)]
pub struct User {
    pub id: Snowflake,
    pub username: String,
    pub email: String,

    pub display_name: Option<String>,
    pub avatar: Option<Snowflake>,

    pub is_bot: bool,
}

impl User {
    pub fn unknown(id: Snowflake) -> Self {
        Self {
            id,
            username: "unknown".to_string(),
            email: "unknown@local.host".to_string(),
            display_name: None,
            avatar: None,
            is_bot: false,
        }
    }

    pub fn global_name(&self) -> String {
        self.display_name.as_ref().unwrap_or(&self.username).clone()
    }
}

impl From<&Row<'_>> for User {
    fn from(row: &Row) -> Self {
        Self {
            id: row.get("id").unwrap(),
            username: row.get("username").unwrap(),
            email: row.get("email").unwrap(),
            display_name: row.get("display_name").unwrap(),
            avatar: row.get("avatar").unwrap(),
            is_bot: row.get("is_bot").unwrap(),
        }
    }
}

impl DB {
    pub fn user(&self, user_id: Snowflake) -> rusqlite::Result<User> {
        self.conn.query_row(
            "SELECT id, display_name, avatar, email, username, is_bot FROM user WHERE id = ?",
            (user_id,),
            |row| Ok(row.into()),
        )
    }

    pub fn add_user(&self, user: User) -> rusqlite::Result<usize> {
        self.conn.execute(
            "INSERT INTO user (id, display_name, email, username, password, is_bot) 
                    VALUES (?, ?, ?, ?, ?, ?);",
            (
                user.id,
                user.display_name,
                user.email,
                user.username,
                "".to_string(),
                user.is_bot,
            ),
        )
    }

    pub fn register(&self, user: User, password: &str) -> rusqlite::Result<usize> {
        self.conn.execute(
            "INSERT INTO user (id, display_name, email, username, password, is_bot) 
                    VALUES (?, ?, ?, ?, ?, 0);",
            (
                user.id,
                user.display_name,
                user.email,
                user.username,
                password,
            ),
        )
    }

    pub fn auth(&self, login: String, password: &str) -> rusqlite::Result<(u64, Snowflake)> {
        let id: Snowflake = self.conn.query_row(
            "SELECT id FROM user WHERE is_bot = 0 AND (email = ?1 OR username = ?1) AND password = ?2",
            (login.clone(), password),
            |row| row.get("id"),
        )?;

        // "/2" limits rand value to non-negative range
        let token = rand::thread_rng().next_u64() / 2;
        self.insert_token(token, id)?;

        Ok((token, id))
    }

    pub fn update_user(&self, user: &User) -> rusqlite::Result<()> {
        self.conn
            .execute(
                "UPDATE user SET display_name = ?, username = ?, avatar = ? WHERE id = ?;",
                (
                    user.display_name.clone(),
                    user.username.clone(),
                    user.avatar,
                    user.id,
                ),
            )
            .map(|_| ())
    }
}
