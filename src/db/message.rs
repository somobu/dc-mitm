use rusqlite::Row;

use crate::{millis, sql_file, structures::snowflake::Snowflake};

use super::DB;

#[derive(Debug, Clone)]
pub struct Message {
    pub id: Snowflake,

    pub channel: Snowflake,
    pub author: Snowflake,
    pub text: String,

    pub edited_at: u64,
}

impl From<&Row<'_>> for Message {
    fn from(row: &Row) -> Self {
        let t: Option<String> = row.get("text").unwrap();
        Message {
            id: row.get("id").unwrap(),
            channel: row.get("channel").unwrap(),
            author: row.get("author").unwrap(),
            text: t.unwrap_or_default(),
            edited_at: row.get("edited_at").unwrap(),
        }
    }
}

impl DB {
    pub fn last_message_in(&self, channel: Snowflake) -> rusqlite::Result<Message> {
        self.conn
            .prepare(sql_file!("get_last_msg"))?
            .query_row([channel], |row| Ok(row.into()))
    }

    pub fn add_message(
        &self,
        id: Snowflake,
        author: Snowflake,
        channel: Snowflake,
        text: String,
    ) -> rusqlite::Result<Message> {
        let edited_at = id.millis();
        self.conn.execute(
            "INSERT INTO message (id, channel, author, text, edited_at) VALUES (?, ?, ?, ?, ?);",
            (id, channel, author, text, edited_at),
        )?;

        self.last_message_in(channel)
    }

    pub fn get_message(&self, message_id: Snowflake) -> rusqlite::Result<Message> {
        self.conn.query_row_and_then(
            "SELECT id, channel, author, text, edited_at FROM message WHERE id = ?",
            [message_id],
            |row| Ok(row.into()),
        )
    }

    pub fn get_messages_around(
        &self,
        channel: Snowflake,
        mid: Snowflake,
        before: u64,
        after: u64,
    ) -> rusqlite::Result<Vec<Message>> {
        self.conn
            .prepare(sql_file!("get_msgs_around"))?
            .query_and_then((channel, mid, before, after), |row| Ok(row.into()))?
            .collect()
    }

    pub fn update_message(&self, message_id: Snowflake, text: String) -> rusqlite::Result<()> {
        self.conn
            .execute(
                "UPDATE message SET text = ?, edited_at = ? WHERE id = ?;",
                (text, millis(), message_id),
            )
            .map(|_| ())
    }

    pub fn delete_message(&self, id: Snowflake) -> rusqlite::Result<()> {
        self.conn
            .execute("DELETE FROM message WHERE id = ?;", (id,))
            .map(|_| ())
    }
}
