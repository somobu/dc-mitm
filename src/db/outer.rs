//! Intended to be used by extensions (not by main server!)
//!
//! Handles "internal ID <=> extension ID" mapping

use rusqlite::OptionalExtension;

use crate::structures::snowflake::Snowflake;

use super::DB;

impl DB {
    pub fn add_msg_link(
        &self,
        ext: &str,
        own_id: Snowflake,
        outer_id: String,
    ) -> rusqlite::Result<()> {
        self.conn.execute(
            "INSERT INTO outer_msg (ext, own_id, outer_id) VALUES (?, ?, ?);",
            (ext, own_id, outer_id),
        )?;

        Ok(())
    }

    pub fn get_msg_own_id(&self, ext: &str, outer_id: &str) -> rusqlite::Result<Option<Snowflake>> {
        self.conn
            .query_row_and_then(
                "SELECT own_id FROM outer_msg WHERE ext = ? AND outer_id = ?",
                [ext.to_string(), outer_id.into()],
                |row| Ok(row.get("own_id").unwrap()),
            )
            .optional()
    }

    pub fn get_msg_outer_id(
        &self,
        ext: &str,
        own_id: Snowflake,
    ) -> rusqlite::Result<Option<String>> {
        self.conn
            .query_row_and_then(
                "SELECT outer_id FROM outer_msg WHERE ext = ? AND own_id = ?",
                [ext.to_string(), own_id.into()],
                |row| Ok(row.get("outer_id").unwrap()),
            )
            .optional()
    }

    pub fn add_user_link(
        &self,
        ext: &str,
        own_id: Snowflake,
        outer_id: String,
    ) -> rusqlite::Result<()> {
        self.conn.execute(
            "INSERT INTO outer_user (ext, own_id, outer_id) VALUES (?, ?, ?);",
            (ext, own_id, outer_id),
        )?;

        Ok(())
    }

    pub fn get_user_own_id(
        &self,
        ext: &str,
        outer_id: &str,
    ) -> rusqlite::Result<Option<Snowflake>> {
        self.conn
            .query_row_and_then(
                "SELECT own_id FROM outer_user WHERE ext = ? AND outer_id = ?",
                [ext.to_string(), outer_id.into()],
                |row| Ok(row.get("own_id").unwrap()),
            )
            .optional()
    }

    pub fn get_user_outer_id(
        &self,
        ext: &str,
        own_id: Snowflake,
    ) -> rusqlite::Result<Option<String>> {
        self.conn
            .query_row_and_then(
                "SELECT outer_id FROM outer_user WHERE ext = ? AND own_id = ?",
                [ext.to_string(), own_id.into()],
                |row| Ok(row.get("outer_id").unwrap()),
            )
            .optional()
    }

    pub fn get_channel_own_id(
        &self,
        ext: &str,
        outer_id: &str,
    ) -> rusqlite::Result<Option<Snowflake>> {
        self.conn
            .query_row_and_then(
                "SELECT own_id FROM outer_channel WHERE ext = ? AND outer_id = ?",
                [ext.to_string(), outer_id.into()],
                |row| Ok(row.get("own_id").unwrap()),
            )
            .optional()
    }

    pub fn get_channel_outer_id(
        &self,
        ext: &str,
        own_id: Snowflake,
    ) -> rusqlite::Result<Option<String>> {
        self.conn
            .query_row_and_then(
                "SELECT outer_id FROM outer_channel WHERE ext = ? AND own_id = ?",
                [ext.to_string(), own_id.into()],
                |row| Ok(row.get("outer_id").unwrap()),
            )
            .optional()
    }
}
