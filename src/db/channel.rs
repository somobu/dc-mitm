use rusqlite::{OptionalExtension, Row};

use crate::sql_file;
use crate::structures::{resource::channel::ChannelType, snowflake::Snowflake};

use super::{user::User, DB};

#[derive(Debug, Clone)]
pub struct Channel {
    pub id: Snowflake,
    pub r#type: ChannelType,
    pub name: String,

    pub guild_id: Option<Snowflake>,

    pub parent_id: Option<Snowflake>,
    pub position: u32,
    pub topic: Option<String>,
    pub nsfw: bool,

    /// You can supply None in insert/update requests
    pub last_message_id: Option<Snowflake>,
}

impl From<&Row<'_>> for Channel {
    fn from(row: &Row) -> Self {
        Channel {
            id: row.get("id").unwrap(),
            r#type: row.get("type").unwrap(),
            name: row.get("name").unwrap(),
            guild_id: row.get("guild_id").unwrap(),
            parent_id: row.get("parent_id").unwrap(),
            position: row.get("position").unwrap(),
            topic: row.get("topic").unwrap(),
            nsfw: row.get("nsfw").unwrap(),
            last_message_id: row.get("last_message_id").unwrap(),
        }
    }
}

impl DB {
    pub fn dm_channels(&self, user_id: Snowflake) -> rusqlite::Result<Vec<Channel>> {
        self.conn
            .prepare(sql_file!("select_dms_of_user"))?
            .query_and_then([user_id], |row| Ok(row.into()))?
            .collect()
    }

    pub fn users_of_dm(&self, channel_id: Snowflake) -> rusqlite::Result<Vec<User>> {
        self.conn
            .prepare(sql_file!("select_users_of_dm"))?
            .query_and_then([channel_id], |row| Ok(row.into()))?
            .collect()
    }

    pub fn guild_channels(&self, guild_id: Snowflake) -> rusqlite::Result<Vec<Channel>> {
        self.conn
            .prepare(sql_file!("select_channels_of_guild"))?
            .query_and_then([guild_id], |row| Ok(row.into()))?
            .collect()
    }

    pub fn guild_channel_create(&self, channel: &Channel) -> rusqlite::Result<()> {
        self.conn.execute(
            "INSERT INTO channel (id, type, name, guild_id, topic, nsfw) VALUES (?, ?, ?, ?, ?, ?);",
            (
                channel.id,
                channel.r#type,
                channel.name.clone(),
                channel.guild_id,
                channel.topic.clone(),
                channel.nsfw
            ),
        )?;

        Ok(())
    }

    pub fn get_channel(&self, channel_id: Snowflake) -> rusqlite::Result<Channel> {
        self.conn
            .query_row_and_then(sql_file!("get_channel"), [channel_id], |row| {
                Ok(row.into())
            })
    }

    pub fn update_channel(&self, channel: &Channel) -> rusqlite::Result<()> {
        self.conn
            .execute(
                "UPDATE channel SET type = ?, name = ?, guild_id = ?, topic = ?, nsfw = ? WHERE id = ?;",
                (
                    channel.r#type,
                    channel.name.clone(),
                    channel.guild_id,
                    channel.topic.clone(),
                    channel.nsfw,
                    channel.id,
                ),
            )
            .map(|_| ())
    }

    pub fn reorder_guild_channel(
        &self,
        id: Snowflake,
        parent_id: Option<Snowflake>,
        position: u32,
    ) -> rusqlite::Result<()> {
        self.conn
            .execute(
                "UPDATE channel SET parent_id = ?, position = ? WHERE id = ?;",
                (parent_id, position, id),
            )
            .map(|_| ())
    }

    pub fn delete_channel(&self, id: Snowflake) -> rusqlite::Result<()> {
        self.conn
            .execute("DELETE FROM channel WHERE id = ?;", (id,))
            .map(|_| ())
    }

    pub fn is_user_in_channel(
        &self,
        user: Snowflake,
        channel: Snowflake,
    ) -> rusqlite::Result<bool> {
        self.conn
            .query_row(sql_file!("is_user_in_channel"), [user, channel], |_| {
                Ok(true)
            })
            .optional()
            .map(|v| v.unwrap_or(false))
    }
}
