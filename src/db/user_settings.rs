use rusqlite::OptionalExtension;

use crate::{sql_file, structures::snowflake::Snowflake};

use super::DB;

pub const READ_STATE_RESOURCE: u32 = u32::MAX - 1;

#[derive(Debug, Clone)]
pub struct ReadState {
    pub user: Snowflake,
    pub channel: Snowflake,
    pub message: Snowflake,
    pub manual: bool,
}

impl DB {
    pub fn insert_settings_proto(
        &self,
        user: Snowflake,
        proto: u8,
        data: &[u8],
        ver: Option<u32>,
    ) -> rusqlite::Result<()> {
        if ver.is_some() {
            self.conn
                .execute(
                    "INSERT INTO user_settings (id, proto, dat, ver) VALUES (?, ?, ?, ?)
                         ON CONFLICT (id, proto) DO UPDATE SET dat=?3, ver=?4;",
                    (user, proto, data, ver.unwrap()),
                )
                .map(|_| ())
        } else {
            self.conn
                .execute(
                    "INSERT INTO user_settings (id, proto, dat) VALUES (?, ?, ?)
                         ON CONFLICT (id, proto) DO UPDATE SET dat=?3",
                    (user, proto, data),
                )
                .map(|_| ())
        }
    }

    pub fn get_settings_proto(&self, user: Snowflake, proto: u8) -> Option<(Vec<u8>, u32)> {
        self.conn
            .query_row(
                "SELECT dat, ver FROM user_settings WHERE id = ? AND proto = ?",
                (user, proto),
                |row| Ok((row.get(0)?, row.get(1)?)),
            )
            .ok()
    }

    pub fn update_read_state(&self, state: ReadState) -> rusqlite::Result<()> {
        self.conn
            .execute(
                "INSERT INTO read_state (user, channel, message, manual) VALUES (?, ?, ?, ?)
                 ON CONFLICT (user, channel) DO UPDATE SET message=?3, manual=?4;",
                (state.user, state.channel, state.message, state.manual),
            )
            .map(|_| ())
    }

    pub fn get_read_states(&self, user: Snowflake) -> rusqlite::Result<Vec<ReadState>> {
        let mut a = self.conn.prepare(sql_file!("get_read_states_of_user"))?;

        let l = a
            .query_and_then([user], |row| {
                Ok(ReadState {
                    user: row.get(0).unwrap(),
                    channel: row.get(1).unwrap(),
                    message: row.get(2).unwrap(),
                    manual: row.get(3).unwrap(),
                })
            })
            .optional()?;

        if l.is_none() {
            Ok(vec![])
        } else {
            l.unwrap().collect()
        }
    }

    pub fn get_resource_ver(&self, user: Snowflake, resource: u32) -> rusqlite::Result<u32> {
        Ok(self
            .conn
            .query_row(
                "SELECT ver FROM resource_ver WHERE user = ? AND res = ?",
                (user, resource),
                |row| Ok(row.get(0)?),
            )
            .optional()?
            .unwrap_or(1))
    }

    pub fn set_resource_ver(
        &self,
        user: Snowflake,
        resource: u32,
        ver: u32,
    ) -> rusqlite::Result<()> {
        self.conn
            .execute(
                "INSERT INTO resource_ver (user, res, ver) VALUES (?, ?, ?)
                 ON CONFLICT (user, res) DO UPDATE SET ver=?3;",
                (user, resource, ver),
            )
            .map(|_| ())
    }
}
