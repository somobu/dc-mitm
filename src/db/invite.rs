use rusqlite::Row;

use crate::structures::snowflake::Snowflake;

use super::DB;

#[derive(Debug, Clone)]
pub struct Invite {
    pub code: String,
    pub guild: Option<Snowflake>,
    pub channel: Snowflake,
    pub inviter: Snowflake,
}

impl From<&Row<'_>> for Invite {
    fn from(row: &Row) -> Self {
        Invite {
            code: row.get("code").unwrap(),
            guild: row.get("guild").unwrap(),
            channel: row.get("channel").unwrap(),
            inviter: row.get("inviter").unwrap(),
        }
    }
}

impl DB {
    pub fn create_invite(&self, invite: &Invite) -> rusqlite::Result<()> {
        self.conn
            .execute(
                "INSERT INTO invite (code, guild, channel, inviter) VALUES (?, ?, ?, ?);",
                (
                    invite.code.clone(),
                    invite.guild,
                    invite.channel,
                    invite.inviter,
                ),
            )
            .map(|_| ())
    }

    pub fn get_invite(&self, code: &str) -> rusqlite::Result<Invite> {
        self.conn.query_row_and_then(
            "SELECT code, guild, channel, inviter FROM invite WHERE code = ?",
            [code],
            |row| Ok(row.into()),
        )
    }

    pub fn get_invites_of_guild(&self, guild: Snowflake) -> rusqlite::Result<Vec<Invite>> {
        self.conn
            .prepare("SELECT code, guild, channel, inviter FROM invite WHERE guild = ?")?
            .query_and_then([guild], |row| Ok(row.into()))?
            .collect()
    }

    pub fn get_invites_of_channel(&self, channel: Snowflake) -> rusqlite::Result<Vec<Invite>> {
        self.conn
            .prepare("SELECT code, guild, channel, inviter FROM invite WHERE channel = ?")?
            .query_and_then([channel], |row| Ok(row.into()))?
            .collect()
    }

    pub fn delete_invite(&self, code: &str) -> rusqlite::Result<()> {
        self.conn
            .execute("DELETE invite WHERE code = ?;", (code,))
            .map(|_| ())
    }
}
