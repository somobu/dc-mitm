use crate::{sql_file, structures::snowflake::Snowflake};

use super::DB;

#[derive(Debug, Clone)]
pub struct Attachment {
    pub id: Snowflake,
    pub channel: Snowflake,
    pub width: u32,
    pub height: u32,
    pub size: usize,
    pub name: Option<String>,
}

impl Attachment {
    /// "real" file path
    pub fn cdn_path(&self) -> String {
        format!(
            "server/attachments/{}/{}",
            self.channel.to_string(),
            self.id.to_string()
        )
    }
}

impl DB {
    pub fn create_attachment(
        &self,
        id: Snowflake,
        channel: Snowflake,
        w: u32,
        h: u32,
        size: usize,
        name: Option<String>,
    ) -> rusqlite::Result<()> {
        self.conn.execute(
            "INSERT INTO attachment (id, channel, width, height, size, name) VALUES (?, ?, ?, ?, ?, ?);",
            (id, channel, w, h, size, name),
        )?;

        Ok(())
    }

    pub fn update_attachment(
        &self,
        id: Snowflake,
        w: u32,
        h: u32,
        size: usize,
        name: Option<String>,
    ) -> rusqlite::Result<()> {
        self.conn.execute(
            "UPDATE attachment 
            SET width = ?, height = ?, size = ?, name = ?
            WHERE id = ?;",
            (w, h, size, name, id),
        )?;

        Ok(())
    }

    pub fn link_attach_to_msg(&self, msg: Snowflake, attach: Snowflake) -> rusqlite::Result<()> {
        self.conn.execute(
            "INSERT INTO message_attachments (message, attachment) VALUES (?, ?);",
            (msg, attach),
        )?;

        Ok(())
    }

    pub fn get_attaches_of_msg(&self, msg: Snowflake) -> rusqlite::Result<Vec<Attachment>> {
        self.conn
            .prepare(sql_file!("get_attaches_of_msg"))?
            .query_and_then([msg], |row| {
                Ok(Attachment {
                    id: row.get(0).unwrap(),
                    channel: row.get(1).unwrap(),
                    width: row.get(2).unwrap(),
                    height: row.get(3).unwrap(),
                    size: row.get(4).unwrap(),
                    name: row.get(5).unwrap(),
                })
            })?
            .collect()
    }
}
