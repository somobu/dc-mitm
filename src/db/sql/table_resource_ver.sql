CREATE TABLE resource_ver (
    user INTEGER NOT NULL,
    res INTEGER NOT NULL,
    ver INTEGER NOT NULL,

    UNIQUE(user, res),
    
    FOREIGN KEY (user) REFERENCES user (id) ON DELETE CASCADE
);