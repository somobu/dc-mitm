SELECT
    user,
    channel,
    message,
    manual
FROM
    read_state
WHERE
    user = ?;