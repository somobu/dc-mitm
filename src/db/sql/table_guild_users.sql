CREATE TABLE guild_users (
    guild_id INTEGER NOT NULL,
    user_id INTEGER NOT NULL,

    UNIQUE(guild_id, user_id),

    FOREIGN KEY (guild_id) REFERENCES guild (id) ON DELETE CASCADE,
    FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE
);