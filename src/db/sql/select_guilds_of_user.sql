SELECT
    guild_id,
    guild.id,
    guild.owner_id,
    guild.name,
    guild.icon
FROM
    guild_users
    INNER JOIN guild ON guild.id = guild_id
WHERE
    user_id = ?