-- 1: user_id
-- 2: guild_id
SELECT
    1
FROM
    guild_users
WHERE
    user_id = ?1
    AND guild_id = ?2