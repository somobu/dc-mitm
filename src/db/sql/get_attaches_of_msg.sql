SELECT
    at.id,
    at.channel,
    at.width,
    at.height,
    at.size,
    at.name
FROM
    message_attachments AS ma
    INNER JOIN attachment AS at ON at.id = ma.attachment
WHERE
    ma.message = ?