-- 1: user_id
-- 2: channel_id
SELECT
    1
FROM
    guild_users
WHERE
    user_id = ?1
    AND guild_id = (
        SELECT
            guild_id
        FROM
            channel
        WHERE
            id = ?2
    )