SELECT
    user_id as id,
    user.display_name as display_name,
    user.avatar as avatar,
    user.email as email,
    user.username as username,
    user.is_bot as is_bot
FROM
    dm_recipients
    INNER JOIN user ON user.id = user_id
WHERE
    channel_id = ?