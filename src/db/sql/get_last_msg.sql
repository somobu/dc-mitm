SELECT
    id,
    channel,
    author,
    text,
    edited_at
FROM
    message
WHERE
    channel = ?
ORDER BY
    id DESC
LIMIT
    1