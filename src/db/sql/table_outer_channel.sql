CREATE TABLE outer_channel (
    ext TEXT NOT NULL,
    own_id INTEGER NOT NULL,
    outer_id TEXT NOT NULL,
    
    UNIQUE(ext, own_id)
);