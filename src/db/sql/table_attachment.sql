CREATE TABLE attachment (
    id INTEGER PRIMARY KEY NOT NULL,
    channel INTEGER NOT NULL,
    width INTEGER NOT NULL,
    height INTEGER NOT NULL,
    size INTEGER NOT NULL,
    name TEXT,

    FOREIGN KEY (channel) REFERENCES channel (id) ON DELETE CASCADE
);