CREATE TABLE user_settings (
    id INTEGER NOT NULL,
    proto INTEGER NOT NULL,
    dat BLOB NOT NULL,
    ver DEFAULT 0,

    UNIQUE(id, proto),
    
    FOREIGN KEY (id) REFERENCES user (id) ON DELETE CASCADE
);