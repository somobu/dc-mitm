CREATE TABLE read_state (
    user INTEGER NOT NULL,
    channel INTEGER NOT NULL,
    message INTEGER NOT NULL,
    manual INTEGER NOT NULL DEFAULT 0,

    UNIQUE(user, channel),

    FOREIGN KEY (user) REFERENCES user (id) ON DELETE CASCADE,
    FOREIGN KEY (channel) REFERENCES channel (id) ON DELETE CASCADE
);