SELECT
    channel_id,
    channel.type,
    channel.name,
    channel.guild_id,
    channel.parent_id,
    channel.position,
    channel.topic,
    channel.nsfw,
    last_message_id
FROM
    dm_recipients
    INNER JOIN channel ON channel.id = channel_id
    LEFT JOIN (
        SELECT
            MAX(id) AS last_message_id,
            channel AS m_ch
        FROM
            message
        GROUP BY
            channel
    ) ON channel.id = m_ch
WHERE
    user_id = ?;