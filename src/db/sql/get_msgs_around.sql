-- #1: channel_id
-- #2: message_id
-- #3: message_count_before
-- #4: message_count_after
SELECT
    *
FROM
    (
        SELECT
            id,
            channel,
            author,
            text,
            edited_at
        FROM
            message
        WHERE
            channel = ?1
            AND id <= ?2
        ORDER BY
            id DESC
        LIMIT
            ?3
    )
UNION
SELECT
    *
FROM
    (
        SELECT
            id,
            channel,
            author,
            text,
            edited_at
        FROM
            message
        WHERE
            channel = ?1
            AND id > ?2
        ORDER BY
            id ASC
        LIMIT
            ?4
    )
ORDER BY
    id DESC