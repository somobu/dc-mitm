CREATE TABLE message_attachments (
    message INTEGER NOT NULL,
    attachment INTEGER NOT NULL,

    UNIQUE(message, attachment),

    FOREIGN KEY (message) REFERENCES message (id) ON DELETE CASCADE,
    FOREIGN KEY (attachment) REFERENCES attachment (id) ON DELETE CASCADE
);