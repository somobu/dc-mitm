CREATE TABLE user (
    id INTEGER PRIMARY KEY NOT NULL,
    display_name TEXT,
    avatar INTEGER,
    email TEXT NOT NULL,
    username TEXT NOT NULL,
    password TEXT,
    is_bot INTEGER NOT NUlL DEFAULT 0,

    UNIQUE(email),
    UNIQUE(username)
);