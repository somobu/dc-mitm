SELECT
    id,
    type,
    name,
    guild_id,
    parent_id,
    position,
    topic,
    nsfw,
    (
        SELECT
            MAX(id) AS last_message_id
        FROM
            message
        WHERE
            message.channel = ?1
        GROUP BY
            channel
    ) AS last_message_id
FROM
    channel
WHERE
    id = ?1