CREATE TABLE dm_recipients (
    channel_id INTEGER NOT NULL,
    user_id INTEGER NOT NULL,

    UNIQUE(channel_id, user_id),

    FOREIGN KEY (channel_id) REFERENCES channel (id) ON DELETE CASCADE,
    FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE
);
