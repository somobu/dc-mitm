SELECT
    id,
    type,
    name,
    guild_id,
    parent_id,
    position,
    topic,
    nsfw,
    last_message_id
FROM
    channel
    LEFT JOIN (
        SELECT
            MAX(id) AS last_message_id,
            channel AS m_ch
        FROM
            message
        GROUP BY
            channel
    ) ON channel.id = m_ch
WHERE
    guild_id = ?