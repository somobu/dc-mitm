SELECT
    user_id as id,
    user.display_name as display_name,
    user.avatar as avatar,
    user.email as email,
    user.username as username,
    user.is_bot as is_bot
FROM
    guild_users
    INNER JOIN user ON user.id = user_id
WHERE
    guild_id = ?