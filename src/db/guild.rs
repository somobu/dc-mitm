use rusqlite::{OptionalExtension, Row};

use crate::{sql_file, structures::snowflake::Snowflake};

use super::{user::User, DB};

#[derive(Debug, Clone)]
pub struct Guild {
    pub id: Snowflake,
    pub owner_id: Snowflake,
    pub name: String,
    pub icon: Option<Snowflake>,
}

impl From<&Row<'_>> for Guild {
    fn from(row: &Row<'_>) -> Self {
        Guild {
            id: row.get("id").unwrap(),
            owner_id: row.get("owner_id").unwrap(),
            name: row.get("name").unwrap(),
            icon: row.get("icon").unwrap(),
        }
    }
}

impl DB {
    pub fn get_guild(&self, guild_id: Snowflake) -> rusqlite::Result<Guild> {
        self.conn.query_row_and_then(
            "SELECT id, owner_id, name, icon FROM guild WHERE id = ?",
            [guild_id],
            |row| Ok(row.into()),
        )
    }

    pub fn guild_create(&self, guild: &Guild) -> rusqlite::Result<()> {
        self.conn.execute(
            "INSERT INTO guild (id, owner_id, name, icon) VALUES (?, ?, ?, ?);",
            (
                guild.id,
                guild.owner_id,
                guild.name.clone(),
                guild.icon.clone(),
            ),
        )?;

        self.conn.execute(
            "INSERT INTO guild_users (guild_id, user_id) VALUES (?, ?);",
            (guild.id, guild.owner_id),
        )?;

        Ok(())
    }

    pub fn guild_update(&self, guild: &Guild) -> rusqlite::Result<()> {
        self.conn.execute(
            "UPDATE guild 
            SET owner_id = ?, name = ?, icon = ? 
            WHERE id = ?;",
            (
                guild.owner_id,
                guild.name.clone(),
                guild.icon.clone(),
                guild.id,
            ),
        )?;

        Ok(())
    }

    pub fn guild_delete(&self, id: Snowflake) -> rusqlite::Result<()> {
        self.conn
            .execute("DELETE FROM guild WHERE id = ?;", (id,))?;

        Ok(())
    }

    pub fn get_guilds_of_user(&self, user_id: Snowflake) -> rusqlite::Result<Vec<Guild>> {
        self.conn
            .prepare(sql_file!("select_guilds_of_user"))?
            .query_and_then([user_id], |row| Ok(row.into()))?
            .collect()
    }

    pub fn get_users_of_guild(&self, guild_id: Snowflake) -> rusqlite::Result<Vec<User>> {
        self.conn
            .prepare(sql_file!("select_users_of_guild"))?
            .query_and_then([guild_id], |row| Ok(row.into()))?
            .collect()
    }

    pub fn is_user_in_guild(&self, guild: Snowflake, user: Snowflake) -> bool {
        self.conn
            .query_row_and_then(sql_file!("is_user_in_guild"), [user, guild], |_| {
                Ok::<_, rusqlite::Error>(true)
            })
            .unwrap_or(false)
    }

    pub fn add_user_to_guild(&self, guild: Snowflake, user: Snowflake) -> rusqlite::Result<()> {
        self.conn.execute(
            "INSERT INTO guild_users (guild_id, user_id) VALUES (?, ?);",
            (guild, user),
        )?;
        Ok(())
    }

    pub fn remove_user_from_guild(
        &self,
        guild: Snowflake,
        user: Snowflake,
    ) -> rusqlite::Result<()> {
        self.conn.execute(
            "DELETE FROM guild_users WHERE guild_id = ? AND user_id = ?;",
            (guild, user),
        )?;
        Ok(())
    }

    /// If forgot that this method exists and implemented it again :/
    pub fn is_user_in_guild_v2(&self, user: Snowflake, guild: Snowflake) -> rusqlite::Result<bool> {
        self.conn
            .query_row(sql_file!("is_user_in_guild"), [user, guild], |_| {
                Ok(true)
            })
            .optional()
            .map(|v| v.unwrap_or(false))
    }
}
