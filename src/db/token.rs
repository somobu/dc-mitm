use crate::structures::snowflake::Snowflake;

use super::DB;

// Original token stored as:
// "000000000000000000000000.000000.00000000000000000000000000000000000000" // 24.6.38 [a-zA-Z0-9_-]

impl DB {
    pub fn insert_token(&self, token: u64, user: Snowflake) -> rusqlite::Result<()> {
        self.conn
            .execute(
                "INSERT INTO token (id, user_id) VALUES (?, ?);",
                (token as i64, user),
            )
            .map(|_| ())
    }

    /// Token -> User ID
    pub fn resolve_token(&self, token: &str) -> Option<Snowflake> {
        let l: Option<u64> = self
            .conn
            .query_row("SELECT user_id FROM token WHERE id = ?", (token,), |row| {
                row.get(0)
            })
            .ok()?;

        l.map(|e| Snowflake::from(e))
    }
}
