//! Database access and model

use rusqlite::Connection;

pub mod attachment;
pub mod channel;
pub mod guild;
pub mod invite;
pub mod message;
pub mod outer;
pub mod token;
pub mod user;
pub mod user_settings;

#[macro_export]
macro_rules! sql_file {
    ($file:expr) => {
        include_str!(concat!("sql/", $file, ".sql"))
    };
}

pub struct DB {
    conn: Connection,
}

impl DB {
    pub fn open_file(path: &str) -> Result<Self, ()> {
        let conn = Connection::open(path).map_err(|_| ())?;
        Ok(Self { conn })
    }

    pub fn init_tables(&self) {
        self.init_table(sql_file!("table_user"));
        self.init_table(sql_file!("table_token"));
        self.init_table(sql_file!("table_user_settings"));
        self.init_table(sql_file!("table_resource_ver"));

        self.init_table(sql_file!("table_guild"));
        self.init_table(sql_file!("table_guild_users"));

        self.init_table(sql_file!("table_channel"));
        self.init_table(sql_file!("table_invite"));
        self.init_table(sql_file!("table_dm_recipients"));
        self.init_table(sql_file!("table_read_state"));

        self.init_table(sql_file!("table_message"));

        self.init_table(sql_file!("table_attachment"));
        self.init_table(sql_file!("table_msg_attaches"));

        self.init_table(sql_file!("table_outer_msg"));
        self.init_table(sql_file!("table_outer_user"));
        self.init_table(sql_file!("table_outer_channel"));
    }

    fn init_table(&self, sql: &str) {
        let _ = self
            .conn
            .execute(sql, ())
            .inspect_err(|e| println!("Create table error: {e:?}"));
    }
}
