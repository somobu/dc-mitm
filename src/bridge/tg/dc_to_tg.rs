use conversion::OuterMessageId;
use teloxide::prelude::*;
use teloxide::types::{InputFile, InputMedia, InputMediaDocument, InputMediaPhoto};

use crate::eventbus::{ChannelEvent, Event};
use crate::AppData;

use super::{conversion, EXT_ID};

pub(crate) async fn fwd_dc_to_tg(bot: teloxide::Bot, data: AppData) {
    let mut sub = data.ebus.lock().await.subscribe();
    loop {
        match sub.recv().await.unwrap() {
            Event::ChannelEvent {
                source,
                channel,
                event,
            } => {
                if source.is_some_and(|e| e == EXT_ID) {
                    // Suppress echo
                    continue;
                }

                let ch = data
                    .db
                    .lock()
                    .await
                    .get_channel_outer_id(EXT_ID, channel)
                    .unwrap();
                if ch.is_none() {
                    continue;
                }

                let (chat_id, thread_id) = conversion::to_tg_channel_id(&ch.unwrap());

                match event {
                    ChannelEvent::MessageCreate(message, attachments, hash_map) => {
                        let author = hash_map.get(&message.author).unwrap();

                        let text = format!(
                            "<b>{}</b>\n{}",
                            author.global_name(),
                            conversion::to_crappy_html(&message.text)
                        );

                        let out_message;
                        if attachments.is_empty() {
                            let mut send_rq = bot.send_message(chat_id, text);
                            send_rq.message_thread_id = thread_id;
                            send_rq.parse_mode = Some(teloxide::types::ParseMode::Html);

                            out_message = send_rq.await.unwrap();
                        } else {
                            let mut media: Vec<InputMedia> = attachments
                                .iter()
                                .map(|a| {
                                    let tg_attach = InputFile::file(a.cdn_path());

                                    if a.width > 0 {
                                        InputMedia::Photo(InputMediaPhoto::new(tg_attach))
                                    } else {
                                        InputMedia::Document(InputMediaDocument::new(tg_attach))
                                    }
                                })
                                .collect();

                            match &mut media[0] {
                                InputMedia::Photo(input) => {
                                    input.caption = Some(text);
                                    input.parse_mode = Some(teloxide::types::ParseMode::Html);
                                }
                                InputMedia::Document(input) => {
                                    input.caption = Some(text);
                                    input.parse_mode = Some(teloxide::types::ParseMode::Html);
                                }
                                _ => {}
                            }

                            let mut send_rq = bot.send_media_group(chat_id, media);
                            send_rq.message_thread_id = thread_id;

                            let msgs = send_rq.await.unwrap();
                            out_message = msgs[0].clone();
                        }

                        data.db
                            .lock()
                            .await
                            .add_msg_link(
                                EXT_ID,
                                message.id,
                                conversion::as_outer_message_id(&out_message),
                            )
                            .unwrap();
                    }

                    ChannelEvent::MessageUpdate(message, _vec, hash_map) => {
                        let message_id = data
                            .db
                            .lock()
                            .await
                            .get_msg_outer_id(EXT_ID, message.id)
                            .unwrap()
                            .unwrap();

                        let outer_mid = OuterMessageId::from_string(&message_id);
                        let message_id = outer_mid.message_id;

                        let author = hash_map.get(&message.author).unwrap();

                        let text = format!(
                            "<b>{} (edited)</b>\n{}",
                            author.global_name(),
                            conversion::to_crappy_html(&message.text)
                        );

                        if outer_mid.is_media {
                            let mut send_rq = bot.edit_message_caption(chat_id, message_id);
                            send_rq.caption = Some(text);
                            send_rq.parse_mode = Some(teloxide::types::ParseMode::Html);
                            send_rq.await.unwrap();
                        } else {
                            let mut send_rq = bot.edit_message_text(chat_id, message_id, text);
                            send_rq.parse_mode = Some(teloxide::types::ParseMode::Html);
                            send_rq.await.unwrap();
                        }
                    }
                    ChannelEvent::MessageDelete(message) => {
                        let db = data.db.lock().await;
                        let message_id = db.get_msg_outer_id(EXT_ID, message.id).unwrap().unwrap();

                        let outer_mid = OuterMessageId::from_string(&message_id);
                        let message_id = outer_mid.message_id;

                        // For now bridge doesnt handle message groups at all, so when message group has more
                        // than one attach bridge will delete only first of them in TG.
                        //
                        // This looks really stupid, so for now we'll delete messages with 1 attach only.
                        let has_multiple_attaches =
                            db.get_attaches_of_msg(message.id).unwrap().len() > 1;

                        if !has_multiple_attaches {
                            bot.delete_message(chat_id, message_id).await.unwrap();
                        }
                    }

                    _ => {}
                }
            }
            _ => {}
        }
    }
}
