use teloxide::types::{ChatId, MediaText, Message, MessageEntityKind, MessageId, ThreadId};

use crate::{db, structures::snowflake::Snowflake};

// Text conversion issues:
// - DC->TG: spoiler, mention, emoji, link conversion is not performed
// - TG->DC: link, emoji conversion is not performed
// - TG->DC: markdown-within-single-word problem

#[derive(Debug)]
pub struct OuterMessageId {
    pub chat_id: ChatId,
    pub message_id: MessageId,
    pub is_media: bool,
}

impl OuterMessageId {
    const VERSION: u8 = 2;

    pub fn from_string(s: &str) -> Self {
        let data: Vec<&str> = s.split("/").into_iter().collect();

        if data.len() == 2 {
            return OuterMessageId {
                chat_id: ChatId(data[0].parse().unwrap()),
                message_id: MessageId(data[1].parse().unwrap()),
                is_media: false,
            };
        }

        let version: u8 = data[0].parse().unwrap();
        match version {
            2 => OuterMessageId {
                chat_id: ChatId(data[1].parse().unwrap()),
                message_id: MessageId(data[2].parse().unwrap()),
                is_media: {
                    let media_u8: u8 = data[3].parse().unwrap();
                    media_u8 > 0
                },
            },
            _ => panic!("Unknown OuterMessageId version {version} found in {s}"),
        }
    }

    pub fn from_message(msg: &Message) -> Self {
        Self {
            chat_id: msg.chat.id,
            message_id: msg.id,
            is_media: msg.media_group_id().is_some(),
        }
    }

    pub fn to_string(&self) -> String {
        let media_u8 = if self.is_media { 1 } else { 0 };
        format!(
            "{}/{}/{}/{}",
            Self::VERSION,
            self.chat_id.0,
            self.message_id.0,
            media_u8
        )
    }
}

pub fn get_tg_chat_id(msg: &Message) -> String {
    let chat_id = msg.chat.id.0;
    let thread_id = msg.thread_id.unwrap_or(ThreadId(MessageId(0)));
    format!("{}/{}", chat_id.to_string(), thread_id.to_string())
}

pub fn as_outer_message_id(msg: &Message) -> String {
    OuterMessageId::from_message(msg).to_string()
}

pub fn to_tg_channel_id(s: &str) -> (ChatId, Option<ThreadId>) {
    let i = s.find("/").unwrap();
    let chat_id: i64 = s[..i].parse().unwrap();
    let thread_id: i32 = s[(i + 1)..].parse().unwrap();

    (
        ChatId(chat_id),
        if thread_id == 0 {
            None
        } else {
            Some(ThreadId(MessageId(thread_id)))
        },
    )
}

pub fn upsert_tg_user(db: &db::DB, from: &teloxide::types::User) -> Result<Snowflake, ()> {
    let tg_uid = from.id.0.to_string();
    let uid = db.get_user_own_id(super::EXT_ID, &tg_uid).unwrap();

    if uid.is_some() {
        return Ok(uid.unwrap());
    } else {
        let username = format!(
            "{} {}",
            from.first_name,
            from.last_name.as_ref().unwrap_or(&"".to_string())
        );

        let uid = Snowflake::new_with_id(super::EXT_WORKER);

        let user = db::user::User {
            id: uid,
            username: format!("gen_{}", uid.to_string()),
            email: format!("{tg_uid}@tg.host"),
            display_name: Some(username),
            avatar: None,
            is_bot: true,
        };

        db.add_user(user).unwrap();
        db.add_user_link(super::EXT_ID, uid, tg_uid).unwrap();

        return Ok(uid);
    }
}

pub fn to_crappy_html(input: &str) -> String {
    let parser = pulldown_cmark::Parser::new(input);

    let mut list_started_right_now = false;
    let mut list_depth: u32 = 0;

    let parser = parser.filter_map(|event| match event {
        pulldown_cmark::Event::Start(tag) => match tag {
            pulldown_cmark::Tag::List(_) => {
                list_depth += 1;

                if list_depth == 1 {
                    list_started_right_now = true;
                }

                None
            }
            pulldown_cmark::Tag::Item => {
                let mut gap = String::new();
                for _ in 1..list_depth {
                    gap += "  ";
                }

                let newline = if list_started_right_now { "" } else { "\n" };

                list_started_right_now = false;

                Some(pulldown_cmark::Event::Text(
                    format!("{newline}{gap}* ").into(),
                ))
            }
            pulldown_cmark::Tag::FootnoteDefinition(_) => None,
            pulldown_cmark::Tag::DefinitionList => None,
            pulldown_cmark::Tag::DefinitionListTitle => None,
            pulldown_cmark::Tag::DefinitionListDefinition => None,
            pulldown_cmark::Tag::Table(_) => Some(pulldown_cmark::Event::Text("\n\n".into())),
            pulldown_cmark::Tag::TableHead => Some(pulldown_cmark::Event::Text("\n| ".into())),
            pulldown_cmark::Tag::TableRow => Some(pulldown_cmark::Event::Text("\n| ".into())),
            pulldown_cmark::Tag::TableCell => Some(pulldown_cmark::Event::Text(" | ".into())),
            pulldown_cmark::Tag::Image {
                link_type: _,
                dest_url,
                title,
                id: _,
            } => Some(pulldown_cmark::Event::Text(
                format!("![{title}]({dest_url})").into(),
            )),
            pulldown_cmark::Tag::MetadataBlock(_) => None,
            pulldown_cmark::Tag::Paragraph => {
                if list_depth == 0 {
                    Some(pulldown_cmark::Event::Start(tag))
                } else {
                    None
                }
            }
            _ => Some(pulldown_cmark::Event::Start(tag)),
        },
        pulldown_cmark::Event::End(tag) => match tag {
            pulldown_cmark::TagEnd::List(_) => {
                list_depth -= 1;

                if list_depth == 0 {
                    Some(pulldown_cmark::Event::Text("\n\n".into()))
                } else {
                    None
                }
            }
            pulldown_cmark::TagEnd::Item => None,
            pulldown_cmark::TagEnd::FootnoteDefinition => None,
            pulldown_cmark::TagEnd::DefinitionList => None,
            pulldown_cmark::TagEnd::DefinitionListTitle => None,
            pulldown_cmark::TagEnd::DefinitionListDefinition => None,
            pulldown_cmark::TagEnd::Table => Some(pulldown_cmark::Event::Text("\n".into())),
            pulldown_cmark::TagEnd::TableHead => Some(pulldown_cmark::Event::Text(" | ".into())),
            pulldown_cmark::TagEnd::TableRow => Some(pulldown_cmark::Event::Text(" | ".into())),
            pulldown_cmark::TagEnd::TableCell => None,
            pulldown_cmark::TagEnd::Image => None,
            pulldown_cmark::TagEnd::MetadataBlock(_) => None,
            pulldown_cmark::TagEnd::Paragraph => {
                if list_depth == 0 {
                    Some(pulldown_cmark::Event::End(tag))
                } else {
                    None
                }
            }
            _ => Some(pulldown_cmark::Event::End(tag)),
        },
        _ => Some(event),
    });

    let mut vec = vec![];
    for item in parser {
        match item {
            pulldown_cmark::Event::Text(cow_str) => {
                let new_str = cow_str;
                let l = vec.last_mut();
                if l.is_some() {
                    let l = l.unwrap();
                    match l {
                        pulldown_cmark::Event::Text(old_str) => {
                            let s = old_str.to_string() + &new_str.to_string();
                            vec.pop().unwrap();
                            vec.push(pulldown_cmark::Event::Text(s.into()))
                        }
                        _ => vec.push(pulldown_cmark::Event::Text(new_str)),
                    }
                }
            }
            _ => vec.push(item),
        }
    }

    let mut html_output = String::new();
    pulldown_cmark::html::push_html(&mut html_output, vec.into_iter());

    // html_output
    html_output
        .replace("<h1>", "\n# ")
        .replace("<h2>", "\n## ")
        .replace("<h3>", "\n### ")
        .replace("<h4>", "\n#### ")
        .replace("<h5>", "\n##### ")
        .replace("<h6>", "\n###### ")
        .replace("</h1>", "\n\n")
        .replace("</h2>", "\n\n")
        .replace("</h3>", "\n\n")
        .replace("</h4>", "\n\n")
        .replace("</h5>", "\n\n")
        .replace("</h6>", "\n\n")
        .replace("<hr />", "\n--------\n")
        .replace("<p>", "")
        .replace("</p>", "\n")
}

pub fn from_crappy_spanned(input: &MediaText) -> String {
    if input.entities.len() == 0 {
        return input.text.clone();
    }

    // Step 1: merge spans
    let mut src = input.entities.clone();
    let mut merged_spans = vec![];

    while src.len() > 0 {
        let mut current = src[0].clone();
        src = src
            .into_iter()
            .skip(1)
            .filter(|next| {
                let same_type = current.kind == next.kind;
                let continous = (current.offset + current.length) >= next.offset;

                if same_type && continous {
                    current.length += next.length;
                    false
                } else {
                    true
                }
            })
            .collect();

        merged_spans.push(current);
    }

    // Step 2: spans -> events
    #[derive(Debug)]
    struct Event {
        position: usize,
        opener: bool,
        kind: MessageEntityKind,
    }

    let mut events: Vec<Event> = merged_spans
        .iter()
        .flat_map(|e| {
            [
                Event {
                    position: e.offset,
                    opener: true,
                    kind: e.kind.clone(),
                },
                Event {
                    position: e.offset + e.length,
                    opener: false,
                    kind: e.kind.clone(),
                },
            ]
        })
        .collect();

    events.sort_by_key(|e| e.position);

    // Step 3: build md string
    let in_string = &input.text;
    let mut out_string = String::new();

    let mut openers: Vec<&Event> = vec![];
    let mut pos = 0;
    let mut idx = 0;

    while idx < events.len() {
        let current_pos = events[idx].position;
        out_string += &in_string[pos..current_pos];

        // Step 3.1: collect events at this position
        let mut idx2 = idx;
        while idx2 < events.len() && events[idx2].position == current_pos {
            idx2 += 1;
        }

        let this_events: Vec<&Event> = events[idx..idx2].iter().map(|e| e).collect();

        // Step 3.2: iterate over closers
        if openers.len() > 0 {
            for opener_idx in (0..openers.len()).rev() {
                let opening_event = openers[opener_idx];

                let closing_event = this_events
                    .iter()
                    .find(|e| !e.opener && e.kind == opening_event.kind);

                if closing_event.is_some() {
                    out_string += md_wrapper(&opening_event.kind);
                    openers.remove(opener_idx);
                }
            }
        }

        // Step 3.3: Iterate over openers
        for this_event in this_events {
            if this_event.opener {
                out_string += md_wrapper(&this_event.kind);
                openers.push(this_event);
            }
        }

        pos = current_pos;
        idx = idx2;
    }

    out_string += &in_string[pos..];

    out_string
}

fn md_wrapper(k: &MessageEntityKind) -> &str {
    match k {
        MessageEntityKind::Mention => "",
        MessageEntityKind::Hashtag => "",
        MessageEntityKind::Cashtag => "",
        MessageEntityKind::BotCommand => "",
        MessageEntityKind::Url => "",
        MessageEntityKind::Email => "",
        MessageEntityKind::PhoneNumber => "",
        MessageEntityKind::Bold => "**",
        MessageEntityKind::Blockquote => "`",
        MessageEntityKind::Italic => "*",
        MessageEntityKind::Underline => "__",
        MessageEntityKind::Strikethrough => "~~",
        MessageEntityKind::Spoiler => "||",
        MessageEntityKind::Code => "```",
        MessageEntityKind::Pre { language: _ } => "`",
        MessageEntityKind::TextLink { url: _ } => "",
        MessageEntityKind::TextMention { user: _ } => "",
        MessageEntityKind::CustomEmoji { custom_emoji_id: _ } => "",
    }
}

#[cfg(test)]
mod test {
    use teloxide::types::{MediaText, MessageEntity, MessageEntityKind};

    use super::from_crappy_spanned;

    use super::to_crappy_html;

    #[test]
    fn crappy_html() {
        let src = r#"""
# h1 Heading 8-)
## h2 Heading
### h3 Heading
#### h4 Heading
##### h5 Heading
###### h6 Heading


## Horizontal Rules

___

---

***


## Emphasis

**This is bold text**

__This is bold text__

*This is italic text*

_This is italic text_

~~Strikethrough~~


## Blockquotes


> Blockquotes can also be nested...
>> ...by using additional greater-than signs right next to each other...
> > > ...or with spaces between arrows.


## Lists

Unordered 1
* Aaaa;
* Bbbb;
* Cccc;

Unordered 2

+ Create a list by starting a line with `+`, `-`, or `*`
+ Sub-lists are made by indenting 2 spaces:
  - Marker character change forces new list start:
    * Ac tristique libero volutpat at
    + Facilisis in pretium nisl aliquet
    - Nulla volutpat aliquam velit
+ Very easy!

Ordered

1. Lorem ipsum dolor sit amet
2. Consectetur adipiscing elit
3. Integer molestie lorem at massa


1. You can use sequential numbers...
1. ...or keep all the numbers as `1.`

Start numbering with offset:

57. foo
1. bar


## Tables

| Option | Description |
| ------ | ----------- |
| data   | path to data files to supply the data that will be passed into templates. |
| engine | engine to be used for processing templates. Handlebars is the default. |
| ext    | extension to be used for dest files. |

"""#;

        println!("\n\n\n{}", to_crappy_html(src));
    }

    #[test]
    fn crappy_spanned() {
        let t = MediaText {
            text: "itaboldita".to_owned(),
            entities: vec![
                MessageEntity {
                    kind: MessageEntityKind::Italic,
                    offset: 0,
                    length: 3,
                },
                MessageEntity {
                    kind: MessageEntityKind::Bold,
                    offset: 3,
                    length: 7,
                },
                MessageEntity {
                    kind: MessageEntityKind::Italic,
                    offset: 3,
                    length: 7,
                },
            ],
            link_preview_options: None,
        };

        println!("{}", from_crappy_spanned(&t));
    }
}
