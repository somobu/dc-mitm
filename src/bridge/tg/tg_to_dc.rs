use std::collections::HashMap;

use teloxide::{net::Download, prelude::*};

use crate::eventbus::{ChannelEvent, Event};
use crate::structures::snowflake::Snowflake;
use crate::{cdn, db, AppData};

use super::{conversion, EXT_ID, EXT_WORKER};

pub(crate) async fn fwd_tg_to_dc(bot: teloxide::Bot, data: AppData) {
    let handler = dptree::entry()
        .branch(Update::filter_message().endpoint(fwd_msg_create))
        .branch(Update::filter_edited_message().endpoint(fwd_msg_update));

    Dispatcher::builder(bot, handler)
        .dependencies(dptree::deps![data])
        .build()
        .dispatch()
        .await
}

async fn fwd_msg_create(bot: Bot, data: AppData, msg: Message) -> Result<(), ()> {
    let date = msg.date;

    match &msg.kind {
        teloxide::types::MessageKind::Common(message) => {
            // Extract data from teloxide shits

            // Commented out due to horrific ownership problems
            // let is_a_reply_msg = message
            //     .reply_to_message
            //     .is_some_and(|e| e.id.0 != e.thread_id.unwrap_or(ThreadId(MessageId(0))).0 .0);

            let from = msg.from.as_ref().unwrap();

            let uid = from.id.0;
            let cid = conversion::get_tg_chat_id(&msg);
            let mid = conversion::as_outer_message_id(&msg);

            println!(
                "{} {cid} {} {} ({uid}): {:?}",
                date.to_string(),
                from.first_name,
                from.last_name.as_ref().unwrap_or(&"?".to_string()),
                message.media_kind
            );

            // Conversions and checks
            let channel = data
                .db
                .lock()
                .await
                .get_channel_own_id(EXT_ID, &cid)
                .unwrap();

            if channel.is_none() {
                return Ok(());
            }

            let created_millis = msg.date.timestamp_millis() as u64;
            let own_id = Snowflake::new_from_millis(created_millis, EXT_WORKER);

            let channel = channel.unwrap();
            let author = {
                let db = data.db.lock().await;
                conversion::upsert_tg_user(&db, from).unwrap()
            };

            let text = match &message.media_kind {
                teloxide::types::MediaKind::Text(media_text) => {
                    conversion::from_crappy_spanned(media_text)
                }
                teloxide::types::MediaKind::Sticker(sticker) => {
                    let sticker = &sticker.sticker;
                    format!(
                        "```Sticker: {}```",
                        sticker.emoji.as_ref().unwrap_or(&"".to_string())
                    )
                }
                teloxide::types::MediaKind::Photo(photo) => {
                    photo.caption.clone().unwrap_or_default()
                }
                _ => format!(
                    "```Bridge is unable to handle {:?} (yet)```",
                    &message.media_kind
                ),
            };

            let mut attaches: Vec<db::attachment::Attachment> = vec![];

            match &message.media_kind {
                teloxide::types::MediaKind::Photo(photo) => {
                    let attach_id = Snowflake::new();

                    let mut max_photo = &photo.photo[0];
                    for v in &photo.photo {
                        if v.file.size > max_photo.file.size {
                            max_photo = v;
                        }
                    }
                    let f = &max_photo.file.id;

                    let file = bot.get_file(f).await.unwrap();

                    let filename = format!(
                        "server/attachments/{}/{}",
                        channel.to_string(),
                        attach_id.to_string()
                    );
                    let mut dst = tokio::fs::File::create(&filename).await.unwrap();

                    bot.download_file(&file.path, &mut dst).await.unwrap();

                    let d = std::fs::read(&filename).unwrap();
                    let (width, height) = cdn::get_image_props(&d).unwrap_or((0, 0));

                    attaches.push(db::attachment::Attachment {
                        id: attach_id,
                        channel,
                        width,
                        height,
                        size: d.len(),
                        name: Some("unknown.png".to_string()),
                    });
                }
                _ => {}
            }

            // Prepare and send broadcast message
            let db = data.db.lock().await;

            let old_message = db.get_message(own_id);
            let should_update = old_message.is_ok();

            let db_msg;
            if old_message.is_err() {
                db_msg = db.add_message(own_id, author, channel, text).unwrap();
                db.add_msg_link(EXT_ID, own_id, mid).unwrap();
            } else {
                db_msg = old_message.unwrap();
            }

            for attach in &attaches {
                db.create_attachment(
                    attach.id,
                    attach.channel,
                    attach.width,
                    attach.height,
                    attach.size,
                    attach.name.clone(),
                )
                .unwrap();

                db.link_attach_to_msg(db_msg.id, attach.id).unwrap();
            }

            let mut users = HashMap::new();
            users.insert(author, db.user(author).unwrap());

            let attaches = db.get_attaches_of_msg(db_msg.id).unwrap();

            if should_update {
                data.ebus.lock().await.publish(Event::ChannelEvent {
                    source: Some(EXT_ID.to_string()),
                    channel,
                    event: ChannelEvent::MessageUpdate(db_msg, attaches, users),
                });
            } else {
                data.ebus.lock().await.publish(Event::ChannelEvent {
                    source: Some(EXT_ID.to_string()),
                    channel,
                    event: ChannelEvent::MessageCreate(db_msg, attaches, users),
                });
            }
        }
        _ => { /* Non-message events is out of interest (for now) */ }
    }

    Ok(())
}

async fn fwd_msg_update(_bot: Bot, data: AppData, msg: Message) -> Result<(), ()> {
    match &msg.kind {
        teloxide::types::MessageKind::Common(message) => {
            let mid = conversion::as_outer_message_id(&msg);

            let own_id = data.db.lock().await.get_msg_own_id(EXT_ID, &mid).unwrap();
            if own_id.is_none() {
                return Ok(());
            }

            let own_id = own_id.unwrap();

            let text = match &message.media_kind {
                teloxide::types::MediaKind::Text(media_text) => {
                    conversion::from_crappy_spanned(media_text)
                }
                teloxide::types::MediaKind::Sticker(sticker) => {
                    let sticker = &sticker.sticker;
                    format!(
                        "```Sticker: {}```",
                        sticker.emoji.as_ref().unwrap_or(&"".to_string())
                    )
                }
                teloxide::types::MediaKind::Photo(photo) => {
                    photo.caption.clone().unwrap_or_default()
                }
                _ => format!(
                    "```Bridge is unable to handle {:?} (yet)```",
                    &message.media_kind
                ),
            };

            let mut old_msg = data.db.lock().await.get_message(own_id).unwrap();
            old_msg.edited_at = message.edit_date.as_ref().unwrap().timestamp_millis() as u64;
            old_msg.text = text.clone();

            data.db.lock().await.update_message(own_id, text).unwrap();

            let mut users = HashMap::new();
            users.insert(
                old_msg.author,
                data.db.lock().await.user(old_msg.author).unwrap(),
            );

            data.ebus.lock().await.publish(Event::ChannelEvent {
                source: Some(EXT_ID.to_string()),
                channel: old_msg.author,
                event: ChannelEvent::MessageUpdate(old_msg, vec![], users),
            });
        }
        _ => { /* Non-message events is out of interest (for now) */ }
    }

    Ok(())
}
