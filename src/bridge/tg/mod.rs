use std::collections::HashMap;

use teloxide::prelude::*;

use crate::{config::TgBotConfig, AppData};

mod conversion;
mod dc_to_tg;
mod tg_to_dc;

pub const EXT_ID: &str = "TG";
pub const EXT_WORKER: u16 = 255;

pub async fn start(data: AppData) {
    println!("[TG] Starting bot...");

    let cfg = data.config.bridge_tg.as_ref();

    if cfg.is_none() {
        let mut map = HashMap::new();
        map.insert("bridge_tg".to_string(), TgBotConfig::default());

        let demo = serde_json::to_string_pretty(&map).unwrap();

        println!("--------------");
        println!("Warning: TG bot config not found! Consider adding this setion to server config:");
        println!("{demo}");
        println!("--------------");

        return;
    }

    let bot = Bot::new(cfg.unwrap().token.clone());

    println!("[TG] Listening for new events...");

    loop {
        let _ = tokio::join!(tokio::spawn(inner_cycle(bot.clone(), data.clone())));
        println!("[TG] Crashed. Restarting.");
    }
}

async fn inner_cycle(bot: Bot, data: AppData) {
    tokio::select!(
        _ = tg_to_dc::fwd_tg_to_dc(bot.clone(), data.clone()) => {},
        _ = dc_to_tg::fwd_dc_to_tg(bot, data) => {}
    )
}
