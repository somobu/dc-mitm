use std::time::Duration;

use crate::AppData;

#[cfg(feature = "bridge-tg")]
mod tg;

#[allow(unused_variables)]
pub async fn server(data: AppData) -> Result<(), ()> {
    #[cfg(feature = "bridge-tg")]
    {
        println!("Launching bridge: telegram...");
        tokio::spawn(tg::start(data.clone()));
    }

    #[cfg(not(feature = "bridge-tg"))]
    {
        if data.config.bridge_tg.is_some() {
            println!("--------------");
            println!("Warning: 'bridge_tg' config section is present but 'bridge-tg' feature is not enabled in this build!");
            println!("Consider re-build (re-run) project w/ `--features bridge-tg` argument set.");
            println!("--------------");
        }
    }


    tokio::time::sleep(Duration::from_secs(u64::MAX)).await;

    Ok(())
}
