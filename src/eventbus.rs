//! ...
//!
//! How it is used:
//! 1. HttpApi/GW modifies data in DB;
//! 2. HttpApi/GW also triggers this event bus;
//! 3. Subscribers (GW) gets notified;
//! 4. ???
//! 5. Profit!

use std::collections::HashMap;

use tokio::sync::broadcast;

use crate::db;
use crate::structures::{resource::member::GuildMember, snowflake::Snowflake};

pub struct EBus {
    tx: tokio::sync::broadcast::Sender<Event>,
}

impl EBus {
    pub fn new() -> Self {
        let (tx, _) = tokio::sync::broadcast::channel::<Event>(16);

        Self { tx }
    }

    pub fn publish(&self, evt: Event) {
        let _ = self.tx.send(evt);
    }

    pub fn subscribe(&self) -> broadcast::Receiver<Event> {
        self.tx.subscribe()
    }
}

/// Event bus message
#[derive(Debug, Clone)]
pub enum Event {
    /// Something happened at the guild (e.g.: guild name has changed)
    GuildEvent { guild: Snowflake, event: GuildEvent },

    /// Something happend at the channel (e.g. message was created)
    ChannelEvent {
        /// - `None` when sent by server itself
        /// - `Some(ext_id)` when sent by extension (e.g. bridge-tg)
        source: Option<String>,

        channel: Snowflake,
        event: ChannelEvent,
    },

    /// Something happend to certain user (e.g. user gone online)
    UserEvent {
        user: db::user::User,
        event: UserEvent,
    },

    /// Some event that should be sent to every session of the same user
    SelfEvent {
        user_id: Snowflake,
        event: SelfEvent,
    },
}

#[derive(Debug, Clone)]
pub enum GuildEvent {
    GuildCreate {
        owner: db::user::User,
        guild: db::guild::Guild,
        channels: Vec<db::channel::Channel>,
    },

    GuildUpdate {
        guild: db::guild::Guild,
    },

    GuildDelete {
        guild: db::guild::Guild,
    },

    UserJoin {
        user: db::user::User,
        guild: db::guild::Guild,
        channels: Vec<db::channel::Channel>,
    },

    UserLeave {
        user: db::user::User,
        guild: db::guild::Guild,
    },
}

/// Channel-wide events
#[derive(Debug, Clone)]
pub enum ChannelEvent {
    ChannelCreate(db::channel::Channel),
    ChannelUpdate(db::channel::Channel),
    ChannelDelete(db::channel::Channel),

    MessageCreate(
        db::message::Message,
        Vec<db::attachment::Attachment>,
        HashMap<Snowflake, db::user::User>,
    ),
    MessageUpdate(
        db::message::Message,
        Vec<db::attachment::Attachment>,
        HashMap<Snowflake, db::user::User>,
    ),
    MessageDelete(db::message::Message),

    TypingStart {
        guild_id: Option<Snowflake>,
        user_id: Snowflake,
        member: Option<GuildMember>,
    },
}

#[derive(Debug, Clone)]
pub enum UserEvent {
    StatusChange { status: &'static str },
    Updated,
}

#[derive(Debug, Clone)]
pub enum SelfEvent {
    MessageAck {
        version: u32,
        channel_id: Snowflake,
        message_id: Snowflake,
        last_viewed: Option<u32>,
        manual: bool,
        mention_count: u32,
    },
}
