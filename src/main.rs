use std::time::{SystemTime, UNIX_EPOCH};

use actix_cors::Cors;
use actix_web::web::{self, Data};
use actix_web::{self, middleware::Logger};
use actix_web::{middleware, App, HttpServer, Scope};
use base64::Engine;
use chrono::{DateTime, Utc};
use eventbus::EBus;
use tokio::sync::Mutex;

mod api;
mod bridge;
mod db;
mod gw;
mod structures;

mod assets;
mod config;
mod eventbus;

#[cfg(feature = "cdn")]
mod cdn;

#[cfg(feature = "voice")]
pub mod voice;

pub type AppData = Data<AppDataStruct>;

pub struct AppDataStruct {
    pub config: config::Config,
    pub db: Mutex<db::DB>,

    pub ebus: Mutex<EBus>,

    // TODO: rename to gw
    pub sessions: Mutex<gw::GW>,
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    env_logger::init_from_env(env_logger::Env::new().default_filter_or("info"));

    std::fs::create_dir_all("server").unwrap();

    let config_file = "server/config.json";

    let config = std::fs::read_to_string(config_file);
    let config: config::Config = if config.is_ok() {
        serde_json::from_str(&config.unwrap()).unwrap()
    } else {
        let cfg = config::Config::default();
        let cfg_str = serde_json::to_string_pretty(&cfg).unwrap();
        std::fs::write(config_file, &cfg_str).unwrap();
        println!("Created default config at {config_file}");
        cfg
    };

    // Ensure data dir is here
    std::fs::create_dir_all(&config.main.assets_directory).unwrap();

    let data = web::Data::new(AppDataStruct {
        config,
        db: Mutex::new(db::DB::open_file("./server/dcas.sqlite").unwrap()),
        ebus: Mutex::new(EBus::new()),
        sessions: Mutex::new(gw::GW::new()),
    });

    data.db.lock().await.init_tables();

    tokio::select! {
        _ = tokio::signal::ctrl_c() => { println!("Got CTRL-C, shutting down (ungracefully)") }
        _ = main_server(data.clone()) => { println!("Main server has died!") },
        // _ = server::gateway::server(data.clone()) => { println!("Gateway server has died!") },
        _ = gw::server(data.clone()) => { println!("Gateway server has died!") },
        _ = cdn_server(data.clone()) => { println!("CDN server has died!") },
        _ = bridge::server(data.clone()) => { println!("Bridge has died!") },
        // _ = server::voice::server(data.clone()) => {},
    }

    Ok(())
}

pub async fn main_server(data: AppData) -> std::io::Result<()> {
    let d0 = data.clone();
    HttpServer::new(move || {
        App::new()
            .app_data(d0.clone())
            .wrap(crate::logger())
            .wrap(Cors::permissive())
            .wrap(middleware::NormalizePath::trim())
            .app_data(web::PayloadConfig::new(8 * 1024 * 1024))
            .service(assets::assets)
            .service(Scope::new("/api/v9").configure(api::configure))
            .service(Scope::new("/api").configure(api::configure))
            .default_service(web::to(assets::index))
    })
    .bind((data.config.main.bind.clone(), data.config.main.port))?
    .run()
    .await
}

#[cfg(feature = "cdn")]
async fn cdn_server(data: AppData) -> std::io::Result<()> {
    cdn::server(data).await
}

#[cfg(not(feature = "cdn"))]
async fn cdn_server(_data: AppData) -> std::io::Result<()> {
    use std::{time::Duration, u64};

    println!("Warning: running without 'cdn' feature");

    tokio::time::sleep(Duration::from_secs(u64::MAX)).await;

    Ok(())
}

pub fn logger() -> Logger {
    Logger::new(r#"%s "%r""#)
}

pub fn millis() -> u64 {
    SystemTime::now()
        .duration_since(UNIX_EPOCH)
        .unwrap()
        .as_millis() as u64
}

pub fn millis_to_iso8601(millis: u64) -> String {
    let dt: DateTime<Utc> = DateTime::from_timestamp_millis(millis as i64).unwrap();
    format!("{}", dt.format("%+"))
}

pub fn b64_encode(v: &[u8]) -> String {
    base64::engine::general_purpose::STANDARD.encode(v)
}

pub fn b64_decode(v: &str) -> Result<Vec<u8>, base64::DecodeError> {
    base64::engine::general_purpose::STANDARD.decode(v)
}
