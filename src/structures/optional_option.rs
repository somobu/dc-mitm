use serde::{de::DeserializeOwned, Deserialize};
use serde_json::Value;

/// In JSON, each fild in dict actually may be in these states:
/// 1. `"field": "value"`
/// 2. `"field": null`
/// 3. field not present in dict
///
/// This struct reflects all this states.
///
/// Usage:
/// ```
/// #[derive(Serialize, Deserialize)]
/// struct DatStruct {
///     #[serde(default)]
///     #[serde(skip_serializing_if = "OptionalOption::is_absent")]
///     pub field: OptionalOption<u32>,
/// }
/// ```
#[derive(Debug, Clone)]
pub enum OptionalOption<T> {
    Absent,
    Null,
    Some(T),
}

#[allow(dead_code)]
impl<T> OptionalOption<T> {
    pub fn is_absent(&self) -> bool {
        matches!(self, OptionalOption::Absent)
    }
}

impl<T> Default for OptionalOption<T> {
    fn default() -> Self {
        Self::Absent
    }
}

impl<'de, T> Deserialize<'de> for OptionalOption<T>
where
    T: DeserializeOwned,
{
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        let v = Value::deserialize(deserializer).unwrap();
        let t: Option<T> = serde_json::from_value(v).unwrap();

        Ok(match t {
            None => Self::Null,
            Some(e) => Self::Some(e),
        })
    }
}
