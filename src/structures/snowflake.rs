use std::time::{SystemTime, UNIX_EPOCH};

use chrono::{DateTime, Utc};
use rusqlite::{types::FromSql, ToSql};
use serde::{Deserialize, Serialize};

pub const DC_BIRHDAY: u64 = 1420070400000;

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct Snowflake(u64);

impl Snowflake {
    pub fn new() -> Snowflake {
        Self::new_with_id(0)
    }

    pub fn new_with_id(id: u16) -> Snowflake {
        let millis = SystemTime::now()
            .duration_since(UNIX_EPOCH)
            .unwrap()
            .as_millis();
        Self::new_from_millis(millis as u64, id)
    }

    pub fn new_from_millis(millis: u64, id: u16) -> Snowflake {
        let start = (millis - DC_BIRHDAY) << 22;
        Snowflake(start + id as u64)
    }

    pub fn millis(&self) -> u64 {
        (self.0 >> 22) + DC_BIRHDAY
    }

    pub fn iso8601(&self) -> String {
        let dt: DateTime<Utc> = DateTime::from_timestamp_millis(self.millis() as i64).unwrap();
        format!("{}", dt.format("%+"))
    }

    pub fn to_string(&self) -> String {
        self.into()
    }
}

impl From<&str> for Snowflake {
    fn from(value: &str) -> Self {
        Snowflake(value.parse().unwrap())
    }
}

impl From<String> for Snowflake {
    fn from(value: String) -> Self {
        Snowflake(value.parse().unwrap())
    }
}

impl From<&String> for Snowflake {
    fn from(value: &String) -> Self {
        Snowflake(value.parse().unwrap())
    }
}

impl From<u64> for Snowflake {
    fn from(value: u64) -> Self {
        Self(value)
    }
}

impl FromSql for Snowflake {
    fn column_result(value: rusqlite::types::ValueRef<'_>) -> rusqlite::types::FromSqlResult<Self> {
        Ok(Snowflake(value.as_i64()? as u64))
    }
}

impl Into<String> for Snowflake {
    fn into(self) -> String {
        self.0.to_string()
    }
}

impl Into<String> for &Snowflake {
    fn into(self) -> String {
        self.0.to_string()
    }
}

impl Into<u64> for Snowflake {
    fn into(self) -> u64 {
        self.0
    }
}

impl ToSql for Snowflake {
    fn to_sql(&self) -> rusqlite::Result<rusqlite::types::ToSqlOutput<'_>> {
        assert!(self.0 < i64::MAX as u64);

        Ok(rusqlite::types::ToSqlOutput::Owned(
            rusqlite::types::Value::Integer(self.0 as i64),
        ))
    }
}

impl Serialize for Snowflake {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        let s: String = self.into();
        String::serialize(&s, serializer)
    }
}

impl<'de> Deserialize<'de> for Snowflake {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        Ok(Snowflake::from(String::deserialize(deserializer)?))
    }
}
