use std::collections::{HashMap, HashSet};

use presence::Presence;
use serde::{Deserialize, Serialize};

use crate::db;

use super::{
    resource::{
        channel, guild,
        member::{self, GuildMember},
        message, user,
    },
    snowflake::Snowflake,
};

pub mod hello;
pub mod identify;
pub mod presence;
pub mod ready;
pub mod resume;
pub mod voice_state;

/// Thing that exists but is not specified yet.
pub type Stub = String;

#[derive(Debug, Serialize, Deserialize)]
pub struct GatewayEvent {
    /// Sequence number (present in Ack and Dispatch only)
    #[serde(default)]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub s: Option<u64>,

    #[serde(flatten)]
    pub base_data: BaseData,

    #[serde(flatten)]
    pub dispatch: Option<DispatchData>,
}

impl GatewayEvent {
    pub fn hello() -> GatewayEvent {
        GatewayEvent {
            s: None,
            base_data: BaseData::Hello(hello::Hello {
                heartbeat_interval: 45045,
                _trace: vec!["localhost".to_string()],
            }),
            dispatch: None,
        }
    }

    pub fn ack() -> GatewayEvent {
        GatewayEvent {
            s: None,
            base_data: BaseData::HeartbeatAck,
            dispatch: None,
        }
    }

    pub fn invalid_session(recoverable: bool) -> GatewayEvent {
        GatewayEvent {
            s: None,
            base_data: BaseData::InvalidSession(recoverable),
            dispatch: None,
        }
    }

    pub fn dispatch(data: DispatchData) -> GatewayEvent {
        GatewayEvent {
            s: None,
            base_data: BaseData::Dispatch,
            dispatch: Some(data),
        }
    }
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(tag = "op", content = "d")]
pub enum BaseData {
    #[serde(rename = 0)]
    Dispatch,

    #[serde(rename = 1)]
    Heartbeat(u32),

    #[serde(rename = 2)]
    Identify(identify::Identify),

    #[serde(rename = 3)]
    PresenceUpdate(PresenceUpdate),

    /// Sent from client when it wants to connect/disconnect to a Voice Server
    #[serde(rename = 4)]
    UpdateVoiceState(voice_state::UpdateVoiceState),

    #[serde(rename = 5)]
    Unknown5,

    #[serde(rename = 6)]
    Resume(resume::Resume),

    #[serde(rename = 7)]
    Reconnect,

    #[serde(rename = 8)]
    RequestGuildMembers(RequestGuildMembers),

    #[serde(rename = 9)]
    InvalidSession(bool),

    #[serde(rename = 10)]
    Hello(hello::Hello),

    #[serde(rename = 11)]
    HeartbeatAck,

    #[serde(rename = 13)]
    Unknown13(Unknown13),

    #[serde(rename = 36)]
    Unknown36(Unknown36),

    #[serde(rename = 37)]
    GuildSubscriptionsBulk(GuildSubscriptionsBulk),
}

#[allow(non_camel_case_types)]
#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(tag = "t", content = "d")]
pub enum DispatchData {
    READY(ready::Ready),
    RESUMED(ready::Resumed),

    GUILD_CREATE(guild::GatewayGuild),
    GUILD_UPDATE(guild::NormalGuild),
    GUILD_DELETE(guild::UnavailableGuild),

    GUILD_MEMBER_ADD(member::GuildMember),
    GUILD_MEMBER_UPDATE(member::GuildMember),
    GUILD_MEMBER_REMOVE(GuildMemberRemove),

    GUILD_MEMBER_LIST_UPDATE(GuildMemberListUpdate),
    GUILD_MEMBERS_CHUNK(GuildMembersChunk),

    CHANNEL_CREATE(channel::GuildChannel),
    CHANNEL_UPDATE(channel::GuildChannel),
    CHANNEL_DELETE(channel::GuildChannel),

    TYPING_START(TypingStart),

    MESSAGE_CREATE(message::MessageWithExtras),
    MESSAGE_UPDATE(message::MessageWithExtras),
    MESSAGE_DELETE(MessageDelete),

    MESSAGE_ACK(MessageAck),

    USER_UPDATE(user::User),
    PRESENCE_UPDATE(presence::Presence),

    VOICE_STATE_UPDATE(voice_state::VoiceState),
    VOICE_SERVER_UPDATE(voice_state::VoiceServerUpdate),
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Unknown13 {
    pub channel_id: Snowflake,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Unknown36 {
    pub guild_id: Snowflake,
}

/// Sent by client
#[derive(Debug, Serialize, Deserialize)]
pub struct GuildSubscriptionsBulk {
    /// Maps GuildID -> Data
    pub subscriptions: HashMap<Snowflake, SubscriptionInfo>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct SubscriptionInfo {
    // Unimplemented at all
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct GuildMemberRemove {
    pub guild_id: Snowflake,
    pub user: crate::structures::resource::user::User,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct RequestGuildMembers {
    pub guild_id: Vec<String>,
    pub user_ids: Vec<String>,
    pub presences: bool,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct GuildMembersChunk {
    pub guild_id: String,

    pub chunk_index: u32,
    pub chunk_count: u32,

    pub members: Vec<member::GuildMember>,
    pub presences: Vec<presence::PresenceStub>,

    pub not_found: Vec<Stub>,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct GuildMemberListUpdate {
    pub id: String,
    pub guild_id: String,
    pub member_count: u32,
    pub online_count: u32,
    pub ops: Vec<GuildMemberListUpdateOperation>,
    pub groups: Vec<GuildMemberListUpdateGroup>,
}

impl GuildMemberListUpdate {
    pub fn new(
        guild_id: Snowflake,
        members: &Vec<db::user::User>,
        active_users: &HashSet<Snowflake>,
    ) -> Self {
        let now_online: Vec<Snowflake> = members
            .iter()
            .map(|e| e.id)
            .filter(|e| active_users.contains(e))
            .collect();

        let mbrs = members.iter().map(|user| {
            let uid = user.id;
            let status = if now_online.contains(&uid) {
                "online".to_string()
            } else {
                "offline".to_string()
            };

            (
                uid,
                GuildMemberListUpdateOpItem::member(GuildMember {
                    presence: Some(Presence::new(uid, &status)),
                    ..GuildMember::new(guild_id, &user)
                }),
            )
        });

        let mut mbrs: Vec<GuildMemberListUpdateOpItem> = mbrs
            .clone()
            .filter_map(|(s, d)| now_online.contains(&s).then_some(d))
            .chain(mbrs.filter_map(|(s, d)| (!now_online.contains(&s)).then_some(d)))
            .collect();

        let mut groups: Vec<GuildMemberListUpdateGroup> = vec![];
        if now_online.len() > 0 {
            mbrs.insert(0, GuildMemberListUpdateOpItem::g_online());
            groups.push(GuildMemberListUpdateGroup::online(now_online.len() as u32));
        }

        if now_online.len() < members.len() {
            mbrs.insert(
                now_online.len() + if now_online.len() > 0 { 1 } else { 0 },
                GuildMemberListUpdateOpItem::g_offline(),
            );

            groups.push(GuildMemberListUpdateGroup::offline(
                (members.len() - now_online.len()) as u32,
            ));
        }

        GuildMemberListUpdate {
            id: "everyone".to_string(),
            guild_id: guild_id.into(),
            member_count: members.len() as u32,
            online_count: now_online.len() as u32,
            ops: vec![GuildMemberListUpdateOperation::sync(mbrs)],
            groups,
        }
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct GuildMemberListUpdateGroup {
    pub id: String,
    pub count: u32,
}

impl GuildMemberListUpdateGroup {
    pub fn online(count: u32) -> Self {
        GuildMemberListUpdateGroup {
            id: "online".to_string(),
            count,
        }
    }

    pub fn offline(count: u32) -> Self {
        GuildMemberListUpdateGroup {
            id: "offline".to_string(),
            count,
        }
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct GuildMemberListUpdateOperation {
    pub range: (u32, u32),
    pub op: String,
    pub items: Vec<GuildMemberListUpdateOpItem>,
}

impl GuildMemberListUpdateOperation {
    pub fn sync(mbrs: Vec<GuildMemberListUpdateOpItem>) -> Self {
        GuildMemberListUpdateOperation {
            range: (0, 99),
            op: "SYNC".to_string(),
            items: mbrs,
        }
    }
}

#[allow(non_camel_case_types)]
#[derive(Debug, Clone, Serialize, Deserialize)]
pub enum GuildMemberListUpdateOpItem {
    group(GuildMemberListUpdateOpItemGroup),
    member(member::GuildMember),
}

impl GuildMemberListUpdateOpItem {
    pub fn g_online() -> Self {
        GuildMemberListUpdateOpItem::group(GuildMemberListUpdateOpItemGroup {
            id: "online".to_string(),
        })
    }

    pub fn g_offline() -> Self {
        GuildMemberListUpdateOpItem::group(GuildMemberListUpdateOpItemGroup {
            id: "offline".to_string(),
        })
    }
}

#[allow(non_camel_case_types)]
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct GuildMemberListUpdateOpItemGroup {
    pub id: String,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct MessageDelete {
    pub id: Snowflake,
    pub channel_id: Snowflake,
    pub guild_id: Option<Snowflake>,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct MessageAck {
    pub version: u32,
    pub channel_id: Snowflake,
    pub message_id: Snowflake,
    pub last_viewed: Option<u32>,
    pub flags: u32,

    #[serde(skip_serializing_if = "is_false")]
    pub manual: bool,

    #[serde(skip_serializing_if = "is_zero")]
    pub mention_count: u32,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub ack_type: Option<u32>,
}

fn is_false(v: &bool) -> bool {
    !v
}

fn is_zero(v: &u32) -> bool {
    *v == 0
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct TypingStart {
    pub channel_id: Snowflake,
    pub user_id: Snowflake,

    /// Unix timestamp in seconds
    pub timestamp: u64,

    pub guild_id: Option<Snowflake>,
    pub member: Option<GuildMember>,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct PresenceUpdate {
    pub status: String,
    pub since: u64,

    // pub activities: Vec<Stub>,
    pub afk: bool,
}
