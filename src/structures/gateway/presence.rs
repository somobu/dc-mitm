use serde::{Deserialize, Serialize};

use crate::structures::{
    gateway::Stub,
    resource::user::{User, UserSmallest},
    snowflake::Snowflake,
};

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Presence {
    pub user: UserSmallest,
    pub guild_id: Option<Snowflake>,
    pub status: String,

    pub activities: Vec<Stub>,
    pub client_status: ClientStatus,
    pub broadcast: Option<Stub>,
}

impl Presence {
    pub fn new(uid: Snowflake, status: &str) -> Self {
        Presence {
            user: UserSmallest { id: uid },
            status: status.to_string(),
            activities: vec![],
            client_status: ClientStatus {
                web: Some(status.to_string()),
            },
            broadcast: None,
            guild_id: None,
        }
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct PresenceStub {
    pub user: User,
    pub guild_id: Snowflake,
    pub status: String,

    pub activities: Vec<Stub>,
    pub client_status: ClientStatus,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct ClientStatus {
    pub web: Option<String>,
}
