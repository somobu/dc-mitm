use serde::{Deserialize, Serialize};

#[allow(non_camel_case_types)]
#[derive(Debug, Serialize, Deserialize)]
pub struct Identify {
    pub token: String,
    
    #[serde(default)]
    pub compress: bool,

    #[serde(default)]
    pub capabilities: u64,
    // TODO: Incomplete
}