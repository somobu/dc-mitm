use serde::{Deserialize, Serialize};

use crate::structures::resource::member::MergedGuildMember;
use crate::structures::resource::user::{SelfUser, User};
use crate::structures::resource::{channel::PrivateChannel, guild::GatewayGuild};
use crate::structures::snowflake::Snowflake;

use super::Stub;

#[allow(non_camel_case_types)]
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Ready {
    pub v: u32,

    pub users: Vec<User>,

    /// Base64-encoded settings protobuf
    pub user_settings_proto: String,

    pub user_guild_settings: WeirdVec<Stub>,
    pub user: SelfUser,
    pub tutorial: Option<Stub>,

    /// UUID
    pub static_client_session_id: String,

    pub sessions: Vec<Stub>,
    pub session_type: String,
    pub session_id: String,
    pub resume_gateway_url: String,

    pub relationships: Vec<Stub>,
    pub read_state: WeirdVec<ChannelReadState>,
    pub private_channels: Vec<PrivateChannel>,
    pub notification_settings: NotificationSettings,

    pub guilds: Vec<GatewayGuild>,

    /// Shares positions with `guilds` field
    pub merged_members: Vec<Vec<MergedGuildMember>>,
    pub guild_join_requests: Vec<Stub>,
    pub guild_experiments: Vec<Stub>,

    pub geo_ordered_rtc_regions: Vec<String>,

    pub friend_suggestion_count: u32,
    pub explicit_content_scan_version: u32,

    pub experiments: Vec<Stub>,

    pub country_code: String,

    pub consents: Consents,

    pub connected_accounts: Vec<Stub>,
    pub auth_session_id_hash: String,

    pub api_code_version: u32,
    pub analytics_token: String,
    pub _trace: Vec<String>,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct WeirdVec<T> {
    pub version: u32,
    pub partial: bool,
    pub entries: Vec<T>,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct NotificationSettings {
    pub flags: u32,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Consents {}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Resumed {
    pub _trace: Vec<String>,
}

impl Resumed {
    pub fn new() -> Self {
        Self {
            _trace: vec!["localhost".to_string()],
        }
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct ChannelReadState {
    /// Channel id
    pub id: Snowflake,

    /// Channel type
    /// - 0 DM;
    /// - 1 channel;
    /// - 2 thread;
    pub flags: u32,

    /// Id of last read message
    pub last_message_id: Snowflake,

    pub mention_count: u32,

    /// Calculated as `last view day since discord epoch + 1`
    pub last_viewed: Option<u32>,

    /// Seen: `"1970-01-01T00:00:00+00:00"` and `"2021-01-01T12:00:00+00:00"`
    pub last_pin_timestamp: String,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct AnotherReadState {
    /// May be '1' or '2'
    pub read_state_type: u8,

    pub last_acked_id: String,
    pub id: String,
    pub badge_count: u32,
}
