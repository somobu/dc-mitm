use serde::{Deserialize, Serialize};

#[allow(non_camel_case_types)]
#[derive(Debug, Serialize, Deserialize)]
pub struct Resume {
    pub token: String,
    pub session_id: Option<String>,
    pub seq: u64,
}