use serde::{Deserialize, Serialize};

#[allow(non_camel_case_types)]
#[derive(Debug, Serialize, Deserialize)]
pub struct Hello {
    pub heartbeat_interval: u32,
    pub _trace: Vec<String>,
}
