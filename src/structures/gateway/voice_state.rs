use serde::{Deserialize, Serialize};

use crate::structures::{resource::member::GuildMember, snowflake::Snowflake};

/// Sent by client
#[allow(non_camel_case_types)]
#[derive(Debug, Serialize, Deserialize)]
pub struct UpdateVoiceState {
    pub guild_id: Option<Snowflake>,
    pub channel_id: Option<Snowflake>,
    pub self_mute: bool,
    pub self_deaf: bool,
    pub self_video: Option<bool>,
    pub flags: Option<u32>,
}

/// Sent by server
#[allow(non_camel_case_types)]
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct VoiceState {
    pub guild_id: Option<Snowflake>,
    pub channel_id: Option<Snowflake>,
    pub user_id: Snowflake,
    pub member: Option<GuildMember>,
    pub session_id: String,

    pub mute: bool,
    pub deaf: bool,
    pub suppress: bool,

    pub self_mute: bool,
    pub self_deaf: bool,
    pub self_stream: Option<bool>,
    pub self_video: Option<bool>,

    pub request_to_speak_timestamp: Option<String>,
}

#[allow(non_camel_case_types)]
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct VoiceServerUpdate {
    pub token: String,
    pub guild_id: Option<Snowflake>,
    pub channel_id: Option<Snowflake>,
    pub endpoint: Option<String>,
}
