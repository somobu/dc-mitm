use std::collections::HashMap;

use serde::{Deserialize, Serialize};

use crate::structures::{gateway::Stub, snowflake::Snowflake};

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Role {
    pub id: Snowflake,
    pub name: String,
    pub position: u32,
    pub permissions: String,

    pub mentionable: bool,
    pub managed: bool,

    pub icon: Option<Stub>,
    pub color: u32,
    pub unicode_emoji: Option<Stub>,

    pub tags: HashMap<Stub, Stub>,
    pub hoist: bool,

    pub flags: u32,
}
