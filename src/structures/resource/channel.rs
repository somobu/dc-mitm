//! Channel data type
//!
//! Paritally based on [discord.sex](https://docs.discord.sex/resources/channel#channel-type) definitions.

use rusqlite::{types::FromSql, ToSql};
use serde::{Deserialize, Serialize};
use serde_repr::{Deserialize_repr, Serialize_repr};

use crate::db;
use crate::structures::{gateway::Stub, snowflake::Snowflake};

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct GuildChannel {
    pub guild_id: Option<Snowflake>,

    pub id: Snowflake,
    pub r#type: ChannelType,
    pub flags: u32,

    pub name: String,

    pub parent_id: Option<Snowflake>,
    pub position: u32,
    pub topic: Option<String>,

    pub rate_limit_per_user: u32,
    pub permission_overwrites: Vec<Stub>,

    pub last_message_id: Option<Snowflake>,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub last_pin_timestamp: Option<String>,

    pub nsfw: bool,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub icon_emoji: Option<IconEmoji>,
}

impl GuildChannel {
    pub fn new(c: &db::channel::Channel) -> Self {
        GuildChannel {
            guild_id: c.guild_id,
            id: c.id.into(),
            r#type: c.r#type,
            name: c.name.clone(),
            flags: 0,
            parent_id: c.parent_id,
            position: c.position,
            topic: c.topic.clone(),
            rate_limit_per_user: 0,
            permission_overwrites: vec![],
            last_pin_timestamp: None,
            last_message_id: c.last_message_id,
            nsfw: c.nsfw,
            icon_emoji: None,
        }
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct IconEmoji {
    pub name: String,
    pub id: Option<Stub>,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct PrivateChannel {
    pub id: Snowflake,
    pub r#type: ChannelType,
    pub flags: u32,

    pub recipient_ids: Vec<Snowflake>,
    pub last_message_id: Option<Snowflake>,

    pub is_spam: bool,
    pub is_message_request: bool,
    pub is_message_request_timestamp: Option<Stub>,

    pub safety_warnings: Vec<Stub>,
}

impl PrivateChannel {
    pub fn new(
        channel: &db::channel::Channel,
        recipients: Vec<db::user::User>,
        last_message_id: Option<Snowflake>,
    ) -> Self {
        PrivateChannel {
            id: channel.id.into(),
            r#type: channel.r#type,
            flags: 0,
            recipient_ids: recipients.iter().map(|e| e.id).collect(),
            last_message_id,
            is_spam: false,
            is_message_request: false,
            is_message_request_timestamp: None,
            safety_warnings: vec![],
        }
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct InviteChannel {
    pub id: Snowflake,
    pub r#type: ChannelType,
    pub name: String,
}

impl InviteChannel {
    pub fn new(c: &db::channel::Channel) -> Self {
        Self {
            id: c.id,
            r#type: c.r#type,
            name: c.name.clone(),
        }
    }
}

#[allow(non_camel_case_types)]
#[derive(Debug, Clone, Copy, PartialEq, Serialize_repr, Deserialize_repr)]
#[repr(u8)]
pub enum ChannelType {
    /// A text channel within a guild
    GUILD_TEXT = 0,

    /// A private channel between two users
    DM = 1,

    /// A voice channel within a guild
    GUILD_VOICE = 2,

    /// A private channel between multiple users
    GROUP_DM = 3,

    /// An organizational category that contains up to 50 channels
    GUILD_CATEGORY = 4,

    /// Almost identical to GUILD_TEXT, a channel that users can follow and crosspost into their own guild
    GUILD_NEWS = 5,

    /// A channel in which game developers can sell their game on Discord
    GUILD_STORE = 6,

    /// A channel where users can match up for various games
    ///
    /// Unsupported in official client (as of 2024)
    #[deprecated]
    GUILD_LFG = 7,

    /// A private channel between multiple users for a group within an LFG channel
    ///
    /// Unsupported in official client (as of 2024)
    #[deprecated]
    LFG_GROUP_DM = 8,

    /// The first iteration of the threads feature, never widely used
    ///
    /// Unsupported in official client (as of 2024)
    #[deprecated]
    THREAD_ALPHA = 9,

    /// A temporary sub-channel within a GUILD_NEWS channel
    NEWS_THREAD = 10,

    /// a temporary sub-channel within a GUILD_TEXT, GUILD_FORUM, or GUILD_MEDIA channel
    PUBLIC_THREAD = 11,

    /// a temporary sub-channel within a GUILD_TEXT channel that is only viewable by those invited and those with the MANAGE_THREADS permission
    PRIVATE_THREAD = 12,

    /// A voice channel for hosting events with an audience in a guild
    GUILD_STAGE_VOICE = 13,

    /// The main channel in a hub containing the listed guilds
    GUILD_DIRECTORY = 14,

    /// A channel that can only contain threads
    GUILD_FORUM = 15,

    /// A channel that can only contain threads in a gallery view
    GUILD_MEDIA = 16,
}

impl FromSql for ChannelType {
    fn column_result(value: rusqlite::types::ValueRef<'_>) -> rusqlite::types::FromSqlResult<Self> {
        let v = value.as_i64()?;

        assert!(v >= 0 && v <= 16);

        let v = v as u8;

        let v = unsafe { (&v as *const u8 as *const ChannelType).as_ref().unwrap() };

        Ok(*v)
    }
}

impl ToSql for ChannelType {
    fn to_sql(&self) -> rusqlite::Result<rusqlite::types::ToSqlOutput<'_>> {
        let l = (*self) as u8;
        Ok(rusqlite::types::ToSqlOutput::Owned(
            rusqlite::types::Value::Integer(
                i64::try_from(l)
                    .map_err(|err| rusqlite::Error::ToSqlConversionFailure(err.into()))?,
            ),
        ))
    }
}
