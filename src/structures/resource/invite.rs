use serde::{Deserialize, Serialize};

use crate::{db, structures::gateway::Stub};

use super::{channel::InviteChannel, guild::InviteGuild, user::User};

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Invite {
    pub r#type: u32,
    pub code: String,
    pub inviter: User,
    pub expires_at: Option<Stub>,
    pub flags: u32,
    pub guild: InviteGuild,
    pub guild_id: String,
    pub channel: InviteChannel,
    pub approximate_member_count: u32,
    pub approximate_presence_count: u32,
}

impl Invite {
    pub fn new(
        i: &db::invite::Invite,
        g: &db::guild::Guild,
        c: &db::channel::Channel,
        u: &db::user::User,
    ) -> Invite {
        Invite {
            r#type: 0,
            code: i.code.clone(),
            inviter: User::new(u),
            expires_at: None,
            flags: 0,
            guild: InviteGuild::new(g),
            guild_id: i.guild.unwrap().into(),
            channel: InviteChannel::new(c),
            approximate_member_count: 0,
            approximate_presence_count: 0,
        }
    }
}
