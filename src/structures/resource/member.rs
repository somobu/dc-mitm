use serde::{Deserialize, Serialize};

use crate::{
    db,
    structures::{
        gateway::{presence::Presence, Stub},
        snowflake::Snowflake,
    },
};

use super::user::User;

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct GuildMember {
    pub guild_id: Snowflake,
    pub user: User,

    /// Sent at GUILD_MEMBER_LIST_UPDATE
    pub presence: Option<Presence>,

    pub roles: Vec<String>,

    pub nick: String,
    pub avatar: Option<Snowflake>,

    pub pending: bool,
    pub joined_at: String,
    pub premium_since: Option<Stub>,
    pub communication_disabled_until: Option<Stub>,

    pub mute: bool,
    pub deaf: bool,

    pub flags: u32,
}

impl GuildMember {
    pub fn new(guild_id: Snowflake, u: &db::user::User) -> Self {
        GuildMember {
            guild_id,
            user: User::new(u),
            presence: None,
            roles: vec![],
            nick: u.global_name(),
            avatar: u.avatar.clone(),
            pending: false,
            joined_at: u.id.iso8601(),
            premium_since: None,
            communication_disabled_until: None,
            mute: false,
            deaf: false,
            flags: 0,
        }
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct MergedGuildMember {
    pub user_id: String,
    pub roles: Vec<String>,

    pub nick: String,
    pub avatar: Option<Snowflake>,

    pub pending: bool,
    pub joined_at: String,
    pub premium_since: Option<Stub>,
    pub communication_disabled_until: Option<Stub>,

    pub mute: bool,
    pub deaf: bool,

    pub flags: u32,
}

impl MergedGuildMember {
    pub fn new(u: &db::user::User) -> Self {
        MergedGuildMember {
            user_id: u.id.into(),
            roles: vec![],
            nick: u.global_name(),
            avatar: u.avatar.clone(),
            pending: false,
            joined_at: u.id.iso8601(),
            premium_since: None,
            communication_disabled_until: None,
            mute: false,
            deaf: false,
            flags: 0,
        }
    }
}
