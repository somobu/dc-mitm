use std::collections::HashMap;

use serde::{Deserialize, Serialize};

use crate::structures::{gateway::Stub, snowflake::Snowflake};
use crate::{db, millis};

use super::{channel::GuildChannel, role::Role};

/// A partial guild object. Represents an offline guild, or a guild the client is not connected to yet
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct UnavailableGuild {
    /// The ID of the guild
    pub id: Snowflake,

    /// Whether the guild is unavailable due to an outage
    pub unavailable: Option<bool>,

    pub geo_restricted: Option<bool>,
}

impl UnavailableGuild {
    pub fn new(guild_id: Snowflake) -> Self {
        Self {
            id: guild_id,
            unavailable: None,
            geo_restricted: None,
        }
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct GatewayGuild {
    pub version: u32,

    pub id: Snowflake,
    pub properties: PartialGuild,
    pub lazy: bool,
    pub lage: bool,
    pub joined_at: String,
    pub data_mode: String,

    pub channels: Vec<GuildChannel>,
    pub threads: Vec<Stub>,
    pub roles: Vec<Role>,
    pub members: Vec<Stub>,
    pub voice_states: Vec<Stub>,
    pub presences: Vec<Stub>,

    pub member_count: u32,
    pub premium_subscription_count: u32,

    pub stickers: Vec<Stub>,
    pub emojis: Vec<Stub>,

    pub stage_instances: Vec<Stub>,
    pub guild_scheduled_events: Vec<Stub>,
    pub activity_instances: Vec<Stub>,
    pub embedded_activities: Vec<Stub>,
    pub application_command_counts: ApplicationCommandCounts,
}

impl GatewayGuild {
    pub fn new(g: &db::guild::Guild, c: &Vec<db::channel::Channel>) -> Self {
        let channels: Vec<GuildChannel> = c.iter().map(|e| GuildChannel::new(e)).collect();

        GatewayGuild {
            version: millis() as u32,
            id: g.id.into(),
            properties: PartialGuild::new(g),
            lazy: false,
            lage: false,
            joined_at: g.id.iso8601(),
            data_mode: "full".to_string(),
            channels,
            threads: vec![],
            roles: vec![Role {
                id: g.id.into(),
                name: "@everyone".to_string(),
                position: 0,
                permissions: "18446744073709551615".to_string(),
                mentionable: true,
                managed: false,
                icon: None,
                color: 0,
                unicode_emoji: None,
                tags: HashMap::new(),
                hoist: false,
                flags: 0,
            }],
            member_count: 2,
            premium_subscription_count: 100,
            stickers: vec![],
            emojis: vec![],
            stage_instances: vec![],
            guild_scheduled_events: vec![],
            activity_instances: vec![],
            application_command_counts: ApplicationCommandCounts {},
            voice_states: vec![],
            members: vec![],
            presences: vec![],
            embedded_activities: vec![],
        }
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct ApplicationCommandCounts {}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct NormalGuild {
    #[serde(flatten)]
    pub properties: PartialGuild,

    pub embed_channel_id: Option<Snowflake>,
    pub embed_enabled: bool,

    pub emojis: Vec<Stub>,

    pub inventory_settings: Option<Stub>,

    pub max_presences: Option<Stub>,

    pub premium_subscription_count: u32,

    pub region: String,

    pub roles: Vec<Stub>,

    pub stickers: Vec<Stub>,

    pub widget_channel_id: Option<Snowflake>,
    pub widget_enabled: bool,
}

impl NormalGuild {
    pub fn new(g: &db::guild::Guild) -> Self {
        NormalGuild {
            properties: PartialGuild::new(g),
            embed_channel_id: None,
            embed_enabled: false,
            emojis: vec![],
            inventory_settings: None,
            max_presences: None,
            premium_subscription_count: 0,
            region: "hongkong".to_string(),
            roles: vec![],
            stickers: vec![],
            widget_channel_id: None,
            widget_enabled: false,
        }
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct PartialGuild {
    pub id: Snowflake,
    pub name: String,
    pub icon: Option<Snowflake>,
    pub owner_id: String,

    pub features: Vec<Stub>,

    pub max_members: u32,
    pub max_video_channel_users: u32,
    pub max_stage_video_channel_users: u32,

    pub premium_tier: u32,
    pub banner: Option<Stub>,
    pub home_header: Option<Stub>,
    pub description: Option<Stub>,
    pub splash: Option<Stub>,
    pub discovery_splash: Option<Stub>,
    pub clan: Option<Stub>,
    pub vanity_url_code: Option<Stub>,

    pub system_channel_id: Option<String>,
    pub system_channel_flags: u32,
    pub afk_channel_id: Option<Stub>,
    pub public_updates_channel_id: Option<Stub>,
    pub safety_alerts_channel_id: Option<Stub>,
    pub rules_channel_id: Option<Stub>,

    pub verification_level: u32,
    pub nsfw_level: u32,

    pub application_id: Option<Stub>,
    pub mfa_level: u32,
    pub incidents_data: Option<Stub>,
    pub afk_timeout: u32,
    pub premium_progress_bar_enabled: bool,
    pub hub_type: Option<Stub>,
    pub default_message_notifications: u32,
    pub latest_onboarding_question_id: Option<Stub>,
    pub explicit_content_filter: u32,
    pub preferred_locale: String,
    pub nsfw: bool,
}

impl PartialGuild {
    pub fn new(g: &db::guild::Guild) -> Self {
        PartialGuild {
            id: g.id.into(),
            name: g.name.clone(),
            icon: g.icon.clone(),
            owner_id: g.owner_id.into(),

            features: vec![],
            max_members: 500_000,
            max_video_channel_users: 25,
            max_stage_video_channel_users: 50,
            premium_tier: 3,
            banner: None,
            home_header: None,
            description: None,
            splash: None,
            discovery_splash: None,
            clan: None,
            vanity_url_code: None,
            system_channel_id: None,
            system_channel_flags: 12,
            afk_channel_id: None,
            public_updates_channel_id: None,
            safety_alerts_channel_id: None,
            rules_channel_id: None,
            verification_level: 0,
            nsfw_level: 0,
            application_id: None,
            mfa_level: 0,
            incidents_data: None,
            afk_timeout: 300,
            premium_progress_bar_enabled: false,
            hub_type: None,
            default_message_notifications: 0,
            latest_onboarding_question_id: None,
            explicit_content_filter: 0,
            preferred_locale: "en-US".to_string(),
            nsfw: false,
        }
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct InviteGuild {
    pub id: Snowflake,
    pub name: String,
    pub banner: Option<String>,
    pub description: Option<String>,
    pub icon: Option<Snowflake>,
    pub features: Vec<String>,
    pub verification_level: u32,
    pub vanity_url_code: Option<Stub>,
    pub nsfw_level: u32,
    pub nsfw: bool,
    pub premium_subscription_count: u32,
}

impl InviteGuild {
    pub fn new(g: &db::guild::Guild) -> Self {
        Self {
            id: g.id,
            name: g.name.clone(),
            banner: None,
            description: None,
            icon: g.icon.clone(),
            features: vec![],
            verification_level: 0,
            vanity_url_code: None,
            nsfw_level: 0,
            nsfw: false,
            premium_subscription_count: 0,
        }
    }
}
