use serde::{Deserialize, Serialize};

use crate::{
    db,
    structures::{gateway::Stub, snowflake::Snowflake},
};

/// Seen in Presence Update
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct UserSmallest {
    pub id: Snowflake,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct User {
    pub id: Snowflake,

    pub username: String,
    pub global_name: Option<String>,
    pub display_name: String,
    pub discriminator: String,

    pub avatar: Option<Snowflake>,
    pub avatar_decoration_data: Option<Stub>,

    pub public_flags: u32,
    pub bot: bool,
    pub clan: Option<Stub>,
    // "primary_guild": null,
    // "flags": 0,

    // "banner": null,
    // "banner_color": "#111214",
    // "accent_color": 1118740,

    // "bio": "text"
}

impl User {
    pub fn new(user: &db::user::User) -> Self {
        Self {
            id: user.id,
            username: user.username.clone(),
            global_name: user.display_name.clone(),
            display_name: user.global_name(),
            discriminator: "0".to_string(),
            avatar: user.avatar,
            avatar_decoration_data: None,
            public_flags: 0,
            bot: user.is_bot,
            clan: None,
        }
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct SelfUser {
    pub id: Snowflake,

    pub username: String,
    pub global_name: String,
    // Display name?
    pub discriminator: String,

    pub avatar: Option<Snowflake>,
    pub avatar_decoration_data: Option<Stub>,

    // Public flags?
    pub clan: Option<Stub>,

    pub verified: bool,
    pub purchased_flags: u32,
    pub pronouns: String,
    pub premium_type: u32,
    pub premium: bool,
    pub phone: Option<Stub>,
    pub nsfw_allowed: bool,
    pub mobile: bool,
    pub mfa_enabled: bool,
    pub flags: u32,
    pub email: String,
    pub desktop: bool,
    pub bio: String,
    pub banner_color: String,
    pub banner: Option<Stub>,
    pub accent_color: u32,
}

impl SelfUser {
    pub fn new(u: &db::user::User) -> Self {
        SelfUser {
            verified: true,
            username: u.username.clone(),
            purchased_flags: 0,
            pronouns: "".to_string(),
            premium_type: 0,
            premium: false,
            phone: None,
            nsfw_allowed: true,
            mobile: true,
            mfa_enabled: false,
            id: u.id.into(),
            global_name: u.global_name(),
            flags: 0,
            email: u.email.clone(),
            discriminator: "0".to_string(),
            desktop: true,
            clan: None,
            bio: "".to_string(),
            banner_color: "#111214".to_string(),
            banner: None,
            avatar_decoration_data: None,
            avatar: u.avatar.clone(),
            accent_color: 1118740,
        }
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct UserProfile {
    pub user: User,

    pub connected_accounts: Vec<Stub>,
    pub premium_since: Option<Stub>,
    pub premium_type: Option<Stub>,
    pub premium_guild_since: Option<Stub>,
    pub user_profile: UserProfileInner,

    pub badges: Vec<Stub>,
    pub guild_badges: Vec<Stub>,

    pub mutual_friends: Vec<Stub>,
    pub mutual_guilds: Vec<Stub>,
    // guild_member: <guild member data>
    // guild_member_profile: <...>
    // legacy_username: String
}

impl UserProfile {
    pub fn new(user: &db::user::User) -> Self {
        Self {
            user: User::new(user),
            connected_accounts: vec![],
            premium_since: None,
            premium_type: None,
            premium_guild_since: None,
            user_profile: UserProfileInner::new(),
            badges: vec![],
            guild_badges: vec![],
            mutual_friends: vec![],
            mutual_guilds: vec![],
        }
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct UserProfileInner {
    pub bio: Option<String>,
    pub accent_color: u32,
    pub pronouns: Option<String>,
}

impl UserProfileInner {
    pub fn new() -> Self {
        Self {
            bio: None,
            accent_color: 0,
            pronouns: None,
        }
    }
}
