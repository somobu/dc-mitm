use serde::{Deserialize, Serialize};

#[derive(Debug, Default, Serialize, Deserialize)]
pub struct AttachmentUpload {
    pub attachments: Vec<AttachmentUploadEntry>,
}

#[derive(Debug, Default, Serialize, Deserialize)]
pub struct AttachmentUploadEntry {
    pub id: u32,
    pub upload_filename: String,
    pub upload_url: String,
}