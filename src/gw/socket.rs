use std::io::Write;

use flate2::write::ZlibEncoder;
use futures_util::stream::{SplitSink, SplitStream};
use futures_util::{SinkExt, StreamExt};
use tokio::net::TcpStream;
use tokio_tungstenite::tungstenite::protocol::{frame::coding::CloseCode, CloseFrame};
use tokio_tungstenite::tungstenite::Message;
use tokio_tungstenite::WebSocketStream;

use crate::structures::gateway::GatewayEvent;

type RX = SplitStream<WebSocketStream<TcpStream>>;
type TX = SplitSink<WebSocketStream<TcpStream>, Message>;

const ZLIB_BUF_LIMIT: usize = 16 * 1024 * 1024;

pub struct Socket {
    rx: RX,
    tx: TX,
    cm: CompressionMode,

    zs: Option<ZlibEncoder<Vec<u8>>>,
}

#[derive(Debug)]
pub enum CompressionMode {
    Uncompressed,
    ZlibStream,
}

impl Socket {
    pub fn new(stream: WebSocketStream<TcpStream>, cm: CompressionMode) -> Self {
        let (tx, rx) = stream.split();

        let zs = if matches!(cm, CompressionMode::ZlibStream) {
            Some(ZlibEncoder::new(Vec::new(), flate2::Compression::default()))
        } else {
            None
        };

        Self { rx, tx, cm, zs }
    }

    pub async fn receive(&mut self) -> Option<GatewayEvent> {
        let m = self.rx.next().await?.ok()?;
        let m = match m {
            Message::Text(t) => Some(t),
            _ => None,
        }?;

        println!("Received: {m}");

        let l = serde_json::from_str(&m);
        Some(l.unwrap())
    }

    pub async fn send(&mut self, event: &GatewayEvent) {
        let r = serde_json::to_string(event).unwrap();

        // Behold the MEME:
        // https://stackoverflow.com/a/51983601
        let subs = if r.len() < 300 {
            &r[..]
        } else {
            let end = r.char_indices().map(|(i, _)| i).nth(300).unwrap_or(r.len());
            &r[0..end]
        };

        println!("Sending ({:?}) {subs}", self.cm,);

        match self.cm {
            CompressionMode::Uncompressed => {
                self.tx.send(Message::Text(r)).await.unwrap();
            }
            CompressionMode::ZlibStream => {
                let zs = self.zs.as_mut().unwrap();
                let s = zs.get_ref().len();

                zs.write_all(r.as_bytes()).unwrap();
                zs.flush().unwrap();

                let compressed_bytes = &zs.get_ref()[s..];

                let m = Message::Binary(Vec::from(compressed_bytes));
                self.tx.send(m).await.unwrap();

                if zs.get_ref().len() > ZLIB_BUF_LIMIT {
                    log::warn!("Dropping client due to Zlib buf limit");
                    // TODO: close socket correctly
                    let _ = self
                        .tx
                        .send(Message::Close(Some(CloseFrame {
                            code: CloseCode::Abnormal,
                            reason: "".to_string().into(),
                        })))
                        .await;
                }
            }
        }
    }

    pub async fn send_close_error(&mut self, reason: &str) {
        // We doesnt care about result -- socket will be closed soon anyway
        let _ = self
            .tx
            .send(Message::Close(Some(CloseFrame {
                code: CloseCode::Bad(4004),
                reason: reason.to_string().into(),
            })))
            .await;
    }
}
