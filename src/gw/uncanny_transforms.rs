//! Websocket API Implementation

use std::vec;

use crate::b64_encode;
use crate::config::Config;
use crate::db::user_settings::READ_STATE_RESOURCE;
use crate::db::DB;
use crate::structures::gateway::ready::{
    ChannelReadState, Consents, NotificationSettings, Ready, WeirdVec,
};
use crate::structures::gateway::{BaseData, DispatchData, GatewayEvent};
use crate::structures::resource;
use crate::structures::resource::{
    channel::PrivateChannel, guild::GatewayGuild, member::MergedGuildMember, user::SelfUser,
};
use crate::structures::snowflake::{Snowflake, DC_BIRHDAY};

pub fn stub_ready(config: &Config, db: &DB, session_id: &str, uid: Snowflake) -> GatewayEvent {
    let me = db.user(uid).unwrap();

    let (settings_protobuf, _settings_protobuf_ver) = db
        .get_settings_proto(uid, 1)
        .map(|(k, v)| (b64_encode(k.as_slice()), v))
        .unwrap_or(("".to_string(), 0));

    let guilds = db.get_guilds_of_user(uid).unwrap();
    println!("READY guilds: {guilds:?}");

    let mut users_of_interest = vec![
        me.id,
        // TODO: here also goes friends
    ];

    guilds.iter().for_each(|g| {
        let mut l = db
            .get_users_of_guild(g.id)
            .unwrap()
            .iter()
            .map(|u| u.id)
            .collect();

        users_of_interest.append(&mut l);
    });

    GatewayEvent {
        s: None,
        base_data: BaseData::Dispatch,
        dispatch: Some(DispatchData::READY(Ready {
            v: 9,
            users: users_of_interest
                .iter()
                .map(|u| resource::user::User::new(&db.user(*u).unwrap()))
                .collect(),
            user_settings_proto: settings_protobuf,
            user: SelfUser::new(&me),
            tutorial: None,
            static_client_session_id: "00000000-0000-0000-0000-000000000000".to_string(),
            sessions: vec![],
            session_type: "normal".to_string(),
            session_id: session_id.to_string(),
            resume_gateway_url: config.gateway.url(),
            relationships: vec![],
            user_guild_settings: WeirdVec {
                version: 0,
                partial: false,
                entries: vec![],
            },
            read_state: generate_read_state_list(db, uid),

            private_channels: db
                .dm_channels(me.id)
                .unwrap()
                .iter()
                .map(|channel| {
                    PrivateChannel::new(
                        channel,
                        db.users_of_dm(channel.id).unwrap(),
                        db.last_message_in(channel.id.into()).ok().map(|m| m.id),
                    )
                })
                .collect(),

            notification_settings: NotificationSettings { flags: 0 },

            guilds: guilds
                .iter()
                .map(|g| {
                    let channels = db.guild_channels(g.id).unwrap();
                    GatewayGuild::new(g, &channels)
                })
                .collect(),

            merged_members: guilds
                .iter()
                .map(|g| {
                    db.get_users_of_guild(g.id)
                        .unwrap()
                        .iter()
                        .map(|m| MergedGuildMember::new(m))
                        .collect()
                })
                .collect(),

            guild_join_requests: vec![],
            guild_experiments: vec![],
            geo_ordered_rtc_regions: vec!["warsaw".to_string()],
            friend_suggestion_count: 0,
            explicit_content_scan_version: 1,
            experiments: vec![],
            country_code: "EN".to_string(),
            consents: Consents {},
            connected_accounts: vec![],
            auth_session_id_hash: "4221".to_string(),
            api_code_version: 1,
            analytics_token: "nope".to_string(),
            _trace: vec!["localhost".to_string()],
        })),
    }
}

fn generate_read_state_list(db: &DB, uid: Snowflake) -> WeirdVec<ChannelReadState> {
    WeirdVec {
        version: db.get_resource_ver(uid, READ_STATE_RESOURCE).unwrap(),
        partial: false,
        entries: db
            .get_read_states(uid)
            .unwrap()
            .iter()
            .map(|e| ChannelReadState {
                id: e.channel,

                // TODO: '1' is a channel flag, should be set properly for DMs and threads
                flags: 1,

                last_message_id: e.message,
                mention_count: 0,
                last_viewed: if e.manual {
                    None
                } else {
                    Some(((e.message.millis() - DC_BIRHDAY) / (24 * 3600 * 1000) + 1) as u32)
                },
                last_pin_timestamp: "1970-01-01T00:00:00+00:00".to_string(),
            })
            .collect(),
    }
}
