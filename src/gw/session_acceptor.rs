//! Performs auth checks and all that stuff and then passes websocket to an actual [SessionCtx](super::session::SessionCtx).

use std::time::Duration;

use rand::random;
use tokio::{net::TcpStream, sync::mpsc};
use tokio_tungstenite::WebSocketStream;

use crate::{
    eventbus::{Event, UserEvent},
    structures::gateway::{resume::Resume, GatewayEvent},
    AppData,
};

use super::{
    session,
    socket::{CompressionMode, Socket},
    Session,
};

pub async fn accept(data: AppData, stream: WebSocketStream<TcpStream>, query: Option<String>) {
    // Quick and dirty solution
    // TODO: properl implementation:
    // - parse entire query
    // - check "compress" key for compression
    // - check "encoding" key for encoding (either json or etf)
    let cm = if query.is_some_and(|e| e.contains("compress=zlib-stream")) {
        CompressionMode::ZlibStream
    } else {
        CompressionMode::Uncompressed
    };

    let mut socket = Socket::new(stream, cm);

    // Wait some time for client to respond
    let resume = tokio::select! {
        _ = tokio::time::sleep(Duration::from_secs(60)) => None,
        v = do_preconnect_stuff(&mut socket) => v,
    };

    // If client hasnt responded properly -- kill socket
    if resume.is_none() {
        return;
    }

    let resume = resume.unwrap();
    let user_id = data.db.lock().await.resolve_token(&resume.token);
    if user_id.is_none() {
        socket.send_close_error("TOKEN_INVALID").await;
        return;
    }

    let user_id = user_id.unwrap();

    if resume.session_id.is_none() {
        // Generate new session id
        let session_id: u64 = random();
        let session_id = format!("{}.{session_id}", user_id.to_string());

        // Create session
        let (socket_sender, socket_receiver) = mpsc::channel::<(Socket, u64)>(1);

        let s = Session {
            socket_sender,
            user_id,
        };

        // Lock on gateway
        let has_prev_sessions;
        {
            let mut gw = data.sessions.lock().await;

            has_prev_sessions = gw.has_active_session(user_id);

            gw.add_session(session_id.clone(), s);
        }

        // Lock on event bus
        let broadcast_receiver;
        {
            let ebus = data.ebus.lock().await;

            if !has_prev_sessions {
                let user = data.db.lock().await.user(user_id).unwrap();

                ebus.publish(Event::UserEvent {
                    user,
                    event: UserEvent::StatusChange { status: "online" },
                });
            }

            broadcast_receiver = ebus.subscribe();
        }

        // Run session (it'll block this coroutine)
        session::SessionCtx::new(
            data,
            user_id,
            session_id,
            socket,
            socket_receiver,
            broadcast_receiver,
        )
        .await
        .run()
        .await
    } else {
        let session_id = resume.session_id.as_ref().unwrap();
        let sessions = data.sessions.lock().await;
        let session = sessions.get_session(session_id);

        if session.is_none() {
            // Session with given id not found -- notify client that it should open new session instead
            socket.send(&GatewayEvent::invalid_session(false)).await;
        } else {
            // Session found -- supply our websocket to it
            let last_known_seq = resume.seq;
            session
                .unwrap()
                .replace_socket(socket, last_known_seq)
                .await;
        }

        // (this coroutine dies at this point)
    }
}

/// Wait for client to send Identify/Resume event
async fn do_preconnect_stuff(socket: &mut Socket) -> Option<Resume> {
    socket.send(&GatewayEvent::hello()).await;

    loop {
        let l = socket.receive().await?;
        match l.base_data {
            crate::structures::gateway::BaseData::Heartbeat(_) => {
                socket.send(&GatewayEvent::ack()).await
            }
            crate::structures::gateway::BaseData::Identify(identify) => {
                return Some(Resume {
                    token: identify.token,
                    session_id: None,
                    seq: 0,
                });
            }
            crate::structures::gateway::BaseData::Resume(resume) => {
                return Some(resume);
            }
            _ => return None,
        }
    }
}
