//! Gateway (websocker) server

use std::collections::{HashMap, HashSet};

use socket::Socket;
use tokio::net::TcpListener;
use tokio::sync::mpsc;
use tokio_tungstenite::tungstenite::handshake::server::{ErrorResponse, Request, Response};

use crate::structures::snowflake::Snowflake;
use crate::AppData;

mod session;
mod session_acceptor;
mod socket;
mod uncanny_transforms;

pub struct GW {
    sessions: HashMap<String, Session>,
}

impl GW {
    pub fn new() -> Self {
        GW {
            sessions: HashMap::new(),
        }
    }

    /// Do not forget to fire related broadcast if neccessary!
    pub fn add_session(&mut self, id: String, session: Session) {
        self.sessions.insert(id, session);
    }

    pub fn get_session(&self, id: &String) -> Option<&Session> {
        self.sessions.get(id)
    }

    /// Do not forget to fire related broadcast if neccessary!
    pub fn drop_session(&mut self, id: &String) {
        self.sessions.remove(id);
    }

    pub fn has_active_session(&self, user_id: Snowflake) -> bool {
        self.sessions
            .values()
            .find(|e| e.user_id == user_id)
            .is_some()
    }

    pub fn list_active_users(&self) -> HashSet<Snowflake> {
        self.sessions.values().map(|e| e.user_id).collect()
    }
}

pub struct Session {
    socket_sender: mpsc::Sender<(Socket, u64)>,
    user_id: Snowflake,
}

impl Session {
    pub async fn replace_socket(&self, socket: Socket, last_seq: u64) {
        self.socket_sender.send((socket, last_seq)).await.unwrap()
    }
}

pub async fn server(data: AppData) -> std::io::Result<()> {
    let host = &data.config.gateway.bind;
    let port = data.config.gateway.port;
    let s = TcpListener::bind(format!("{host}:{port}",)).await.unwrap();

    loop {
        let l = s.accept().await.unwrap();
        let data = data.clone();

        let mut query = None;

        let extract_query =
            |request: &Request, response: Response| -> Result<Response, ErrorResponse> {
                query = request.uri().query().map(|f| f.to_string());
                Ok(response)
            };

        let stream = tokio_tungstenite::accept_hdr_async(l.0, extract_query).await;

        if stream.is_ok() {
            let app_data = data.clone();
            tokio::spawn(async move {
                session_acceptor::accept(app_data, stream.unwrap(), query).await;
            });
        } else {
            let e = stream.unwrap_err();
            println!("ERROR ACCEPTING SOCKET {e:#?}");
        }
    }
}
