use std::time::Duration;

use tokio::sync::{broadcast, mpsc};
use tokio::time::sleep;

use crate::db;
use crate::eventbus::{ChannelEvent, GuildEvent, SelfEvent, UserEvent};
use crate::structures::gateway::presence::Presence;
use crate::structures::gateway::ready::Resumed;
use crate::structures::gateway::{
    GuildMemberListUpdate, GuildMemberRemove, MessageAck, TypingStart,
};
use crate::structures::resource::channel::GuildChannel;
use crate::structures::resource::guild::{GatewayGuild, NormalGuild, UnavailableGuild};
use crate::structures::resource::member::GuildMember;
use crate::structures::resource::user::User;
use crate::structures::{
    gateway::{BaseData, DispatchData, GatewayEvent, MessageDelete},
    resource::{self, message::MessageWithExtras},
    snowflake::Snowflake,
};
use crate::{eventbus::Event, millis, AppData};

use super::socket::Socket;
use super::uncanny_transforms;

const ACK_MS: u64 = 60 * 1000;
const SESSION_MS: u64 = 2 * ACK_MS;
const MAX_REPLAY_QUEUE_SIZE: usize = 128;

enum SelectResult {
    AckTimeout,
    SessionTimeout,
    SocketReplace(Socket, u64),
    SocketError,
    SocketMessage(GatewayEvent),
    Broadcast(Event),
}

pub struct SessionCtx {
    // From this receivers rest of the server is able to communicate w/ us
    socket_receiver: mpsc::Receiver<(Socket, u64)>,
    broadcast_receiver: broadcast::Receiver<Event>,

    // Our static data
    data: AppData,
    user_id: Snowflake,
    session_id: String,

    // Our dynamic data
    socket: Option<Socket>,
    last_ack: u64,
    next_seq_no: u64,

    replay_queue: Vec<DispatchData>,
    replay_queue_first_seq_no: u64,

    allowed_guilds: Vec<Snowflake>,
    allowed_channels: Vec<Snowflake>,
}

impl SessionCtx {
    pub async fn new(
        data: AppData,
        user_id: Snowflake,
        session_id: String,
        current_socket: Socket,
        socket_receiver: mpsc::Receiver<(Socket, u64)>,
        broadcast_receiver: broadcast::Receiver<Event>,
    ) -> Self {
        let mut rz = Self {
            data,
            user_id,
            session_id,
            socket: Some(current_socket),
            socket_receiver,
            broadcast_receiver,
            last_ack: millis(),
            next_seq_no: 0,
            replay_queue: vec![],
            replay_queue_first_seq_no: 0,
            allowed_guilds: vec![],
            allowed_channels: vec![],
        };

        rz.rebuild_allowed_ids().await;

        rz
    }

    async fn rebuild_allowed_ids(&mut self) {
        let db = self.data.db.lock().await;

        self.allowed_guilds.clear();
        self.allowed_channels.clear();

        for guild in db.get_guilds_of_user(self.user_id).unwrap() {
            self.allowed_guilds.push(guild.id);

            for channel in db.guild_channels(guild.id).unwrap() {
                self.allowed_channels.push(channel.id);
            }
        }
    }

    pub async fn run(&mut self) {
        self.send_ready().await;

        loop {
            match self.wait_for_next_event().await {
                SelectResult::AckTimeout => {
                    // Client has failed to ack in time -- ask him to reconnect w/ same session_id
                    self.send(&GatewayEvent::invalid_session(true)).await;
                    self.socket = None;
                }
                SelectResult::SessionTimeout => {
                    // Client is gone for too long -- drop session completely

                    // Notify client
                    self.send(&GatewayEvent::invalid_session(false)).await;

                    // Drop session
                    self.data
                        .sessions
                        .lock()
                        .await
                        .drop_session(&self.session_id);

                    // Send broadcast
                    self.fire_bc_become_offline().await;

                    // Exit
                    break;
                }
                SelectResult::SocketReplace(new_socket, last_seq_no) => {
                    let take_count = self.next_seq_no as i64 - last_seq_no as i64 - 1;

                    println!("Client asked us to replay from {last_seq_no}");
                    println!("  we're now at {}", self.next_seq_no);
                    println!(
                        "  we have {} entries in replay queue, counting from {}",
                        self.replay_queue.len(),
                        self.replay_queue_first_seq_no
                    );
                    println!("  we want to replay {take_count} entries");

                    if take_count > self.replay_queue.len() as i64 {
                        println!("Decision: force client to reconnect");

                        // Our replay queue is not that old
                        self.send(&GatewayEvent::invalid_session(false)).await;
                        self.socket = None;
                    } else {
                        println!("Decision: replay the queue");

                        self.last_ack = millis();
                        self.socket = Some(new_socket);

                        // Replay the queue and empy it
                        let rep_q_len = self.replay_queue.len() as i64;
                        for idx in (rep_q_len - take_count)..(rep_q_len) {
                            let seq_idx = self.replay_queue_first_seq_no + idx as u64;

                            let mut evt =
                                GatewayEvent::dispatch(self.replay_queue[idx as usize].clone());
                            evt.s = Some(seq_idx);
                            self.send(&evt).await;
                        }

                        self.replay_queue.clear();

                        // Manually send RESUMED event
                        let mut evt = GatewayEvent::dispatch(DispatchData::RESUMED(Resumed::new()));
                        evt.s = Some(self.next_seq_no);
                        self.send(&evt).await;
                        self.next_seq_no += 1;

                        // Update sequence counters
                        self.replay_queue_first_seq_no = self.next_seq_no;
                    }
                }
                SelectResult::SocketError => {
                    // TODO: close connection properly
                    self.socket = None;
                }
                SelectResult::SocketMessage(message) => {
                    self.handle_socket_message(message).await;
                }
                SelectResult::Broadcast(message) => {
                    self.handle_ebus_message(message).await;
                }
            };
        }
    }

    async fn wait_for_next_event(&mut self) -> SelectResult {
        // Calculate wait times
        let now = millis();

        let na = self.last_ack + ACK_MS;
        let wait_ack = if na < now { 0 } else { na - now };

        let ns = self.last_ack + SESSION_MS;
        let wait_session = if ns < now { 0 } else { ns - now };

        // Explicitly borrow parts of self which'll be used in `select`
        let has_socket = self.socket.is_some();
        let socket_pair = &mut self.socket;
        let socket_receiver = &mut self.socket_receiver;
        let broadcast_receiver = &mut self.broadcast_receiver;

        tokio::select! {

            // Option 1: there's no ack from current socket for too long (= kill socket)
            _ = sleep(Duration::from_millis(wait_ack)), if has_socket => {
                SelectResult::AckTimeout
            },

            // Option 2: there's no ack for too long (= kill session)
            _ = sleep(Duration::from_millis(wait_session)) => {
                SelectResult::SessionTimeout
            },

            // Option 3: there's event from current socket
            r = receive_from_socket(socket_pair), if has_socket => {
                if r.is_none() {
                    SelectResult::SocketError
                } else {
                    SelectResult::SocketMessage(r.unwrap())
                }
            }

            // Option 4: someone wants us to switch socket
            r = socket_receiver.recv() => {
                let (socket, seq_no) = r.unwrap();
                SelectResult::SocketReplace(socket, seq_no)
            },

            // Option 5: there's broadcast event
            r = broadcast_receiver.recv() => {
                SelectResult::Broadcast(r.unwrap())
            },
        }
    }

    async fn handle_socket_message(&mut self, message: GatewayEvent) {
        match message.base_data {
            BaseData::Dispatch => {
                println!("Why do we receive DISPATCH from a client?");
                return;
            }
            BaseData::Heartbeat(_seq_no) => {
                self.last_ack = millis();

                // TODO: rm events up to seq_no

                self.send(&GatewayEvent::ack()).await;
            }
            BaseData::UpdateVoiceState(_state) => {
                /* if state.guild_id.is_none() {
                    // TODO: Client requested disconnect
                    continue;
                }

                let guild = state.guild_id.unwrap();
                let user = data.db.lock().await.user(uid.unwrap()).unwrap();

                // Warning: server MUST send events in the following order:
                // 1. Voice State Update
                // 2. Voice Server Update
                //
                // Client will break if this order is broken

                data.gw
                    .lock()
                    .await
                    .send_bc(DispatchData::VOICE_STATE_UPDATE(VoiceState {
                        guild_id: state.guild_id,
                        channel_id: state.channel_id,
                        user_id: uid.unwrap(),
                        member: Some(guild_mem_fallback(&user)),
                        session_id: session_id.clone(),
                        mute: false,
                        deaf: false,
                        suppress: false,
                        self_mute: false,
                        self_deaf: false,
                        self_stream: Some(false),
                        self_video: Some(false),
                        request_to_speak_timestamp: None,
                    }));

                let tx0 = tx.clone();
                let sn0 = sequence_num.clone();
                let ep0 = format!(
                    "{}:{}",
                    data.config.voice.ws_host, data.config.voice.ws_port
                );

                tokio::spawn(async move {
                    tokio::time::sleep(Duration::from_millis(200)).await;

                    let mut r = GatewayEvent::dispatch(DispatchData::VOICE_SERVER_UPDATE(
                        VoiceServerUpdate {
                            token: "token".to_string(),
                            guild_id: Some(guild),
                            channel_id: None,
                            endpoint: Some(ep0),
                        },
                    ));

                    if r.dispatch.is_some() {
                        let mut sn = sn0.lock().await;
                        let sn = sn.deref_mut();
                        *sn += 1;
                        r.s = Some(*sn);
                    }

                    send(tx0.lock().await.deref_mut(), r).await;
                }); */
            }
            BaseData::GuildSubscriptionsBulk(e) => {
                let active_users = self.data.sessions.lock().await.list_active_users();

                let db = self.data.db.lock().await;

                let l: Vec<GatewayEvent> = e
                    .subscriptions
                    .iter()
                    .map(|(guild_id, _req)| {
                        let members = db.get_users_of_guild(*guild_id).unwrap();
                        GatewayEvent::dispatch(DispatchData::GUILD_MEMBER_LIST_UPDATE(
                            GuildMemberListUpdate::new(*guild_id, &members, &active_users),
                        ))
                    })
                    .collect();

                drop(db);

                for e in l {
                    self.send(&e).await;
                }
            }
            _ => {
                println!("Unhandled! {message:?}");
                return;
            }
        }
    }

    async fn handle_ebus_message(&mut self, message: Event) {
        match message {
            Event::GuildEvent { guild, event } => {
                let rebuild_pre = match &event {
                    GuildEvent::GuildCreate { owner, .. } => owner.id == self.user_id,
                    GuildEvent::UserJoin { user, .. } => user.id == self.user_id,
                    _ => false,
                };

                let rebuild_post = match &event {
                    GuildEvent::UserLeave { user, .. } => user.id == self.user_id,
                    _ => false,
                };

                if rebuild_pre {
                    self.rebuild_allowed_ids().await;
                }

                if self.allowed_guilds.contains(&guild) {
                    self.handle_bc_guild_event(event).await;
                }

                if rebuild_post {
                    self.rebuild_allowed_ids().await;
                }
            }
            Event::ChannelEvent {
                source: _,
                channel,
                event,
            } => {
                let rebuild_pre = match &event {
                    ChannelEvent::ChannelCreate(channel) => channel
                        .guild_id
                        .is_some_and(|v| self.allowed_guilds.contains(&v)),
                    _ => false,
                };

                let rebuild_post = match &event {
                    ChannelEvent::ChannelDelete(channel) => channel
                        .guild_id
                        .is_some_and(|v| self.allowed_guilds.contains(&v)),
                    _ => false,
                };

                if rebuild_pre {
                    self.rebuild_allowed_ids().await;
                }

                if self.allowed_channels.contains(&channel) {
                    self.handle_bc_channel_event(channel, event).await;
                }

                if rebuild_post {
                    self.rebuild_allowed_ids().await;
                }
            }
            Event::UserEvent { user, event } => {
                // This event group is sorted in handlers
                match event {
                    UserEvent::StatusChange { status } => {
                        self.handle_user_presence_event(user, status).await
                    }
                    UserEvent::Updated => self.handle_user_update(user).await,
                }
            }
            Event::SelfEvent { user_id, event } => {
                if user_id != self.user_id {
                    return;
                }

                match event {
                    SelfEvent::MessageAck {
                        version,
                        channel_id,
                        message_id,
                        last_viewed,
                        manual,
                        mention_count,
                    } => {
                        self.send_dispatch(DispatchData::MESSAGE_ACK(MessageAck {
                            version,
                            channel_id,
                            message_id,
                            last_viewed,
                            flags: 1,
                            manual,
                            mention_count,
                            ack_type: if manual { Some(0) } else { None },
                        }))
                        .await;
                    }
                }
            }
        }
    }

    async fn handle_user_presence_event(&mut self, user: db::user::User, status: &str) {
        // Step 1: send presence update

        self.send_dispatch(DispatchData::PRESENCE_UPDATE(Presence::new(
            user.id, status,
        )))
        .await;

        // Step 2: update member lists on all common servers

        let active_users = self.data.sessions.lock().await.list_active_users();

        let db = self.data.db.lock().await;
        let my_guilds = db.get_guilds_of_user(self.user_id).unwrap();
        let target_guilds = db.get_guilds_of_user(user.id).unwrap();
        drop(db);

        for guild in target_guilds {
            if my_guilds.iter().find(|g| g.id == guild.id).is_some() {
                // TODO: here we send complete member list update, which is big and costly

                // Note: #non-optimal-member-list
                // There's more efficient ways to update member list
                // It doesnt matter on a scale of <100 users per guild
                // But this code have to be optimized in the future
                let members = self
                    .data
                    .db
                    .lock()
                    .await
                    .get_users_of_guild(guild.id)
                    .unwrap();
                self.send_dispatch(DispatchData::GUILD_MEMBER_LIST_UPDATE(
                    GuildMemberListUpdate::new(guild.id, &members, &active_users),
                ))
                .await;
            }
        }
    }

    async fn handle_user_update(&mut self, user: db::user::User) {
        let db = self.data.db.lock().await;
        let my_guilds = db.get_guilds_of_user(self.user_id).unwrap();
        let target_guilds = db.get_guilds_of_user(user.id).unwrap();
        drop(db);

        let has_common_guilds = my_guilds
            .iter()
            .find(|e| target_guilds.iter().find(|v| e.id == v.id).is_some())
            .is_some();

        if has_common_guilds {
            self.send_dispatch(DispatchData::USER_UPDATE(User::new(&user)))
                .await;
        }
    }

    async fn handle_bc_guild_event(&mut self, event: GuildEvent) {
        match event {
            GuildEvent::GuildCreate {
                owner: _,
                guild,
                channels,
            } => {
                // Note: we dont have to filter message here
                // as it was (or at least should be) checked higher on call chain
                self.send_dispatch(DispatchData::GUILD_CREATE(GatewayGuild::new(
                    &guild, &channels,
                )))
                .await;
            }

            GuildEvent::GuildUpdate { guild } => {
                self.send_dispatch(DispatchData::GUILD_UPDATE(NormalGuild::new(&guild)))
                    .await;
            }

            GuildEvent::GuildDelete { guild } => {
                let g = UnavailableGuild {
                    id: guild.id,
                    unavailable: None,
                    geo_restricted: None,
                };

                self.send_dispatch(DispatchData::GUILD_DELETE(g)).await;
            }

            GuildEvent::UserJoin {
                user,
                guild,
                channels,
            } => {
                if user.id == self.user_id {
                    self.send_dispatch(DispatchData::GUILD_CREATE(GatewayGuild::new(
                        &guild, &channels,
                    )))
                    .await;

                    self.send_dispatch(DispatchData::GUILD_MEMBER_ADD(GuildMember::new(
                        guild.id, &user,
                    )))
                    .await;
                } else {
                    // TODO: see #non-optimal-member-list note
                    let active_users = self.data.sessions.lock().await.list_active_users();
                    let members = self
                        .data
                        .db
                        .lock()
                        .await
                        .get_users_of_guild(guild.id)
                        .unwrap();
                    self.send_dispatch(DispatchData::GUILD_MEMBER_LIST_UPDATE(
                        GuildMemberListUpdate::new(guild.id, &members, &active_users),
                    ))
                    .await;
                }
            }
            GuildEvent::UserLeave { user, guild } => {
                if user.id == self.user_id {
                    self.send_dispatch(DispatchData::GUILD_DELETE(UnavailableGuild::new(guild.id)))
                        .await;

                    self.send_dispatch(DispatchData::GUILD_MEMBER_REMOVE(GuildMemberRemove {
                        guild_id: guild.id,
                        user: resource::user::User::new(&user),
                    }))
                    .await;
                } else {
                    // TODO: see #non-optimal-member-list note
                    let active_users = self.data.sessions.lock().await.list_active_users();
                    let members = self
                        .data
                        .db
                        .lock()
                        .await
                        .get_users_of_guild(guild.id)
                        .unwrap();
                    self.send_dispatch(DispatchData::GUILD_MEMBER_LIST_UPDATE(
                        GuildMemberListUpdate::new(guild.id, &members, &active_users),
                    ))
                    .await;
                }
            }
        }
    }

    async fn handle_bc_channel_event(&mut self, channel_id: Snowflake, event: ChannelEvent) {
        match event {
            ChannelEvent::ChannelCreate(channel) => {
                let d = DispatchData::CHANNEL_CREATE(GuildChannel::new(&channel));
                self.send_dispatch(d).await;
            }
            ChannelEvent::ChannelUpdate(channel) => {
                let d = DispatchData::CHANNEL_UPDATE(GuildChannel::new(&channel));
                self.send_dispatch(d).await;
            }
            ChannelEvent::ChannelDelete(channel) => {
                let d = DispatchData::CHANNEL_DELETE(GuildChannel::new(&channel));
                self.send_dispatch(d).await;
            }
            ChannelEvent::MessageCreate(message, vec, hash_map) => {
                // TODO: I've seen "double sent message" bug in DC client.
                //
                // It was fixed by not sending "MESSAGE CREATE" dispatch event
                // when given message was created by this user.
                //
                // That fix caused client to not show message with picture
                // attachment. So that check if now removed.

                let d = DispatchData::MESSAGE_CREATE(MessageWithExtras {
                    msg: resource::message::Message::new(
                        &self.data.config,
                        &message,
                        &vec,
                        &hash_map,
                    ),
                    guild_id: None,
                    mentions: vec![],
                });

                self.send_dispatch(d).await;
            }
            ChannelEvent::MessageUpdate(message, vec, hash_map) => {
                let d = DispatchData::MESSAGE_UPDATE(MessageWithExtras {
                    msg: resource::message::Message::new(
                        &self.data.config,
                        &message,
                        &vec,
                        &hash_map,
                    ),
                    guild_id: None,
                    mentions: vec![],
                });

                self.send_dispatch(d).await;
            }
            ChannelEvent::MessageDelete(message) => {
                let d = DispatchData::MESSAGE_DELETE(MessageDelete {
                    id: message.id,
                    channel_id: message.channel,
                    guild_id: None,
                });
                self.send_dispatch(d).await;
            }
            ChannelEvent::TypingStart {
                guild_id,
                user_id,
                member,
            } => {
                let d = DispatchData::TYPING_START(TypingStart {
                    channel_id,
                    guild_id,
                    user_id,
                    timestamp: millis() / 1000,
                    member,
                });
                self.send_dispatch(d).await;
            }
        }
    }

    /// Silently drops transmission if there are no active socket
    async fn send(&mut self, event: &GatewayEvent) {
        if self.socket.is_none() {
            println!("Failed to send: no socket available");
            return;
        }

        self.socket.as_mut().unwrap().send(event).await;
    }

    async fn send_ready(&mut self) {
        let db = self.data.db.lock().await;
        let ready =
            uncanny_transforms::stub_ready(&self.data.config, &db, &self.session_id, self.user_id);
        drop(db);

        self.send(&ready).await;
    }

    async fn send_dispatch(&mut self, d: DispatchData) {
        let is_dispatch = matches!(d, DispatchData::RESUMED(_));
        assert_ne!(is_dispatch, true, "Dude, send RESUMED manually");

        self.replay_queue.push(d.clone());
        if self.replay_queue.len() > MAX_REPLAY_QUEUE_SIZE {
            self.replay_queue.remove(0);
            self.replay_queue_first_seq_no += 1;
        }

        // Gateway events increase sequence number
        let mut event = GatewayEvent::dispatch(d);
        event.s = Some(self.next_seq_no);
        self.next_seq_no += 1;

        self.send(&event).await;
    }

    async fn fire_bc_become_offline(&self) {
        let has_other_sessions = self
            .data
            .sessions
            .lock()
            .await
            .has_active_session(self.user_id);

        let user = self.data.db.lock().await.user(self.user_id).unwrap();

        if !has_other_sessions {
            self.data.ebus.lock().await.publish(Event::UserEvent {
                user,
                event: UserEvent::StatusChange { status: "offline" },
            });
        }
    }
}

async fn receive_from_socket(socket: &mut Option<Socket>) -> Option<GatewayEvent> {
    socket.as_mut()?.receive().await
}
