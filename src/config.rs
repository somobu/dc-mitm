use serde::{Deserialize, Serialize};

#[derive(Debug, Default, Serialize, Deserialize)]
pub struct Config {
    /// Main http server (static + api)
    pub main: MainConfig,

    /// Main websocket server (gateway)
    pub gateway: GatewayConfig,

    /// Voice server (voice websocket)
    pub voice: VoiceConfig,

    /// Image/file upload and distribution server
    pub cdn: CdnConfig,

    pub bridge_tg: Option<TgBotConfig>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct MainConfig {
    /// Scheme to report to clients
    ///
    /// Possible values: "http", "https"
    pub public_scheme: String,

    /// IP/Domain to report to clients
    ///
    /// Examples: `localhost`, `192.168.43.1`, `my.awesome.domain.com`
    pub public_host: String,

    /// Port to report to clients
    pub public_port: Option<u16>,

    /// IP to bind to
    ///
    /// Examples:
    /// - `0.0.0.0` binds to all interfaces (default value)
    pub bind: String,

    /// Port to bind to
    pub port: u16,

    /// Assets will be served from this directory
    ///
    /// `GET http://server/assets/{asset}` will read from `{assets_directory}/{asset}`
    pub assets_directory: String,

    /// Optional address of assets server to fetch assets from
    ///
    /// Assets fetching done with `cURL` command line tool.
    ///
    /// Example: `"https://my.server.com/"`
    #[serde(default)]
    pub assets_fetch_src: Option<String>,

    /// Optional `cURL` proxy argument
    ///
    /// Example: `-x socks5://localhost:9150` (default tor port)
    #[serde(default)]
    pub assets_fetch_proxy: Option<String>,

    // False if omitted
    #[serde(default)]
    pub registration_enabled: bool,

    /// List of regexes to test registration email against of.
    ///
    /// Would only be used if non-empty
    #[serde(default)]
    pub registration_email_whitelist: Vec<String>,
}

impl MainConfig {
    pub fn public_port(&self) -> u16 {
        self.public_port.unwrap_or_else(|| {
            if self.public_scheme == "http" {
                80
            } else {
                443
            }
        })
    }

    /// Scheme://host:port (no trailing slash)
    pub fn url(&self) -> String {
        let s = &self.public_scheme;
        let h = &self.public_host;
        let p = &self.public_port();
        format!("{s}://{h}:{p}")
    }
}

impl Default for MainConfig {
    fn default() -> Self {
        Self {
            public_scheme: "http".to_string(),
            public_host: "localhost".to_string(),
            public_port: Some(9000),
            bind: "0.0.0.0".to_string(),
            port: 9000,
            assets_directory: "client/assets".to_string(),
            assets_fetch_src: None,
            assets_fetch_proxy: None,
            registration_enabled: true,
            registration_email_whitelist: vec![],
        }
    }
}

#[derive(Debug, Serialize, Deserialize)]
pub struct GatewayConfig {
    /// Scheme to report to clients
    ///
    /// Possible values: "ws", "wss"
    pub public_scheme: String,

    /// IP/Domain to report to clients
    pub public_host: String,

    /// Port to report to clients
    pub public_port: Option<u16>,

    /// IP to bind to
    pub bind: String,

    /// Port to bind to
    pub port: u16,
}

impl GatewayConfig {
    pub fn public_port(&self) -> u16 {
        self.public_port
            .unwrap_or_else(|| if self.public_scheme == "ws" { 80 } else { 443 })
    }

    /// Scheme://host:port (no trailing slash)
    pub fn url(&self) -> String {
        let s = &self.public_scheme;
        let h = &self.public_host;
        let p = &self.public_port();
        format!("{s}://{h}:{p}")
    }
}

impl Default for GatewayConfig {
    fn default() -> Self {
        Self {
            public_scheme: "ws".to_string(),
            public_host: "localhost".to_string(),
            public_port: Some(9001),
            bind: "0.0.0.0".to_string(),
            port: 9001,
        }
    }
}

#[derive(Debug, Serialize, Deserialize)]
pub struct VoiceConfig {
    /// IP/Domain to report to clients
    pub ws_host: String,

    /// IP to bind to
    pub ws_bind: String,

    /// Port to bind to
    pub ws_port: u16,
}

impl Default for VoiceConfig {
    fn default() -> Self {
        Self {
            ws_host: "localhost".to_string(),
            ws_bind: "0.0.0.0".to_string(),
            ws_port: 9002,
        }
    }
}

#[derive(Debug, Serialize, Deserialize)]
pub struct CdnConfig {
    /// Scheme to report to clients
    ///
    /// Possible values: "http", "https"
    pub public_scheme: String,

    /// IP/Domain to report to clients
    ///
    /// Examples: `localhost`, `192.168.43.1`, `my.awesome.domain.com`
    pub public_host: String,

    /// Port to report to clients
    pub public_port: Option<u16>,

    /// IP to bind to
    ///
    /// Examples:
    /// - `0.0.0.0` binds to all interfaces (default value)
    pub bind: String,

    /// Port to bind to
    pub port: u16,
}

impl CdnConfig {
    pub fn public_port(&self) -> u16 {
        self.public_port.unwrap_or_else(|| {
            if self.public_scheme == "http" {
                80
            } else {
                443
            }
        })
    }

    /// Scheme://host:port (no trailing slash)
    pub fn url(&self) -> String {
        let s = &self.public_scheme;
        let h = &self.public_host;
        let p = &self.public_port();
        format!("{s}://{h}:{p}")
    }
}

impl Default for CdnConfig {
    fn default() -> Self {
        Self {
            public_scheme: "http".to_string(),
            public_host: "localhost".to_string(),
            public_port: Some(9005),
            bind: "0.0.0.0".to_string(),
            port: 9005,
        }
    }
}

#[derive(Debug, Serialize, Deserialize)]
pub struct TgBotConfig {
    pub token: String,
}

impl Default for TgBotConfig {
    fn default() -> Self {
        Self {
            token: "token from telegram".to_string(),
        }
    }
}
