use actix_web::web::{self};
use actix_web::{get, patch, HttpResponse};
use paste::paste;
use prost::Message;
use serde::{Deserialize, Serialize};

use super::hr::{self, HR};
use crate::api::LoggedIn;
use crate::eventbus::Event;
use crate::structures::resource::user::{SelfUser, UserProfile};
use crate::structures::snowflake::Snowflake;
use crate::{b64_decode, b64_encode, AppData};

pub fn configure(cfg: &mut web::ServiceConfig) {
    cfg.service(get_me);
    cfg.service(get_payment_sources);
    cfg.service(get_country_code);
    cfg.service(patch_me);
    cfg.service(get_profile);
    cfg.service(get_settings_proto);
    cfg.service(patch_settings_proto);
}

#[get("/users/@me")]
async fn get_me(ctx: AppData, uid: LoggedIn) -> HR {
    let me = ctx.db.lock().await.user(*uid).unwrap();
    hr::ok_json(&SelfUser::new(&me))
}

#[patch("/users/@me")]
async fn patch_me(ctx: AppData, uid: LoggedIn, data: web::Json<UserPatchIn>) -> HR {
    let db = ctx.db.lock().await;
    let mut me = db.user(*uid).unwrap();

    // No CDN -- no avatar
    #[cfg(feature = "cdn")]
    {
        if data.0.avatar.is_some() {
            let icon_id = Snowflake::new();
            let icon = crate::cdn::b64_to_pic(&data.0.avatar.unwrap());

            crate::cdn::store_avatar(me.id, icon_id, &icon);

            me.avatar = Some(icon_id)
        }
    }

    data.0.global_name.inspect(|global_name| {
        let global_name = global_name.trim();

        if global_name.is_empty() {
            me.display_name = None;
        } else {
            me.display_name = Some(global_name.to_string());
        }
    });

    db.update_user(&me).unwrap();

    let u = SelfUser::new(&me);

    ctx.ebus.lock().await.publish(Event::UserEvent {
        user: me.clone(),
        event: crate::eventbus::UserEvent::Updated,
    });

    ctx.ebus.lock().await.publish(Event::UserEvent {
        user: me,
        event: crate::eventbus::UserEvent::StatusChange { status: "online" },
    });

    hr::ok_json(&u)
}

#[derive(Debug, Deserialize)]
struct UserPatchIn {
    /// base64-encoded image
    #[allow(dead_code)] // It is fine cuz we dont handle avatars in no-CDN mode
    avatar: Option<String>,

    global_name: Option<String>,
}

#[get("/users/@me/billing/payment-sources")]
async fn get_payment_sources(_uid: LoggedIn) -> HR {
    let v: Vec<String> = vec![];
    hr::ok_json(&v)
}

#[get("/users/@me/billing/country-code")]
async fn get_country_code(_uid: LoggedIn) -> HR {
    #[derive(Serialize)]
    struct CC {
        country_code: String,
    }

    let v = CC {
        country_code: "US".to_string(),
    };

    hr::ok_json(&v)
}

#[get("/users/@me/settings-proto/{code}")]
async fn get_settings_proto(ctx: AppData, uid: LoggedIn, code: web::Path<u8>) -> HR {
    assert!(
        *code < 3,
        "This server supports only #1 and #2 settings-proto"
    );

    let (encoded, _prev) = ctx
        .db
        .lock()
        .await
        .get_settings_proto(*uid, *code)
        .unwrap_or_default();

    let v = UserProtoOut {
        settings: b64_encode(&encoded),
        out_of_date: None,
    };

    hr::ok_json(&v)
}

macro_rules! assign_if_some {
    ($prev:ident, $patch:ident, $($field:literal),*) => {
        paste! {
            $(
                if $patch.[<$field>].is_some() {
                    $prev.[<$field>] = $patch.[<$field>];
                }
            )*
        }
    };
}

#[patch("/users/@me/settings-proto/{code}")]
async fn patch_settings_proto(
    ctx: AppData,
    uid: LoggedIn,
    code: web::Path<u8>,
    data: web::Json<UserProtoIn>,
) -> HR {
    let proto_bytes = b64_decode(&data.0.settings).unwrap();

    let prev_data = ctx.db.lock().await.get_settings_proto(*uid, *code);

    let encoded;

    if *code == 1 {
        let (mut prev_proto, _prev_proto_ver) = prev_data
            .map(|(k, v)| {
                let k = k.as_slice();
                (protos::PreloadedUserSettings::decode(k).unwrap(), v)
            })
            .unwrap_or_default();

        let proto_patch = protos::PreloadedUserSettings::decode(proto_bytes.as_slice()).unwrap();

        #[rustfmt::skip]
        assign_if_some!(
            prev_proto, proto_patch,
            "versions", "inbox", "guilds", "user_content", "voice_and_video", 
            "text_and_images", "notifications", "privacy", "debug", "game_library",
            "status", "localization", "appearance", "guild_folders", "favorites",
            "audio_context_settings", "communities", "broadcast", "clips", "for_later",
            "safety_settings", "icymi_settings", "applications"
        );

        encoded = prev_proto.encode_to_vec();
    } else if *code == 2 {
        let (mut prev_proto, _prev_proto_ver) = prev_data
            .map(|(k, v)| {
                let k = k.as_slice();
                (protos::FrecencyUserSettings::decode(k).unwrap(), v)
            })
            .unwrap_or_default();

        let proto_patch = protos::FrecencyUserSettings::decode(proto_bytes.as_slice()).unwrap();

        #[rustfmt::skip]
        assign_if_some!(
            prev_proto, proto_patch,
            "versions", "favorite_gifs", "favorite_stickers", "sticker_frecency", "favorite_emojis", 
            "emoji_frecency", "application_command_frecency", "favorite_soundboard_sounds", "application_frecency", 
            "heard_sound_frecency", "played_sound_frecency", "guild_and_channel_frecency", "emoji_reaction_frecency"
        );

        encoded = prev_proto.encode_to_vec();
    } else {
        return HR::Ok(HttpResponse::BadRequest().finish());
    }

    ctx.db
        .lock()
        .await
        .insert_settings_proto(*uid, *code, &encoded, data.0.required_data_version)
        .unwrap();

    let v = UserProtoOut {
        settings: b64_encode(&encoded),
        out_of_date: Some(false),
    };

    hr::ok_json(&v)
}

/// Src: [discord.sex](https://docs.discord.sex/resources/user-settings#modify-user-settings-proto)
#[derive(Debug, Deserialize)]
struct UserProtoIn {
    /// The base-64 encoded serialized user settings protobuf modifications
    settings: String,

    /// The required data version of the proto
    ///
    /// When making offline edits, the required data version of the proto
    /// should be set to the last known version. This ensures that the client
    /// doesn't overwrite newer edits made on a different client on edit.
    required_data_version: Option<u32>,
}

/// Src: [discord.sex](https://docs.discord.sex/resources/user-settings#modify-user-settings-proto)
#[derive(Debug, Serialize)]
struct UserProtoOut {
    /// The base-64 encoded serialized user settings protobuf
    settings: String,

    /// Whether the user settings update was discarded due to an outdated
    /// `required_data_version`
    #[serde(skip_serializing_if = "Option::is_none")]
    out_of_date: Option<bool>,
}

#[get("/users/{user}/profile")]
async fn get_profile(ctx: AppData, uid: LoggedIn, user: web::Path<Snowflake>) -> HR {
    // This check is tricky: we should check if two users are actually related
    // Dunno how to do it tho. Just leave it be for now.
    hr::check_access(*uid)?;

    let me = ctx.db.lock().await.user(*user).unwrap();
    hr::ok_json(&UserProfile::new(&me))
}
