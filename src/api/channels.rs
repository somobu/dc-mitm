use actix_web::{delete, get, patch, post, web};

use serde::Deserialize;

use super::hr::{self, HR};
use super::LoggedIn;
use crate::eventbus::{ChannelEvent, Event};
use crate::structures::gateway::Stub;
use crate::structures::resource::channel::{ChannelType, GuildChannel};
use crate::structures::resource::member::GuildMember;
use crate::structures::snowflake::Snowflake;

use crate::structures::optional_option::OptionalOption;
use crate::{db, AppData};

pub fn configure(cfg: &mut web::ServiceConfig) {
    cfg.service(patch_channel);
    cfg.service(delete_channel);

    cfg.service(get_pinned);
    cfg.service(post_typing);

    cfg.service(create_guild_channel);
    cfg.service(patch_guild_channels);
}

#[patch("/channels/{channel_id}")]
async fn patch_channel(
    ctx: AppData,
    uid: LoggedIn,
    channel_id: web::Path<Snowflake>,
    data: web::Json<ChannelEdit>,
) -> HR {
    hr::check_channel_access(&ctx, *uid, *channel_id).await?;

    let db = ctx.db.lock().await;

    let old_ch = db.get_channel(*channel_id).unwrap();

    let ch = db::channel::Channel {
        id: *channel_id,
        r#type: old_ch.r#type,
        guild_id: old_ch.guild_id,
        parent_id: old_ch.parent_id,
        position: old_ch.position,

        name: data.0.name,
        topic: data.0.topic,
        nsfw: data.0.nsfw,

        last_message_id: None,
    };

    db.update_channel(&ch).unwrap();

    let ch_out = GuildChannel::new(&ch);

    ctx.ebus.lock().await.publish(Event::ChannelEvent {
        source: None,
        channel: *channel_id,
        event: ChannelEvent::ChannelUpdate(ch),
    });

    hr::ok_json(&ch_out)
}

/// Sent by client w/ `channel edit` request
#[allow(dead_code)]
#[derive(Debug, Deserialize)]
pub struct ChannelEdit {
    pub name: String,
    pub r#type: ChannelType,
    pub topic: Option<String>,
    pub bitrate: u32,
    pub user_limit: u32,
    pub nsfw: bool,
    pub flags: u32,
    pub rate_limit_per_user: u32,
}

#[delete("/channels/{channel_id}")]
async fn delete_channel(ctx: AppData, uid: LoggedIn, channel_id: web::Path<Snowflake>) -> HR {
    hr::check_channel_access(&ctx, *uid, *channel_id).await?;

    let channel = ctx.db.lock().await.get_channel(*channel_id).unwrap();
    ctx.db.lock().await.delete_channel(*channel_id).unwrap();

    let ch_out = GuildChannel::new(&channel);

    ctx.ebus.lock().await.publish(Event::ChannelEvent {
        source: None,
        channel: channel.id,
        event: ChannelEvent::ChannelDelete(channel),
    });

    hr::ok_json(&ch_out)
}

#[get("/channels/{channel_id}/pins")]
async fn get_pinned(ctx: AppData, uid: LoggedIn, channel_id: web::Path<Snowflake>) -> HR {
    hr::check_channel_access(&ctx, *uid, *channel_id).await?;

    let rz: Vec<Stub> = vec![];
    hr::ok_json(&rz)
}

#[post("/channels/{channel_id}/typing")]
async fn post_typing(ctx: AppData, uid: LoggedIn, channel_id: web::Path<Snowflake>) -> HR {
    hr::check_channel_access(&ctx, *uid, *channel_id).await?;

    let db = ctx.db.lock().await;

    let channel = db.get_channel(*channel_id).unwrap();
    let guild_id = channel.guild_id;

    ctx.ebus.lock().await.publish(Event::ChannelEvent {
        source: None,
        channel: *channel_id,
        event: ChannelEvent::TypingStart {
            guild_id,
            user_id: *uid,
            member: guild_id.map(|guild_id| {
                let user = db.user(*uid).unwrap();
                GuildMember::new(guild_id, &user)
            }),
        },
    });

    hr::ok_empty()
}

#[post("/guilds/{guild_id}/channels")]
async fn create_guild_channel(
    ctx: AppData,
    uid: LoggedIn,
    guild_id: web::Path<Snowflake>,
    data: web::Json<ChannelIn>,
) -> HR {
    hr::check_guild_access(&ctx, *uid, *guild_id).await?;

    let channel = db::channel::Channel {
        id: Snowflake::new(),
        r#type: data.0.r#type,
        name: data.0.name,
        guild_id: Some(*guild_id),
        parent_id: data.0.parent_id,
        position: 0,
        topic: None,
        nsfw: false,
        last_message_id: None,
    };

    ctx.db.lock().await.guild_channel_create(&channel).unwrap();

    let ch_out = GuildChannel::new(&channel);

    ctx.ebus.lock().await.publish(Event::ChannelEvent {
        source: None,
        channel: channel.id,
        event: ChannelEvent::ChannelCreate(channel),
    });

    hr::created_json(&ch_out)
}

/// Sent by client w/ `channel create` request
#[allow(dead_code)]
#[derive(Debug, Deserialize)]
pub struct ChannelIn {
    pub name: String,
    pub r#type: ChannelType,
    pub parent_id: Option<Snowflake>,
    pub permission_overwrites: Vec<Stub>,
}

#[patch("/guilds/{guild_id}/channels")]
async fn patch_guild_channels(
    ctx: AppData,
    uid: LoggedIn,
    guild_id: web::Path<Snowflake>,
    data: web::Json<Vec<ChannelsPatchIn>>,
) -> HR {
    hr::check_guild_access(&ctx, *uid, *guild_id).await?;

    let db = ctx.db.lock().await;
    for ch in data.0 {
        let mut channel = db.get_channel(ch.id).unwrap();

        channel.parent_id = match ch.parent_id {
            OptionalOption::Absent => channel.parent_id,
            OptionalOption::Null => None,
            OptionalOption::Some(v) => Some(v),
        };

        channel.position = match ch.position {
            OptionalOption::Absent => channel.position,
            OptionalOption::Null => 0,
            OptionalOption::Some(v) => v,
        };

        db.reorder_guild_channel(ch.id, channel.parent_id, channel.position)
            .unwrap();

        ctx.ebus.lock().await.publish(Event::ChannelEvent {
            source: None,
            channel: ch.id,
            event: ChannelEvent::ChannelUpdate(channel),
        });
    }

    hr::ok_empty()
}

#[derive(Debug, Deserialize)]
pub struct ChannelsPatchIn {
    pub id: Snowflake,

    #[serde(default)]
    #[serde(skip_serializing_if = "OptionalOption::is_absent")]
    pub position: OptionalOption<u32>,

    #[serde(default)]
    pub parent_id: OptionalOption<Snowflake>,
    // TODO: lock_permissions: Option<bool>,
}
