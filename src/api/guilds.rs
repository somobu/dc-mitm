use actix_web::{delete, get, patch, post, web, HttpResponse};
use serde::{Deserialize, Serialize};

use super::hr::{self, HR};
use crate::api::LoggedIn;
use crate::eventbus::{Event, GuildEvent};
use crate::structures::gateway::Stub;
use crate::structures::resource::channel::ChannelType;
use crate::structures::resource::guild::{GatewayGuild, NormalGuild, UnavailableGuild};
use crate::structures::snowflake::Snowflake;
use crate::{db, AppData};

pub fn configure(cfg: &mut web::ServiceConfig) {
    cfg.service(create_guild);
    cfg.service(edit_guild);
    cfg.service(delete_guild);
    cfg.service(migrate_command_scope);
    cfg.service(get_guild_templates);
    cfg.service(leave_guild);
}

#[post("/guilds")]
async fn create_guild(ctx: AppData, uid: LoggedIn, data: web::Json<GuildIn>) -> HR {
    #[allow(unused_mut)]
    let mut guild = db::guild::Guild {
        id: Snowflake::new(),
        owner_id: *uid,
        name: data.0.name,
        icon: None,
    };

    #[cfg(feature = "cdn")]
    {
        if data.0.icon.is_some() {
            let icon = crate::cdn::b64_to_pic(&data.0.icon.unwrap());
            let icon_id = Snowflake::new();

            crate::cdn::store_icon(guild.id, icon_id, &icon);

            guild.icon = Some(icon_id);
        }
    }

    let db = ctx.db.lock().await;

    db.guild_create(&guild).unwrap();

    db.guild_channel_create(&db::channel::Channel {
        id: Snowflake::new_with_id(1),
        r#type: ChannelType::GUILD_TEXT,
        name: "main".to_string(),
        guild_id: Some(guild.id),
        parent_id: None,
        position: 0,
        topic: None,
        nsfw: false,
        last_message_id: None,
    })
    .unwrap();

    db.guild_channel_create(&db::channel::Channel {
        id: Snowflake::new_with_id(2),
        r#type: ChannelType::GUILD_VOICE,
        name: "voice".to_string(),
        guild_id: Some(guild.id),
        parent_id: None,
        position: 0,
        topic: None,
        nsfw: false,
        last_message_id: None,
    })
    .unwrap();

    let channels = db.guild_channels(guild.id).unwrap();
    let owner = db.user(*uid).unwrap();

    let guild_out = GatewayGuild::new(&guild, &channels);

    ctx.ebus.lock().await.publish(Event::GuildEvent {
        guild: guild.id,
        event: GuildEvent::GuildCreate {
            owner,
            guild,
            channels,
        },
    });

    hr::created_json(&guild_out)
}

/// Client sends this during guild creation proccess
#[allow(dead_code)]
#[derive(Debug, Deserialize)]
struct GuildIn {
    name: String,

    /// Base64-encoded image
    icon: Option<String>,

    channels: Vec<Stub>,
    system_channel_id: Option<Snowflake>,
    guild_template_code: Option<String>,
}

#[patch("/guilds/{guild_id}")]
async fn edit_guild(
    ctx: AppData,
    uid: LoggedIn,
    guild_id: web::Path<Snowflake>,
    data: web::Json<GuildUpdateIn>,
) -> HR {
    hr::check_guild_access(&ctx, *uid, *guild_id).await?;

    #[allow(unused_mut)]
    let mut guild = ctx.db.lock().await.get_guild(*guild_id).unwrap();
    guild.name = data.0.name;

    #[cfg(feature = "cdn")]
    {
        if data.0.icon.is_some() {
            let icon_data = data.0.icon.unwrap();

            // There are two possible cases:
            //
            // 1. `icon_data` is something like "1339874292422148096" i.e. icon is
            //    a Snowflake serialized to string (sent when server icon is not
            //    changed);
            //
            // 2. `icon_data` is rather long base64-encoded binary;

            if icon_data.len() < 32 {
                guild.icon = Some(icon_data.into());
            } else {
                let icon = crate::cdn::b64_to_pic(&icon_data);
                let icon_id = Snowflake::new();

                crate::cdn::store_icon(*guild_id, icon_id, &icon);

                guild.icon = Some(icon_id);
            }
        } else {
            guild.icon = None;
        }
    }

    ctx.db.lock().await.guild_update(&guild).unwrap();

    let guild_out = NormalGuild::new(&guild);

    ctx.ebus.lock().await.publish(Event::GuildEvent {
        guild: guild.id,
        event: GuildEvent::GuildUpdate { guild },
    });

    hr::ok_json(&guild_out)
}

/// Client sends this during guild edition process
#[allow(dead_code)]
#[derive(Debug, Deserialize)]
pub struct GuildUpdateIn {
    pub name: String,
    pub description: Option<Stub>,

    /// Base64-encoded image
    pub icon: Option<String>,

    pub splash: Option<String>,
    pub banner: Option<String>,
    pub home_header: Option<String>,
    pub afk_channel_id: Option<Snowflake>,
    pub afk_timeout: u32,
    pub system_channel_id: Option<Snowflake>,
    pub verification_level: u32,
    pub default_message_notifications: u32,
    pub explicit_content_filter: u32,
    pub system_channel_flags: u32,
    pub public_updates_channel_id: Option<Snowflake>,
    pub safety_alerts_channel_id: Option<Snowflake>,
    pub premium_progress_bar_enabled: bool,
    pub clan: Option<Stub>,
}

#[post("/guilds/{guild_id}/delete")]
async fn delete_guild(ctx: AppData, uid: LoggedIn, guild_id: web::Path<Snowflake>) -> HR {
    hr::check_guild_access(&ctx, *uid, *guild_id).await?;

    let guild = ctx.db.lock().await.get_guild(*guild_id).unwrap();
    ctx.db.lock().await.guild_delete(*guild_id).unwrap();

    ctx.ebus.lock().await.publish(Event::GuildEvent {
        guild: guild.id,
        event: GuildEvent::GuildDelete { guild },
    });

    let guild_out = UnavailableGuild {
        id: *guild_id,
        unavailable: None,
        geo_restricted: None,
    };

    hr::ok_json(&guild_out)
}

#[post("/guilds/{guild_id}/migrate-command-scope")]
async fn migrate_command_scope(ctx: AppData, uid: LoggedIn, guild_id: web::Path<Snowflake>) -> HR {
    hr::check_guild_access(&ctx, *uid, *guild_id).await?;

    #[derive(Serialize)]
    struct MCS {
        integration_ids_with_app_commands: Vec<Stub>,
    }
    let mcs = MCS {
        integration_ids_with_app_commands: vec![],
    };

    hr::ok_json(&mcs)
}

#[get("/guilds/{guild_id}/templates")]
async fn get_guild_templates(ctx: AppData, uid: LoggedIn, guild_id: web::Path<Snowflake>) -> HR {
    hr::check_guild_access(&ctx, *uid, *guild_id).await?;

    let v: Vec<String> = vec![];
    hr::ok_json(&v)
}

/// Data can be:
/// - { "lurking": false }
#[delete("/users/@me/guilds/{guild_id}")]
async fn leave_guild(
    ctx: AppData,
    uid: LoggedIn,
    guild_id: web::Path<Snowflake>,
    _data: String,
) -> HR {
    hr::check_guild_access(&ctx, *uid, *guild_id).await?;

    let db = ctx.db.lock().await;
    db.remove_user_from_guild(*guild_id, *uid).unwrap();

    let user = db.user(*uid).unwrap();
    let guild = db.get_guild(*guild_id).unwrap();

    ctx.ebus.lock().await.publish(Event::GuildEvent {
        guild: guild.id,
        event: GuildEvent::UserLeave { user, guild },
    });

    Ok(HttpResponse::NoContent().finish())
}
