use actix_web::http::header::{HeaderName, HeaderValue};
use actix_web::{body::BoxBody, http::StatusCode, web::Bytes, HttpResponse};
use serde::Serialize;

use crate::structures::snowflake::Snowflake;
use crate::AppData;

pub type HR = actix_web::Result<HttpResponse>;

const MSG_NO_RIGHTS: &str = "you don't have the right\ntherefore you don't have the right";

/// Sub access check, use it when actual access check fcn is not implemented yet.
pub fn check_access(_uid: Snowflake) -> actix_web::Result<()> {
    // TODO: check access
    Ok(())
}

pub async fn check_guild_access(
    ctx: &AppData,
    uid: Snowflake,
    guild: Snowflake,
) -> actix_web::Result<()> {
    let l = ctx
        .db
        .lock()
        .await
        .is_user_in_guild_v2(uid, guild)
        .map_err(|e| actix_web::error::ErrorInternalServerError(format!("DB failure: {e:?}")))?;

    if l {
        Ok(())
    } else {
        Err(actix_web::error::ErrorForbidden(MSG_NO_RIGHTS))
    }
}

pub async fn check_channel_access(
    ctx: &AppData,
    uid: Snowflake,
    channel: Snowflake,
) -> actix_web::Result<()> {
    let l = ctx
        .db
        .lock()
        .await
        .is_user_in_channel(uid, channel)
        .map_err(|e| actix_web::error::ErrorInternalServerError(format!("DB failure: {e:?}")))?;

    if l {
        Ok(())
    } else {
        Err(actix_web::error::ErrorForbidden(MSG_NO_RIGHTS))
    }
}

pub fn ok_json<T: ?Sized + Serialize>(value: &T) -> HR {
    json(StatusCode::OK, value)
}

pub fn created_json<T: ?Sized + Serialize>(value: &T) -> HR {
    json(StatusCode::CREATED, value)
}

pub fn json<T>(code: actix_web::http::StatusCode, value: &T) -> HR
where
    T: ?Sized + Serialize,
{
    let data = serde_json::to_string(value).unwrap();

    let mut resp =
        HttpResponse::new(code).set_body(BoxBody::new(Bytes::copy_from_slice(data.as_bytes())));

    resp.headers_mut().insert(
        HeaderName::from_static("content-type"),
        HeaderValue::from_static("application/json"),
    );

    resp.headers_mut().insert(
        HeaderName::from_static("cache-control"),
        HeaderValue::from_static("no-cache"),
    );

    Ok(resp)
}

pub fn ok_empty() -> HR {
    Ok(HttpResponse::NoContent().finish())
}
