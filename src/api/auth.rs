use std::collections::HashMap;

use actix_web::get;
use actix_web::web::{self};
use actix_web::{http::StatusCode, post};
use base64::prelude::BASE64_STANDARD;
use base64::Engine;
use regex::Regex;
use serde::{Deserialize, Serialize};
use sha3_rust::sha3_512;

use super::hr::{self, HR};
use crate::db::user::User;
use crate::structures::gateway::Stub;
use crate::structures::snowflake::Snowflake;
use crate::AppData;

pub fn configure(cfg: &mut web::ServiceConfig) {
    cfg.service(post_login);
    cfg.service(post_register);
    cfg.service(location_metadata);
    cfg.service(username_suggestions);
}

#[post("/auth/login")]
async fn post_login(ctx: AppData, data: web::Json<LoginInputs>) -> HR {
    let db = ctx.db.lock().await;
    let auth_result = db.auth(data.0.login, &hash_of(&data.0.password));

    if auth_result.is_ok() {
        let (token, user_id) = auth_result.unwrap();

        hr::ok_json(&LoginSuccess {
            user_id,
            token: token.to_string(),
            user_settings: UserSettings {
                locale: "en-US".to_string(),
                theme: "dark".to_string(),
            },
        })
        // .add_header("set-cookie", "__Secure-recent_mfa=; Expires=Sat, 13-Jul-2024 18:09:09 GMT; Max-Age=0; Secure; HttpOnly; Path=/api; SameSite=Strict")
    } else {
        println!("Login error: {:?}", auth_result.unwrap_err());
        let mut error = FormError::default();
        error.errors.login = Some(FormErrorList::default_login_error());
        error.errors.password = Some(FormErrorList::default_login_error());
        hr::json(StatusCode::FORBIDDEN, &error)
    }
}

#[allow(dead_code)]
#[derive(Debug, Deserialize)]
struct LoginInputs {
    login: String,
    password: String,
    undelete: bool,
    login_source: Option<()>,
    gift_code_sku_id: Option<()>,
}

#[allow(dead_code)]
#[derive(Debug, Serialize)]
struct LoginCaptcha {
    // TODO: should be used when implementing login rate-limit
    /*
    {
        "captcha_key": [
            "captcha-required"
        ],
        "captcha_sitekey": "00000000-0000-0000-0000-000000000000",  // UUID?
        "captcha_service": "hcaptcha"
    }
    */
}

#[derive(Debug, Serialize)]
struct LoginSuccess {
    user_id: Snowflake,
    token: String,
    user_settings: UserSettings,
}

#[derive(Debug, Serialize)]
struct UserSettings {
    locale: String,
    theme: String,
}

#[post("/auth/register")]
async fn post_register(ctx: AppData, data: web::Json<RegistrationRequest>) -> HR {
    if !ctx.config.main.registration_enabled {
        let mut login_error = FormError::default();
        login_error.errors.email = Some(FormErrorList::error(
            "REGISTRATION_FORBIDDEN",
            &format!("Registration forbidden"),
        ));

        return hr::json(StatusCode::FORBIDDEN, &login_error);
    }

    if ctx.config.main.registration_email_whitelist.len() > 0 {
        let mut has_match = false;

        for regex in &ctx.config.main.registration_email_whitelist {
            let regex = Regex::new(&regex).unwrap();
            if regex.is_match(&data.0.email) {
                has_match = true;
                break;
            }
        }

        if !has_match {
            let mut login_error = FormError::default();
            login_error.errors.email = Some(FormErrorList::error(
                "EMAIL_PROHIBITED",
                &format!("This email address is prohibited"),
            ));

            return hr::json(StatusCode::FORBIDDEN, &login_error);
        }
    }

    let pwd_hash = hash_of(&data.0.password);

    let db = ctx.db.lock().await;
    let reg_result = db.register(
        User {
            id: Snowflake::new(),
            username: data.0.username.clone(),
            email: data.0.email,
            display_name: data.0.global_name,
            avatar: None,
            is_bot: false,
        },
        &pwd_hash,
    );

    if reg_result.is_err() {
        let mut login_error = FormError::default();
        login_error.errors.email = Some(FormErrorList::error(
            "EMAIL_ALREADY_REGISTERED",
            &format!("{reg_result:?}"),
        ));

        return hr::json(StatusCode::FORBIDDEN, &login_error);
    }

    let auth_result = db.auth(data.0.username, &pwd_hash);

    if auth_result.is_ok() {
        let (token, _) = auth_result.unwrap();

        hr::ok_json(&RegistrationSuccessResponse {
            token: token.to_string(),
            show_verification_form: false,
        })
        // This header was seen at Jul 2024, but presumably is not needed
        // .add_header("set-cookie", "__Secure-recent_mfa=; Expires=Sat, 13-Jul-2024 18:09:09 GMT; Max-Age=0; Secure; HttpOnly; Path=/api; SameSite=Strict")
    } else {
        println!("ACHTUNG! ACHTUNG! Unable to auth w/ valid password! {auth_result:?}");

        let mut login_error = FormError::default();
        login_error.errors.email = Some(FormErrorList::error(
            "EMAIL_ALREADY_REGISTERED",
            &format!("{auth_result:?}"),
        ));
        hr::json(StatusCode::FORBIDDEN, &login_error)
    }
}

#[allow(dead_code)]
#[derive(Debug, Deserialize)]
struct RegistrationRequest {
    email: String,
    username: String,
    global_name: Option<String>,
    password: String,
    invite: Option<Stub>,
    consent: bool,
    date_of_birth: String,
    gift_code_sku_id: Option<Stub>,
    promotional_email_opt_in: Option<bool>,
}

#[derive(Debug, Default, Serialize, Deserialize)]
struct RegistrationSuccessResponse {
    token: String,
    show_verification_form: bool,
}

fn hash_of(pwd: &str) -> String {
    let l = sha3_512(pwd.as_bytes());
    BASE64_STANDARD.encode(&l)
}

#[get("/auth/location-metadata")]
async fn location_metadata(_ctx: AppData, _data: String) -> HR {
    hr::json(StatusCode::OK, &LocationMetadata::default())
}

#[derive(Debug, Default, Serialize, Deserialize)]
struct LocationMetadata {
    consent_required: bool,
    country_code: String,
    promotional_email_opt_in: PromotionalEmail,
}

#[derive(Debug, Default, Serialize, Deserialize)]
struct PromotionalEmail {
    required: bool,
    pre_checked: bool,
}

#[get("/unique-username/username-suggestions-unauthed")]
async fn username_suggestions(_ctx: AppData) -> HR {
    // TODO: proper suggiestion generation
    // TODO: there is ?global_name=Username url parameter

    let mut map: HashMap<String, String> = HashMap::new();
    map.insert("username".to_string(), "Юрий Семецкий".to_string());

    hr::json(StatusCode::OK, &map)
}

// TODO: implement "is username taken?" route
// /api/v9/unique-username/username-attempt-unauthed
// {"taken":true}

/// Form error response, used in both register and login
#[derive(Debug, Serialize)]
struct FormError {
    /// Always "Invalid Form Body". Unused?
    message: String,

    /// Always 50035
    code: u32,

    errors: FormErrorsDict,
}

impl Default for FormError {
    fn default() -> Self {
        Self {
            message: "Invalid Form Body".to_string(),
            code: 50035,
            errors: Default::default(),
        }
    }
}

#[derive(Debug, Default, Serialize)]
struct FormErrorsDict {
    #[serde(skip_serializing_if = "Option::is_none")]
    email: Option<FormErrorList>,

    #[serde(skip_serializing_if = "Option::is_none")]
    login: Option<FormErrorList>,

    #[serde(skip_serializing_if = "Option::is_none")]
    username: Option<FormErrorList>,

    #[serde(skip_serializing_if = "Option::is_none")]
    password: Option<FormErrorList>,

    #[serde(skip_serializing_if = "Option::is_none")]
    date_of_birth: Option<FormErrorList>,
}

#[derive(Debug, Serialize)]
struct FormErrorList {
    _errors: Vec<FormErrorItem>,
}

impl FormErrorList {
    fn error(code: &str, message: &str) -> Self {
        Self {
            _errors: vec![FormErrorItem {
                code: code.to_string(),
                message: message.to_string(),
            }],
        }
    }

    fn default_login_error() -> Self {
        Self {
            _errors: vec![FormErrorItem {
                message: "Login or password is invalid.".to_string(),
                code: "INVALID_LOGIN".to_string(),
            }],
        }
    }
}

#[derive(Debug, Serialize)]
struct FormErrorItem {
    /// Known codes:
    /// - USERNAME_ALREADY_TAKEN -- Username is unavailable. Try adding numbers, letters, underscores _ , or periods.
    /// - PASSWORD_REQUIREMENTS_MIN_LENGTH -- Must be at least 8 characters long.
    /// - PASSWORD_BAD_PASSWORD -- Password is too weak or common to use.
    /// - DATE_OF_BIRTH_UNDERAGE -- You need to be 16 or older in order to use Discord.
    /// - INVALID_LOGIN -- Login or password is invalid.
    code: String,

    /// Any arbitrary message to be shown to user
    message: String,
}
