use std::collections::HashMap;

use actix_web::{delete, get, http::StatusCode, patch, post, web, HttpResponse};

use serde::{Deserialize, Serialize};

use super::hr::{self, HR};
use crate::api::LoggedIn;
use crate::db::user_settings::{ReadState, READ_STATE_RESOURCE};
use crate::eventbus::{ChannelEvent, Event, SelfEvent};
use crate::structures::gateway::Stub;
use crate::structures::resource::message::Message;
use crate::structures::snowflake::Snowflake;
use crate::AppData;

pub fn configure(cfg: &mut web::ServiceConfig) {
    cfg.service(get_channel_messages);
    cfg.service(post_channel_message);
    cfg.service(patch_channel_message);
    cfg.service(delete_channel_message);
    cfg.service(post_ack);
}

#[get("/channels/{channel_id}/messages")]
async fn get_channel_messages(
    ctx: AppData,
    uid: LoggedIn,
    cid: web::Path<Snowflake>,
    params: web::Query<GetMessagesParams>,
) -> HR {
    hr::check_channel_access(&ctx, *uid, *cid).await?;

    let db = ctx.db.lock().await;

    let mut users = HashMap::new();
    users.insert(*uid, db.user(*uid).unwrap());

    let c = params.limit.unwrap_or(50);
    if c > 100 {
        panic!("No more than 100 msgs please!");
    }

    let msgs;
    if params.around.is_some() {
        let mid = params.around.unwrap();
        msgs = db.get_messages_around(*cid, mid, c / 2, c / 2).unwrap();
    } else if params.before.is_some() {
        let mid = params.before.unwrap();
        msgs = db.get_messages_around(*cid, mid, c, 0).unwrap();
    } else if params.after.is_some() {
        let mid = params.after.unwrap();
        msgs = db.get_messages_around(*cid, mid, 0, c).unwrap();
    } else {
        msgs = db
            .get_messages_around(*cid, Snowflake::new(), c, 0)
            .unwrap();
    }

    let msgs: Vec<Message> = msgs
        .iter()
        .map(|e| {
            let attaches = db.get_attaches_of_msg(e.id).unwrap();

            if !users.contains_key(&e.author) {
                println!("Users doesnt has key {:?}, looking up", e.author);
                users.insert(e.author, db.user(e.author).unwrap());
                println!("Found!");
            }

            Message::new(&ctx.config, e, &attaches, &users)
        })
        .collect();

    hr::ok_json(&msgs)
}

#[derive(Debug, Deserialize)]
struct GetMessagesParams {
    around: Option<Snowflake>,
    before: Option<Snowflake>,
    after: Option<Snowflake>,
    limit: Option<u64>,
}

#[post("/channels/{channel_id}/messages")]
async fn post_channel_message(
    ctx: AppData,
    uid: LoggedIn,
    channel_id: web::Path<Snowflake>,
    data: web::Json<MessageIn>,
) -> HR {
    hr::check_channel_access(&ctx, *uid, *channel_id).await?;

    let msg_id = Snowflake::new();

    let db = ctx.db.lock().await;

    // Write data to DB
    let msg_out = db
        .add_message(msg_id, *uid, *channel_id, data.0.content)
        .unwrap();

    // Note: attachments have to be written AFTER message due to sql
    // constraints.
    data.0.attachments.iter().for_each(|e| {
        let attach = Snowflake::from(&e.uploaded_filename);
        db.link_attach_to_msg(msg_id, attach).unwrap();
    });

    // Retrieve related info
    let attaches = db.get_attaches_of_msg(msg_id).unwrap();

    let mut users = HashMap::new();
    users.insert(*uid, db.user(*uid).unwrap());

    // Format DC message
    let mut formatted_msg = Message::new(&ctx.config, &msg_out, &attaches, &users);
    formatted_msg.nonce = Some(data.0.nonce);

    // Send broadcast
    ctx.ebus.lock().await.publish(Event::ChannelEvent {
        source: None,
        channel: *channel_id,
        event: ChannelEvent::MessageCreate(msg_out, attaches, users),
    });

    // Respond
    hr::ok_json(&formatted_msg)
}

/// Message sent by user
#[allow(dead_code)]
#[derive(Debug, Deserialize)]
struct MessageIn {
    content: String,
    nonce: String,
    // "channel_id": "..."
    // "type": 0
    // "sticker_ids": []
    #[serde(default)]
    attachments: Vec<MessageInAttachment>,

    mobile_network_type: Option<String>,
    tts: Option<bool>,
    flags: Option<u32>,
}

#[allow(dead_code)]
#[derive(Debug, Deserialize)]
struct MessageInAttachment {
    id: String,
    filename: String,
    uploaded_filename: String,
}

#[patch("/channels/{channel_id}/messages/{message_id}")]
async fn patch_channel_message(
    ctx: AppData,
    uid: LoggedIn,
    web::Path((channel_id, message_id)): web::Path<(Snowflake, Snowflake)>,
    data: web::Json<MessagePatchIn>,
) -> HR {
    hr::check_channel_access(&ctx, *uid, channel_id).await?;

    let db = ctx.db.lock().await;

    // Ensure user edits his own message
    let old_message = db.get_message(message_id).unwrap();
    if old_message.author != *uid {
        return Err(actix_web::error::ErrorForbidden("U r not the author"));
    }

    // Write data to DB
    db.update_message(message_id, data.0.content).unwrap();

    // Retrieve related info
    let message = db.get_message(message_id).unwrap();

    let attaches = db.get_attaches_of_msg(message_id).unwrap();

    let mut users = HashMap::new();
    users.insert(*uid, db.user(*uid).unwrap());

    // Format DC message
    let msg_out = Message::new(&ctx.config, &message, &attaches, &users);

    // Send broadcast
    ctx.ebus.lock().await.publish(Event::ChannelEvent {
        source: None,
        channel: channel_id,
        event: ChannelEvent::MessageUpdate(message, attaches, users),
    });

    // Respond
    hr::ok_json(&msg_out)
}

#[derive(Debug, Serialize, Deserialize)]
struct MessagePatchIn {
    content: String,
}

#[delete("/channels/{channel_id}/messages/{message_id}")]
async fn delete_channel_message(
    ctx: AppData,
    uid: LoggedIn,
    web::Path((channel_id, message_id)): web::Path<(Snowflake, Snowflake)>,
) -> HR {
    hr::check_channel_access(&ctx, *uid, channel_id).await?;

    let db = ctx.db.lock().await;

    // Retrieve related info
    let message = db.get_message(message_id).unwrap();

    // Update DB
    db.delete_message(message.id).unwrap();

    // Send broadcast
    ctx.ebus.lock().await.publish(Event::ChannelEvent {
        source: None,
        channel: channel_id,
        event: ChannelEvent::MessageDelete(message),
    });

    // Respond
    Ok(HttpResponse::new(StatusCode::OK))
}

#[post("/channels/{channel_id}/messages/{message_id}/ack")]
async fn post_ack(
    ctx: AppData,
    uid: LoggedIn,
    web::Path((channel_id, message_id)): web::Path<(Snowflake, Snowflake)>,
    data: web::Json<AckBody>,
) -> HR {
    hr::check_channel_access(&ctx, *uid, channel_id).await?;

    let db = ctx.db.lock().await;

    // Retrieve related info
    let next_ver = db.get_resource_ver(*uid, READ_STATE_RESOURCE).unwrap() + 1;

    // Update DB
    db.set_resource_ver(*uid, READ_STATE_RESOURCE, next_ver)
        .unwrap();

    db.update_read_state(ReadState {
        user: *uid,
        channel: channel_id,
        message: message_id,
        manual: data.0.manual,
    })
    .unwrap();

    // Send broadcast
    ctx.ebus.lock().await.publish(Event::SelfEvent {
        user_id: *uid,
        event: SelfEvent::MessageAck {
            version: next_ver,
            channel_id,
            message_id,
            last_viewed: data.0.last_viewed,
            manual: data.0.manual,
            mention_count: data.0.mention_count,
        },
    });

    hr::ok_json(&AckResponse::default())
}

#[derive(Debug, Deserialize)]
struct AckBody {
    /// Send during auto ack
    ///
    /// Calculated as `last view day since discord epoch + 1`
    last_viewed: Option<u32>,

    /// Sent during manual ack
    #[serde(default)]
    manual: bool,

    /// Sent during manual ack
    #[serde(default)]
    mention_count: u32,
    /*
    /// Sent during auto ack
    ///
    /// Use unknown
    token: Option<Stub>,
    */
}

#[derive(Debug, Default, Serialize)]
struct AckResponse {
    /// Always null?
    token: Option<Stub>,
}
