use actix_web::http::StatusCode;

use actix_web::{delete, get, post, web, HttpResponse};

use super::hr::{self, HR};
use crate::api::LoggedIn;
use crate::db::DB;
use crate::eventbus::{Event, GuildEvent};
use crate::structures::resource::invite::Invite;
use crate::structures::snowflake::Snowflake;
use crate::{db, AppData};

pub fn configure(cfg: &mut web::ServiceConfig) {
    cfg.service(create_guild_invite);
    cfg.service(get_guild_invites);
    cfg.service(get_channel_invites);
    cfg.service(get_invite);
    cfg.service(join_invite);
    cfg.service(delte_invite);
}

#[post("/channels/{channel_id}/invites")]
async fn create_guild_invite(ctx: AppData, uid: LoggedIn, channel_id: web::Path<Snowflake>) -> HR {
    hr::check_channel_access(&ctx, *uid, *channel_id).await?;

    let db = ctx.db.lock().await;

    let ch = db.get_channel(*channel_id).unwrap();
    let guild = ch.guild_id.map(|e| db.get_guild(e).unwrap()).map(|g| g.id);

    let invites = db.get_invites_of_guild(guild.unwrap()).unwrap();
    let existing_invite = invites
        .iter()
        .find(|e| e.inviter == *uid && e.channel == *channel_id)
        .cloned();

    if existing_invite.is_some() {
        let existing_invite = existing_invite.unwrap();
        hr::ok_json(&invite(&db, &existing_invite))
    } else {
        let new_invite = db::invite::Invite {
            code: format!("{:0x}", rand::random::<u32>()),
            guild,
            channel: *channel_id,
            inviter: *uid,
        };

        db.create_invite(&new_invite).unwrap();

        hr::ok_json(&invite(&db, &new_invite))
    }
}

#[get("/guilds/{guild_id}/invites")]
async fn get_guild_invites(ctx: AppData, uid: LoggedIn, guild_id: web::Path<Snowflake>) -> HR {
    hr::check_guild_access(&ctx, *uid, *guild_id).await?;

    let db = ctx.db.lock().await;

    let invites: Vec<Invite> = db
        .get_invites_of_guild(*guild_id)
        .unwrap()
        .iter()
        .map(|e| invite(&db, e))
        .collect();

    hr::ok_json(&invites)
}

#[get("/channels/{channel_id}/invites")]
async fn get_channel_invites(ctx: AppData, uid: LoggedIn, channel_id: web::Path<Snowflake>) -> HR {
    hr::check_channel_access(&ctx, *uid, *channel_id).await?;

    let db = ctx.db.lock().await;

    let invites: Vec<Invite> = db
        .get_invites_of_channel(*channel_id)
        .unwrap()
        .iter()
        .filter(|e| e.channel == *channel_id)
        .map(|e| invite(&db, e))
        .collect();

    hr::ok_json(&invites)
}

#[get("/invites/{code}")]
async fn get_invite(ctx: AppData, path: web::Path<String>) -> HR {
    let db = ctx.db.lock().await;
    let inv = db.get_invite(&path).unwrap();

    hr::ok_json(&invite(&db, &inv))
}

#[post("/invites/{code}")]
async fn join_invite(ctx: AppData, uid: LoggedIn, code: web::Path<String>) -> HR {
    let db = ctx.db.lock().await;

    let inv = db.get_invite(&code).unwrap().clone();
    let guild_id = inv.guild.unwrap();
    let is_in_guild = db.is_user_in_guild(guild_id, *uid);
    if !is_in_guild {
        db.add_user_to_guild(guild_id, *uid).unwrap();

        let user = db.user(*uid).unwrap();
        let guild = db.get_guild(inv.guild.unwrap()).unwrap();
        let channels = db.guild_channels(guild.id).unwrap();

        ctx.ebus.lock().await.publish(Event::GuildEvent {
            guild: guild.id,
            event: GuildEvent::UserJoin {
                user,
                guild,
                channels,
            },
        });
    }

    hr::ok_json(&invite(&db, &inv))
}

#[delete("/invites/{code}")]
async fn delte_invite(ctx: AppData, uid: LoggedIn, code: web::Path<String>) -> HR {
    let db = ctx.db.lock().await;
    let inv = db.get_invite(&code);
    if inv.is_ok() {
        let inv = inv.unwrap();

        hr::check_channel_access(&ctx, *uid, inv.channel).await?;

        db.delete_invite(&code).unwrap();
        hr::ok_json(&invite(&db, &inv))
    } else {
        Ok(HttpResponse::new(StatusCode::FORBIDDEN))
    }
}

fn invite(db: &DB, i: &db::invite::Invite) -> Invite {
    let g = db.get_guild(i.guild.unwrap()).unwrap();
    let c = db.get_channel(i.channel).unwrap();
    let u = db.user(i.inviter).unwrap();

    Invite::new(i, &g, &c, &u)
}
