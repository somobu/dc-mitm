//! Spacebar-specific API routes
//! 
//! Unfortunately, both Spacebar and JankClient is unable to handle current 
//! (Feb 2025) implementation of Discord API, so this routes are de-facto 
//! useless. But I'll keep it in hope that one day situation will change.

use actix_web::{get, web};
use serde::Serialize;

use crate::AppData;

use super::hr::{self, HR};

pub fn configure(cfg: &mut web::ServiceConfig) {
    cfg.service(get_instance_domains);
}

#[get("/policies/instance/domains")]
async fn get_instance_domains(ctx: AppData) -> HR {
    #[allow(non_snake_case)]
    #[derive(Debug, Serialize)]
    struct Response {
        cdn: String,
        gateway: String,
        defaultApiVersion: String,
        apiEndpoint: String,
    }

    hr::ok_json(&Response {
        cdn: ctx.config.cdn.url(),
        gateway: ctx.config.gateway.url(),
        defaultApiVersion: "9".to_string(),
        apiEndpoint: format!("{}/api/v9", ctx.config.main.url()),
    })
}
