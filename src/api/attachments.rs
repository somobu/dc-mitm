use actix_web::{post, web};
use serde::{Deserialize, Serialize};

use super::hr::{self, HR};
use crate::{structures::snowflake::Snowflake, AppData};

const MAX_UPLOAD_SIZE: u32 = 10 * 1024 * 1024;

pub fn configure(cfg: &mut web::ServiceConfig) {
    cfg.service(reserve_attachment);
}

#[post("/channels/{channel_id}/attachments")]
async fn reserve_attachment(
    ctx: AppData,
    channel_id: web::Path<Snowflake>,
    data: web::Json<AttachmentsReserveIn>,
) -> HR {
    let mut snowflake_id = 0;

    let cdn = ctx.config.cdn.url();

    let db = ctx.db.lock().await;

    let ch_out = AttachmentsReserveOut {
        attachments: data
            .files
            .iter()
            .filter(|e| e.file_size < MAX_UPLOAD_SIZE)
            .map(|f| {
                let file_id = Snowflake::new_with_id(snowflake_id);
                snowflake_id += 1;

                let file_name = &f.filename;

                db.create_attachment(file_id, channel_id.0, 0, 0, 0, None)
                    .unwrap();

                AttachmentsReserveProps {
                    id: f.id.parse().unwrap(),
                    upload_url: format!(
                        "{cdn}/upload/{}/{}/{file_name}",
                        channel_id.to_string(),
                        file_id.to_string(),
                    ),
                    upload_filename: format!("{}", file_id.to_string()),
                }
            })
            .collect(),
    };

    hr::ok_json(&ch_out)
}

#[derive(Debug, Deserialize)]
struct AttachmentsReserveIn {
    files: Vec<AttachmentsReserveFile>,
}

#[allow(dead_code)]
#[derive(Debug, Deserialize)]
struct AttachmentsReserveFile {
    filename: String,
    file_size: u32,
    id: String,
    is_clip: bool,
}

#[derive(Debug, Serialize)]
struct AttachmentsReserveOut {
    attachments: Vec<AttachmentsReserveProps>,
}

#[derive(Debug, Serialize)]
struct AttachmentsReserveProps {
    id: u32,
    upload_url: String,
    upload_filename: String,
}
