//! HTTP API v9 Implementation

use std::ops::Deref;

use actix_web::http::StatusCode;
use actix_web::web::{self};
use actix_web::{FromRequest, HttpResponse};
use futures_util::future::LocalBoxFuture;

use crate::structures::snowflake::Snowflake;
use crate::AppData;

pub mod attachments;
pub mod auth;
pub mod channels;
pub mod guilds;
pub mod hr;
pub mod invites;
pub mod messages;
pub mod spacebar;
pub mod users;

pub fn configure(cfg: &mut web::ServiceConfig) {
    attachments::configure(cfg);
    auth::configure(cfg);
    channels::configure(cfg);
    guilds::configure(cfg);
    invites::configure(cfg);
    messages::configure(cfg);
    spacebar::configure(cfg);
    users::configure(cfg);

    cfg.default_service(web::to(not_found));
}

async fn not_found() -> actix_web::Result<HttpResponse> {
    Ok(HttpResponse::new(StatusCode::NOT_FOUND))
}

/// Extracts user's Snowflake from Authorization header
/// 
/// Usage:
/// ```rust
/// #[get("/some/url")]
/// async fn some_endpoint(uid: LoggedIn) { // This is how u define it
///     let dat_uid: Snowflake = *uid;      // This is how u use it
/// }
/// ```
pub struct LoggedIn(pub Snowflake);

impl FromRequest for LoggedIn {
    type Error = actix_web::Error;

    type Future = LocalBoxFuture<'static, Result<LoggedIn, Self::Error>>;

    fn from_request(
        req: &actix_web::HttpRequest,
        _payload: &mut actix_web::dev::Payload,
    ) -> Self::Future {
        let ctx: AppData = req.app_data::<AppData>().unwrap().clone();
        let token = req
            .headers()
            .get("Authorization")
            .unwrap()
            .to_str()
            .unwrap()
            .to_string();

        Box::pin(async move {
            ctx.db
                .lock()
                .await
                .resolve_token(&token)
                .ok_or(actix_web::error::ErrorForbidden("No auth"))
                .map(|v| Self(v))
        })
    }
}

impl Deref for LoggedIn {
    type Target = Snowflake;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
