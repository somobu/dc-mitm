//! CDN server and upload API

use std::{io::Cursor, path::Path};

use actix_cors::Cors;
use actix_web::web::{self, Bytes};
use actix_web::HttpRequest;
use actix_web::{get, http::StatusCode, put, App, HttpResponse, HttpServer, Scope};
use base64::{prelude::BASE64_STANDARD, Engine};
use image::{imageops::FilterType, ImageError, ImageFormat, ImageReader};
use serde::Deserialize;

use crate::api::hr::HR;
use crate::assets;
use crate::{structures::snowflake::Snowflake, AppData};

pub async fn server(data: AppData) -> std::io::Result<()> {
    let d0 = data.clone();
    HttpServer::new(move || {
        App::new()
            .app_data(d0.clone())
            .wrap(crate::logger())
            .wrap(Cors::permissive())
            .app_data(web::PayloadConfig::new(32 * 1024 * 1024))
            .service(Scope::new("").configure(configure))
    })
    .bind((data.config.cdn.bind.clone(), data.config.cdn.port))?
    .run()
    .await
}

pub fn configure(cfg: &mut web::ServiceConfig) {
    cfg.service(cdn_attachment);
    cfg.service(cdn_icon_short);
    cfg.service(cdn_avatar_short);
    cfg.service(cdn_avatar_full);
    cfg.service(upload_attachment);
}

#[derive(Debug, Deserialize)]
struct ImageConversionParams {
    format: Option<String>,
    width: Option<u32>,
    height: Option<u32>,
}

#[get("/attachments/{channel}/{id}/{filename}")]
async fn cdn_attachment(req: HttpRequest, path: web::Path<(String, String, String)>) -> HR {
    let (channel, id, filename) = path.into_inner();

    let ext = assets::get_file_ext(&filename);
    let mime = assets::mime_from_ext(ext);
    let resize = assets::is_resizable_image(ext);

    let mod_mime = if mime.starts_with("text/") {
        mime.to_string() + ";charset=UTF-8"
    } else {
        mime.to_string()
    };

    let params = web::Query::<ImageConversionParams>::from_query(req.query_string())
        .unwrap()
        .into_inner();

    let file_path = format!("server/attachments/{channel}/{id}");
    let convert = if resize { Some(params) } else { None };
    let rz = serve_attach(&file_path, convert);
    let bytes = rz.unwrap();

    if req.headers().contains_key("range") {
        // DC requests long text files as http partial content

        // TODO: For now CDN returns the entire file as-is, but proper
        // pagination has to be implemented

        let e = bytes.len() - 1;
        let l = bytes.len();

        Ok(HttpResponse::PartialContent()
            .content_type(mod_mime)
            .insert_header(("x-goog-stored-content-length", bytes.len()))
            .insert_header(("content-range", format!("bytes 0-{e}/{l}")))
            .body(bytes))
    } else {
        Ok(HttpResponse::Ok().content_type(mod_mime).body(bytes))
    }
}

fn serve_attach(
    path: &str,
    params: Option<ImageConversionParams>,
) -> Result<Bytes, std::io::Error> {
    let data = std::fs::read(path)?;

    // No conversion is required -- return as-is
    if params.is_none() {
        return Ok(Bytes::from(data));
    }

    let params = params.unwrap();

    let img = ImageReader::new(Cursor::new(data)).with_guessed_format()?;

    let original_format = img.format().unwrap();

    let img = img.decode().unwrap();

    // Resize image (if needed)
    let target_w = params.width.unwrap_or(img.width());
    let target_h = params.height.unwrap_or(img.height());
    let resize = (target_w <= img.width()) && (target_h <= img.height());

    let img = if resize {
        img.resize(target_w, target_h, FilterType::Lanczos3)
    } else {
        img
    };

    // Convert raw pixels to specified image format
    let mut bytes: Vec<u8> = Vec::new();

    let requested_format = params.format.unwrap_or_default();
    let target_format = match requested_format.as_str() {
        "webp" => ImageFormat::WebP,
        _ => original_format,
    };

    img.write_to(&mut Cursor::new(&mut bytes), target_format)
        .unwrap();

    return Ok(Bytes::copy_from_slice(&bytes));
}

#[get("/icons/{server}/{icon}")]
async fn cdn_icon_short(path: web::Path<(String, String)>) -> actix_web::Result<Bytes> {
    let (server, icon) = path.into_inner();
    let rz = serve_simple_file(&format!("server/icons/{server}/{icon}"));
    Ok(rz.unwrap())
}

#[get("/avatars/{user}/{avatar}")]
async fn cdn_avatar_short(path: web::Path<(String, String)>) -> actix_web::Result<Bytes> {
    let (user, avatar) = path.into_inner();
    let rz = serve_simple_file(&format!("server/avatars/{user}/{avatar}"));
    Ok(rz.unwrap())
}

#[get("/guilds/{guild}/users/{user}/avatars/{avatar}")]
async fn cdn_avatar_full(path: web::Path<(String, String, String)>) -> actix_web::Result<Bytes> {
    let (_guild, user, avatar) = path.into_inner();
    let rz = serve_simple_file(&format!("server/avatars/{user}/{avatar}"));
    Ok(rz.unwrap())
}

fn serve_simple_file(path: &str) -> Result<Bytes, ()> {
    let path_no_ext = &path[..path.rfind(".").unwrap()];

    let data = std::fs::read(format!("{path_no_ext}.png"));
    if data.is_ok() {
        return Ok(Bytes::from(data.unwrap()));
    }

    Err(())
}

#[put("/upload/{channel_id}/{file_id}/{file_name}")]
async fn upload_attachment(
    ctx: AppData,
    path: web::Path<(String, String, String)>,
    data: Bytes,
) -> HttpResponse {
    let (channel_id, file_id, file_name) = path.into_inner();

    let size = data.len();
    let (w, h) = get_image_props(&data).unwrap_or((0, 0));

    let save_result = save_file(&data, &channel_id, &file_id);
    if save_result.is_err() {
        return HttpResponse::new(StatusCode::BAD_REQUEST);
    }

    ctx.db
        .lock()
        .await
        .update_attachment(Snowflake::from(file_id), w, h, size, Some(file_name))
        .unwrap();

    HttpResponse::new(StatusCode::CREATED)
}

pub fn get_image_props(data: &[u8]) -> Result<(u32, u32), ImageError> {
    let img = ImageReader::new(Cursor::new(data))
        .with_guessed_format()?
        .decode()?;

    let w = img.width();
    let h = img.height();

    Ok((w, h))
}

fn save_file(data: &Bytes, channel_id: &str, file_id: &str) -> Result<String, std::io::Error> {
    std::fs::create_dir_all(format!("server/attachments/{channel_id}/")).unwrap();

    let filename = format!("server/attachments/{channel_id}/{file_id}");
    std::fs::write(&filename, data)?;

    Ok(filename)
}

pub fn store_avatar(server: Snowflake, icon: Snowflake, data: &[u8]) {
    let filename = format!(
        "server/avatars/{}/{}.png",
        server.to_string(),
        icon.to_string()
    );

    store_simple(&filename, data);
}

pub fn store_icon(server: Snowflake, icon: Snowflake, data: &[u8]) {
    let filename = format!(
        "server/icons/{}/{}.png",
        server.to_string(),
        icon.to_string()
    );

    store_simple(&filename, data);
}

fn store_simple(filename: &str, data: &[u8]) {
    let img = ImageReader::new(Cursor::new(data))
        .with_guessed_format()
        .unwrap()
        .decode()
        .unwrap()
        .resize_to_fill(512, 512, FilterType::Lanczos3);

    let parent = Path::new(filename).parent().unwrap();
    std::fs::create_dir_all(parent).unwrap();

    img.save_with_format(filename, ImageFormat::Png).unwrap();
}

pub fn b64_to_pic(data: &str) -> Vec<u8> {
    assert!(data.starts_with("data:image/"));

    let data = &data[data.find(",").unwrap() + 1..];
    BASE64_STANDARD.decode(data).unwrap()
}
