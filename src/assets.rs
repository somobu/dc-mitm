//! Static assets fetching and serving

use std::{collections::HashMap, path::Path};

use actix_web::{body::BoxBody, error::ErrorForbidden, get, web, HttpResponse};

use log::warn;

use crate::{config::Config, AppData};

pub async fn index(ctx: AppData) -> actix_web::Result<HttpResponse> {
    fetch_asset(ctx, "index.html", "channels/@me").await
}

#[get("/assets/{asset}")]
pub async fn assets(ctx: AppData, path: web::Path<(String,)>) -> actix_web::Result<HttpResponse> {
    let (asset_name,) = path.into_inner();
    let sub_url = format!("assets/{asset_name}");
    fetch_asset(ctx, &asset_name, &sub_url).await
}

async fn fetch_asset(
    ctx: AppData,
    name_on_disk: &str,
    sub_url: &str,
) -> actix_web::Result<HttpResponse> {
    let assets_dir = &ctx.config.main.assets_directory;
    let processed_path = format!("{assets_dir}/{name_on_disk}");

    if sub_url.ends_with(".map") {
        // Just fail
        return Err(ErrorForbidden(""));
    }

    if !Path::new(&processed_path).exists() {
        if ctx.config.main.assets_fetch_src.is_none() {
            warn!("Requested asset {sub_url} doesnt exist at {processed_path}, no fetch performed");
        } else {
            warn!("Requested asset {sub_url} doesnt exist at {processed_path}, fetching");
            std::fs::create_dir_all(assets_dir)?;

            let src = ctx.config.main.assets_fetch_src.as_ref().unwrap();
            let proxy = ctx
                .config
                .main
                .assets_fetch_proxy
                .clone()
                .unwrap_or("".to_string());
            let cmd = format!("curl {proxy} {src}/{sub_url} -o {assets_dir}/{name_on_disk}");

            let rz = tokio::process::Command::new("bash")
                .arg("-c")
                .arg(cmd)
                .output()
                .await
                .unwrap();

            warn!("Fetching finished: {sub_url}, status: {}", rz.status);
        }
    }

    if name_on_disk.ends_with(".js") || name_on_disk.ends_with(".html") {
        let mut data = std::fs::read_to_string(&processed_path)?;

        replacements(&ctx.config)
            .iter()
            .for_each(|(k, v)| data = data.replace(k, v));

        ok_mime(&processed_path, BoxBody::new(data))
    } else {
        let data = std::fs::read(&processed_path)?;
        ok_mime(&processed_path, BoxBody::new(data))
    }
}

fn replacements(cfg: &Config) -> HashMap<&'static str, String> {
    let ms = &cfg.main.public_scheme;
    let mh = &cfg.main.public_host;
    let mp = cfg.main.public_port();

    let gs = &cfg.gateway.public_scheme;
    let gh = &cfg.gateway.public_host;
    let gp = cfg.gateway.public_port();

    let cs = &cfg.cdn.public_scheme;
    let ch = &cfg.cdn.public_host;
    let cp = cfg.cdn.public_port();

    HashMap::from([
        ("R_MAIN_SHORT", format!("{mh}:{mp}")),
        ("R_API_ENDPOINT_SHORT", format!("{mh}:{mp}/api")),
        ("R_API_ENDPOINT_FULL", format!("{ms}://{mh}:{mp}/api")),
        ("R_GW_ENDPOINT_FULL", format!("{gs}://{gh}:{gp}")),
        ("R_CDN_SHORT", format!("{ch}:{cp}")),
        ("R_CDN_FULL", format!("{cs}://{ch}:{cp}")),
    ])
}

fn ok_mime(path: &str, body: BoxBody) -> actix_web::Result<HttpResponse> {
    let end = get_file_ext(path);
    Ok(HttpResponse::Ok()
        .content_type(mime_from_ext(end))
        .body(body))
}

pub fn get_file_ext(path: &str) -> &str {
    if path.contains(".") {
        &path[path.rfind(".").unwrap() + 1..]
    } else {
        "bin"
    }
}

pub fn mime_from_ext(ext: &str) -> &str {
    match ext {
        "css" => "text/css",
        "js" => "text/javascript",
        "json" => "application/json",
        "html" => "text/html",
        "md" => "text/markdown",
        "txt" => "text/plain",
        "toml" => "text/plain",

        "gif" => "image/gif",
        "ico" => "image/vnd.microsoft.icon",
        "jpg" | "jpeg" => "image/jpeg",
        "png" => "image/png",
        "svg" => "image/svg+xml",
        "webp" => "image/webp",

        "woff" | "woff2" => "font/woff",

        "mp3" => "audio/mpeg",

        "mp4" => "video/mp4",
        "webm" => "video/webm",

        _ => {
            println!("Unknown extension!: {ext}");
            "application/octet-stream"
        }
    }
}

#[allow(unused)]
pub fn is_resizable_image(ext: &str) -> bool {
    ["ico", "jpg", "jpeg", "png", "svg", "webp"].contains(&ext)
}
