#!/usr/bin/python3

import sqlite3

print("Migration tool is going to do migration things")


# Step one: collect table names, column names and rows

con_bak = sqlite3.connect("dcas.sqlite.bak")

tables = list(con_bak.cursor().execute("SELECT tbl_name FROM sqlite_master WHERE type='table' and tbl_name not like 'sqlite_%';"))
tables = list(map(lambda x: x[0], tables))

names = {}
values = {}

for table in tables:
    cursor = con_bak.execute("SELECT * from "+table)
    names[table] = list(map(lambda x: x[0], cursor.description))
    values[table] = cursor.fetchall()

con_bak.close()


# Step two: 

con = sqlite3.connect("dcas.sqlite")

for table in tables:
    cur = con.cursor()

    header = ", ".join(names[table])
    subst = ", ".join("?"*len(names[table]))

    cur.executemany("INSERT INTO "+table+" ("+header+") VALUES ("+subst+");", values[table])

con.commit()

print("Migration tool successfully finished migration")
